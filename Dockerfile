# Compile
FROM node:14-alpine AS builder
WORKDIR /app
COPY ./package*.json ./
RUN npm install
COPY . .
RUN rm -rf ./.env.*
ARG env
COPY ./.env.${env} ./.env
RUN npm run build

# Server
FROM lipanski/docker-static-website:latest
COPY --from=builder /app/dist/spa .