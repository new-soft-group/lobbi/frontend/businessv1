import { MessageType } from 'src/services/enums/MessageType';
import { Conversation } from 'src/services/models/Conversation';
import { Message } from 'src/services/models/Message';
import { computed } from 'vue';
import { useStore } from 'src/store';
import { User } from 'src/services/models/User';
import moment from 'moment';
import { useI18n } from 'vue-i18n';
import PreviewMedia from 'src/components/modals/PreviewMedia.vue';
import { useQuasar } from 'quasar';
import axios from 'axios';
import forceDownload from 'src/services/helpers/ForceDownload';

export default function (
    message?: Message,
    isReply = false,
    isConversationName = false,
    initialConversation?: Conversation,
    initialStore?: any
) {
    const { t } = useI18n({ useScope: 'global' });
    const store = initialStore ?? useStore();
    const user = computed((): User => store.state.user.user as User);
    const $q = useQuasar();

    const isSender = computed(
        (): boolean => user.value.id == (message?.user?.id ?? message?.userId)
    );

    const time = computed((): string =>
        moment.utc(message?.createdAt).local().format('h:mm A')
    );

    const date = computed((): string => {
        const createdAt = moment.utc(message?.createdAt).local();
        const currentAt = moment.utc().local();

        // Today
        if (createdAt.clone().format('D/M/Y') == currentAt.format('D/M/Y')) {
            return t('dates.today');
        }

        // Yesterday
        else if (
            createdAt.clone().format('D/M/Y') ==
            currentAt.subtract(1, 'days').format('D/M/Y')
        ) {
            return t('dates.yesterday');
        }

        return createdAt.format('D MMM y');
    });

    const text = computed((): string => {
        let _text = message?.text ?? '';

        // Trim text
        _text = _text.trim();

        if (!isReply && message?.type == MessageType.TEXT) {
            // Check if emoji
            const singleEmojiRegex = new RegExp(
                /^(?:[\u2700-\u27bf]|(?:\ud83c[\udde6-\uddff]){2}|[\ud800-\udbff][\udc00-\udfff]|[\u0023-\u0039]\ufe0f?\u20e3|\u3299|\u3297|\u303d|\u3030|\u24c2|\ud83c[\udd70-\udd71]|\ud83c[\udd7e-\udd7f]|\ud83c\udd8e|\ud83c[\udd91-\udd9a]|\ud83c[\udde6-\uddff]|\ud83c[\ude01-\ude02]|\ud83c\ude1a|\ud83c\ude2f|\ud83c[\ude32-\ude3a]|\ud83c[\ude50-\ude51]|\u203c|\u2049|[\u25aa-\u25ab]|\u25b6|\u25c0|[\u25fb-\u25fe]|\u00a9|\u00ae|\u2122|\u2139|\ud83c\udc04|[\u2600-\u26FF]|\u2b05|\u2b06|\u2b07|\u2b1b|\u2b1c|\u2b50|\u2b55|\u231a|\u231b|\u2328|\u23cf|[\u23e9-\u23f3]|[\u23f8-\u23fa]|\ud83c\udccf|\u2934|\u2935|[\u2190-\u21ff])$/gim
            );
            if (singleEmojiRegex.exec(_text)) {
                return `<span class="single-emoji">${_text}<span>`;
            }

            // Check if emoji
            const doubleEmojiRegex = new RegExp(
                /^(?:[\u2700-\u27bf]|(?:\ud83c[\udde6-\uddff]){2}|[\ud800-\udbff][\udc00-\udfff]|[\u0023-\u0039]\ufe0f?\u20e3|\u3299|\u3297|\u303d|\u3030|\u24c2|\ud83c[\udd70-\udd71]|\ud83c[\udd7e-\udd7f]|\ud83c\udd8e|\ud83c[\udd91-\udd9a]|\ud83c[\udde6-\uddff]|\ud83c[\ude01-\ude02]|\ud83c\ude1a|\ud83c\ude2f|\ud83c[\ude32-\ude3a]|\ud83c[\ude50-\ude51]|\u203c|\u2049|[\u25aa-\u25ab]|\u25b6|\u25c0|[\u25fb-\u25fe]|\u00a9|\u00ae|\u2122|\u2139|\ud83c\udc04|[\u2600-\u26FF]|\u2b05|\u2b06|\u2b07|\u2b1b|\u2b1c|\u2b50|\u2b55|\u231a|\u231b|\u2328|\u23cf|[\u23e9-\u23f3]|[\u23f8-\u23fa]|\ud83c\udccf|\u2934|\u2935|[\u2190-\u21ff]){2}$/gim
            );
            if (doubleEmojiRegex.exec(_text)) {
                return `<span class="double-emoji">${_text}<span>`;
            }

            // Check if emoji
            const tripleEmojiRegex = new RegExp(
                /^(?:[\u2700-\u27bf]|(?:\ud83c[\udde6-\uddff]){2}|[\ud800-\udbff][\udc00-\udfff]|[\u0023-\u0039]\ufe0f?\u20e3|\u3299|\u3297|\u303d|\u3030|\u24c2|\ud83c[\udd70-\udd71]|\ud83c[\udd7e-\udd7f]|\ud83c\udd8e|\ud83c[\udd91-\udd9a]|\ud83c[\udde6-\uddff]|\ud83c[\ude01-\ude02]|\ud83c\ude1a|\ud83c\ude2f|\ud83c[\ude32-\ude3a]|\ud83c[\ude50-\ude51]|\u203c|\u2049|[\u25aa-\u25ab]|\u25b6|\u25c0|[\u25fb-\u25fe]|\u00a9|\u00ae|\u2122|\u2139|\ud83c\udc04|[\u2600-\u26FF]|\u2b05|\u2b06|\u2b07|\u2b1b|\u2b1c|\u2b50|\u2b55|\u231a|\u231b|\u2328|\u23cf|[\u23e9-\u23f3]|[\u23f8-\u23fa]|\ud83c\udccf|\u2934|\u2935|[\u2190-\u21ff]){3}$/gim
            );
            if (tripleEmojiRegex.exec(_text)) {
                return `<span class="triple-emoji">${_text}<span>`;
            }
        }

        if (isReply) {
            // Check if text is empty
            if (
                _text == '' ||
                [
                    MessageType.STICKER,
                    MessageType.EVENT_REMINDER,
                    MessageType.DOCUMENT,
                    MessageType.AUDIO
                ].includes(parseInt(`${message?.type ?? ''}`))
            ) {
                if (message?.type == MessageType.LIVE_LOCATION) {
                    _text = 'Live Location';
                } else if (message?.type == MessageType.EVENT_REMINDER) {
                    _text = 'Event Reminder';
                } else if (message?.type == MessageType.MOMENT) {
                    _text = 'Moment';
                } else if (message?.type == MessageType.POLL) {
                    _text = 'Poll';
                } else if (message?.type == MessageType.IMAGE) {
                    _text = 'Image';
                } else if (message?.type == MessageType.VIDEO) {
                    _text = 'Video';
                } else if (message?.type == MessageType.DOCUMENT) {
                    _text = 'Document';
                } else if (message?.type == MessageType.AUDIO) {
                    _text = 'Audio';
                } else if (message?.type == MessageType.GIF) {
                    _text = 'Gif';
                } else if (message?.type == MessageType.CONTACT) {
                    _text = 'Contact';
                } else if (message?.type == MessageType.LOCATION) {
                    _text = 'Location';
                } else if (message?.type == MessageType.STICKER) {
                    _text = 'Sticker';
                }

                // Italic the text
                _text = `<i>${_text}</i>`;
            }

            // Check if message has specific icon or condition
            if (message?.type == MessageType.STICKER) {
                _text = '<i>Sticker</i>';
            }
        }

        // Check for localization
        if (message?.localization) {
            // Get the user locale
            const locale: string = user.value.locale ?? 'en';

            // @ts-ignore
            _text = message?.localization?.[locale];
            _text = `<i>${_text}</i>`;
        }

        let match;

        // Check for dates
        const dateRegex = new RegExp(/\[\^(.+?)\^\]/gim);
        while (null != (match = dateRegex.exec(_text))) {
            _text = _text.replace(
                match[0],
                moment(match[1]).local().format('hh:mm A')
            );
        }

        // Check for bold
        const boldRegex = new RegExp(/\[\*(.+?)\*\]/gim);
        while (null != (match = boldRegex.exec(_text))) {
            _text = _text.replace(match[0], `<b>${match[1]}</b>`);
        }

        // Check for italic
        const italicRegex = new RegExp(/\[\~(.+?)\~\]/gim);
        while (null != (match = italicRegex.exec(_text))) {
            _text = _text.replace(match[0], `<i>${match[1]}</i>`);
        }

        // Check for underline
        const underlineRegex = new RegExp(/\[\_(.+?)\_\]/gim);
        while (null != (match = underlineRegex.exec(_text))) {
            _text = _text.replace(match[0], `<u>${match[1]}</u>`);
        }

        // Check for strike
        const strikeRegex = new RegExp(/\[\-(.+?)\-\]/gim);
        while (null != (match = strikeRegex.exec(_text))) {
            _text = _text.replace(match[0], `<s>${match[1]}</s>`);
        }

        // Check for URL
        const urlRegex = new RegExp(
            /(http(s)?:\/\/.)?(www\.)?[-a-zA-Z0-9@:%._\+~#=]{2,256}\.[a-z]{2,6}\b([-a-zA-Z0-9@:%_\+.~#?&//=]*)/gim
        );
        const urls: string[] = [];
        let i = 0;
        while (null != (match = urlRegex.exec(_text))) {
            urls.push(match[0]);
            _text = _text.replace(match[0], `[(${i})]`);
            i++;
        }
        urls.forEach((url, index) => {
            if (url.toLowerCase().includes('://')) {
                _text = _text.replace(
                    `[(${index})]`,
                    `<a target="_blank" href="${url}">${url}</a>`
                );
            } else {
                _text = _text.replace(
                    `[(${index})]`,
                    `<a target="_blank" href="//${url}">${url}</a>`
                );
            }
        });

        return _text.replace(/\n/g, '<br/>');
    });

    const statusIcon = computed(() => {
        const _icon: { name?: string; color?: string } = {
            name: undefined,
            color: undefined
        };

        const messageStatus = store.state.message.messageStatus;

        // Check for non-sender or system type
        if (
            message?.type == MessageType.SYSTEM ||
            message?.userId != user.value.id
        ) {
            return _icon;
        }

        // Check for pending message
        if (message.id == message.localId) {
            _icon.name = 'schedule';
            _icon.color = 'grey';
        }

        if (messageStatus != 200 && messageStatus != 201) {
            _icon.name = 'error';
            _icon.color = 'red';
        }

        return _icon;
    });

    const previewMedia = () => {
        if (
            message?.type == MessageType.IMAGE ||
            message?.type == MessageType.VIDEO
        ) {
            $q.dialog({
                component: PreviewMedia,
                componentProps: {
                    message
                }
            });
        }
    };

    const height = computed((): number => {
        const _height =
            (300 * (message?.file?.height ?? 1)) / (message?.file?.width ?? 1);
        return _height > 400 ? 400 : _height;
    });

    const downloadFile = () => {
        if (message?.file?.url) {
            axios({
                method: 'get',
                url: message?.file?.url,
                responseType: 'arraybuffer'
            })
                .then((response: any) => {
                    forceDownload(
                        response,
                        message?.file?.name ?? message?.text
                    );
                })
                .catch(() => console.error('Cannot download file.'));
        }
    };

    const previewUrl = computed(() => {
        if (message?.type == MessageType.VIDEO) {
            return message?.thumbnail?.url ?? '';
        } else if (message?.type == MessageType.IMAGE) {
            return message?.thumbnail?.url ?? message?.file?.url ?? '';
        } else if (
            message?.type == MessageType.GIF ||
            message?.type == MessageType.STICKER
        ) {
            return message?.file?.url ?? '';
        }
        return '';
    });

    const agentName = computed(() => {
        const conversation = store.state.conversation.selectedConversation;

        if (conversation) {
            // If not chat user will show as agent
            if (conversation.user.id != message?.user?.id) {
                return 'Agent - ' + message?.user?.name;
            }
        }

        return message?.user?.name;
    });

    const agentId = computed(() => {
        return message?.user?.id;
    });

    return {
        time,
        date,
        text,
        isSender,
        statusIcon,
        height,
        previewMedia,
        downloadFile,
        previewUrl,
        agentName,
        agentId
    };
}
