import { RouteRecordRaw } from 'vue-router';

const routes: RouteRecordRaw[] = [
    {
        path: '/',
        name: 'login',
        meta: {
            isGuest: true
        },
        component: () => import('layouts/DefaultLayout.vue'),
        children: [{ path: '', component: () => import('pages/Login.vue') }]
    },
    {
        path: '/',
        meta: {
            isAuthenticated: true
        },
        component: () => import('layouts/MainLayout.vue'),
        children: [
            {
                path: 'home',
                name: 'home',
                component: () => import('pages/Index.vue')
            },
            {
                path: 'helpdesk',
                name: 'helpdesk',
                component: () => import('src/pages/Helpdesk.vue')
            },
            {
                path: 'team',
                name: 'team',
                component: () => import('pages/Team.vue')
            },
            /* {
                path: 'attendance',
                name: 'attendance',
                component: () => import('pages/Attendance.vue')
            }, */
            {
                path: 'invite',
                name: 'invite',
                component: () => import('pages/Invite.vue')
            },
            {
                path: 'preference',
                name: 'preference',
                component: () => import('pages/Preference.vue')
            },
            {
                path: 'user/:id',
                name: 'user',
                component: () => import('pages/Helpdesk.vue')
            },
            {
                path: 'chat',
                name: 'chat',
                component: () => import('pages/Helpdesk.vue')
            }
        ]
    },

    // Always leave this as last one,
    // but you can also remove it
    {
        path: '/:catchAll(.*)*',
        component: () => import('pages/Error404.vue')
    }
];

export default routes;
