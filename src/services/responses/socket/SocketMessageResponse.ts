import { ConversationOriginal } from 'src/services/models/Conversation';
import { Device } from 'src/services/models/Device';
import { Message } from 'src/services/models/Message';
import { User } from 'src/services/models/User';

export interface SocketMessageResponse {
    id: string;
    type: string;
    body: SocketMessageResponseBody;
}

export interface SocketMessageResponseBody {
    token?: string;
    device?: Device;
    userId?: string;
    conversationId?: string;
    message?: Message;
    conversation?: ConversationOriginal;
    deletedConversationId?: string;
    clearedConversationId?: string;
    user?: User;
    loggedOutDeviceId?: string;
}
