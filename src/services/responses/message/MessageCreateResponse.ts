import { Message } from 'src/services/models/Message';

export interface MessageCreateResponse {
    status: number;
    message: string;
    result?: Message;
}
