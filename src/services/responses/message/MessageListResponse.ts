import { Message } from 'src/services/models/Message';

export interface MessageListResponse {
    status: number;
    message: string;
    result?: MessageListResponseResult;
}

export interface MessageListResponseResult {
    messages: Message[];
}
