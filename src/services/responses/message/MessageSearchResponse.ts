import { Message } from 'src/services/models/Message';
import { Pagination } from 'src/services/models/Pagination';

export interface MessageSearchResponse {
    status: number;
    message: string;
    result?: MessageSearchResponseResult;
}

export interface MessageSearchResponseResult {
    messages: Message[];
    pagination?: Pagination;
}
