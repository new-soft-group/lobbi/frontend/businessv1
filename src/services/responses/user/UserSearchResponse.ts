import { User } from 'src/services/models/User';

export interface UserSearchResponse {
    status: number;
    message: string;
    result?: UserSearchResponseResult;
}

export interface UserSearchResponseResult {
    user: User;
    users: [];
}
