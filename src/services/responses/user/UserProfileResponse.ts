import { User } from 'src/services/models/User';

export interface UserProfileResponse {
    id: string;
    result?: UserProfileResponseResult;
}

export interface UserProfileResponseResult {
    user: User;
}
