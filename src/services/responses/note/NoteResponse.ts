export interface NoteResponseResult {
    result: {
        note: {
            id: string;
            text: string;
            createdAt: string;
            agent: any;
        };
    };
}
