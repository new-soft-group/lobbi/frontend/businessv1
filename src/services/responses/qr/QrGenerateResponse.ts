export interface QRGenerateResponse {
    status: number;
    message: string;
    result?: QRGenerateResponseResult;
}

export interface QRGenerateResponseResult {
    hash: string;
}
