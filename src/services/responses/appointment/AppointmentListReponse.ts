import { Appointment } from 'src/services/models/Appointment';
import { Pagination } from 'src/services/models/Pagination';

export interface AppointmentListResponseResult {
    appointments: Appointment[];
    pagination: Pagination;
}
