import { Roles } from 'src/services/models/Roles';

export interface RolesListResponse {
    status: number;
    message: string;
    result?: RolesListResponseResult;
}

export interface RolesListResponseResult {
    role: Roles;
    roles: [];
}
