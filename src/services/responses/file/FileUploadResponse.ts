import { File } from 'src/services/models/Message';

export interface FileUploadResponse {
    status: number;
    message: string;
    result?: FileUploadResponseResult;
}

export interface FileUploadResponseResult {
    file: File;
}
