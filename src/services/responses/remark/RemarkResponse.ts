import { Remark } from 'src/services/models/Remark';

export interface RemarkResponse {
    status: number;
    message: string;
    result?: RemarkResponseResult;
}

export interface RemarkResponseResult {
    remark: Remark;
    remarks: [];
}
