export interface BasicResponse {
    status: number;
    message: string;
    result?: any;
}
