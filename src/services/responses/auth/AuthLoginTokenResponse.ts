import { Device } from 'src/services/models/Device';
import { User } from 'src/services/models/User';

export interface AuthLoginTokenResponse {
    status: number;
    message: string;
    result?: AuthLoginTokenResponseResult;
}

export interface AuthLoginTokenResponseResult {
    token: string;
    user: User;
    device: Device;
}
