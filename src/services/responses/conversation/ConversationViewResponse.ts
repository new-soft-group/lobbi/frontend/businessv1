import { ConversationOriginal } from 'src/services/models/Conversation';

export interface ConversationViewResponse {
    status: number;
    message: string;
    result?: ConversationViewResponseResult;
}

export interface ConversationViewResponseResult {
    conversation: ConversationOriginal;
}
