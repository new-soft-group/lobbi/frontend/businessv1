import { ConversationOriginal } from 'src/services/models/Conversation';
import { Pagination } from 'src/services/models/Pagination';

export interface ConversationListResponse {
    status: number;
    message: string;
    result?: ConversationListResponseResult;
}

export interface ConversationListResponseResult {
    conversations: ConversationOriginal[];
    pagination: Pagination;
    personalTotalUnread: number;
    groupTotalUnread: number;
}
