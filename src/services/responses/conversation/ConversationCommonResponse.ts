import { ConversationOriginal } from 'src/services/models/Conversation';
import { Pagination } from 'src/services/models/Pagination';

export interface ConversationCommonResponse {
	status: number;
	message: string;
	result?: ConversationCommonResponseResult;
}

export interface ConversationCommonResponseResult {
	conversations: ConversationOriginal[];
	pagination: Pagination;
}