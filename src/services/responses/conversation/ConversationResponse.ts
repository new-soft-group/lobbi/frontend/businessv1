import { ConversationList } from 'src/services/models/Conversation';

export interface ConversationResponse {
    status: number;
    message: string;
    result?: ConversationResponseResult;
}

export interface ConversationResponseResult {
    conversations: ConversationList;
}

export interface ConversationsTransferResponseResult {
    transfers: any;
}

export interface ConversationsTransferResponse {
    status: number;
    message: string;
    result?: ConversationsTransferResponseResult;
}
