import { IssueType } from 'src/services/models/IssueType';

export interface IssueTypeResponse {
    status: number;
    message: string;
    result?: IssueTypeResponseResult;
}

export interface IssueTypeResponseResult {
    issueTypes: IssueType;
}
