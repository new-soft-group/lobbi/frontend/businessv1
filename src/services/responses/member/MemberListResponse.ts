import { MemberList } from 'src/services/models/Member';

export interface MemberListResponse {
    status: number;
    message: string;
    result?: MemberListResponseResult;
}

export interface MemberListResponseResult {
    member: MemberList;
    members: [];
}
