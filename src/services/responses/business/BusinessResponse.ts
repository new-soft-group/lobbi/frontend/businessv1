import { Business } from 'src/services/models/Business';
import { Customer } from 'src/services/models/Customer';

export interface BusinessResponse {
    status: number;
    message: string;
    result?: BusinessResponseResult;
}
export interface CustomerResponse {
    status: number;
    message: string;
    result?: CustomerResponseResult;
}

export interface BusinessResponseResult {
    businesses: Business;
}
export interface CustomerResponseResult {
    customers: Customer;
}
