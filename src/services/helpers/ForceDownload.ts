import generateRandomString from 'src/services/helpers/GenerateRandomString';

export default (response: any, filename = generateRandomString(24)) => {
	const url = window.URL.createObjectURL(new Blob([response.data]));
	const link = document.createElement('a');
	link.href = url;
	link.setAttribute('download', filename);
	document.body.appendChild(link);
	link.click();
};