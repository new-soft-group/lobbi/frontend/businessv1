export default (text: string) => {
    let newText = '';
    let match;

    // Convert paragraph
    const paragraphRegex = new RegExp(/<p>(.+?)<\/p>/gim);
    while (null != (match = paragraphRegex.exec(text))) {
        if (match[1] == '<br>') {
            newText += '\n';
        } else {
            newText += `${match[1]}\n`;
        }
    }

    let i = 0;
    let hasNextMarkup = true;
    while (hasNextMarkup) {
        if (i > 0) {
            hasNextMarkup = false;
        }

        // Convert bold
        const boldRegex = new RegExp(/<strong>(.+?)<\/strong>/gim);
        while (null != (match = boldRegex.exec(newText))) {
            newText = newText.replace(match[0], `[*${match[1]}*]`);
            hasNextMarkup = true;
        }

        // Convert italic
        const italicRegex = new RegExp(/<em>(.+?)<\/em>/gim);
        while (null != (match = italicRegex.exec(newText))) {
            newText = newText.replace(match[0], `[~${match[1]}~]`);
        }

        // Convert underline
        const underlineRegex = new RegExp(/<u>(.+?)<\/u>/gim);
        while (null != (match = underlineRegex.exec(newText))) {
            newText = newText.replace(match[0], `[_${match[1]}_]`);
        }

        // Convert underline
        const strikeRegex = new RegExp(/<s>(.+?)<\/s>/gim);
        while (null != (match = strikeRegex.exec(newText))) {
            newText = newText.replace(match[0], `[-${match[1]}-]`);
        }

        i++;
    }

    // Convert mentioned
    const mentionRegex = new RegExp(
        /<span class="mention".*?data-id="(.+?)".*?<\/span>.*?<\/span>.*?<\/span>/gim
    );
    let newTextMention = newText;
    while (null != (match = mentionRegex.exec(newText))) {
        newTextMention = newTextMention.replace(match[0], `[@${match[1]}@]`);
    }
    newText = newTextMention;

    // Convert newline
    newText = newText.replace(/<br>/gim, '\n');

    return newText.trim();
};
