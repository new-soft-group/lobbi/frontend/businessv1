/* eslint-disable @typescript-eslint/no-unsafe-return */
/* eslint-disable @typescript-eslint/no-unsafe-member-access */
import { api } from 'boot/axios';
import { AuthLoginTokenRequestBody } from '../requests/auth/AuthLoginTokenRequest';
import {
    AuthLoginTokenResponseResult,
    AuthLoginTokenResponse
} from '../responses/auth/AuthLoginTokenResponse';
import { AuthRequestCodeBody } from '../requests/auth/AuthRequestCode';
import { AuthVerifyCodeBody } from '../requests/auth/AuthVerifyCode';
import { BasicResponse } from '../responses/BasicResponse';

export default class AuthApi {
    async loginByToken(
        body: AuthLoginTokenRequestBody
    ): Promise<AuthLoginTokenResponseResult | undefined> {
        return api
            .post('/api/v1/auth/login/token', body)
            .then((response: any) => response.data)
            .then((response: AuthLoginTokenResponse) => response.result);
    }

    async requestCode(
        body: AuthRequestCodeBody
    ): Promise<BasicResponse | undefined> {
        return api
            .post('/api/v1/auth/login/requestCode', body)
            .then((response: any) => response.data);
    }

    async verifyCode(
        body: AuthVerifyCodeBody
    ): Promise<BasicResponse | undefined> {
        return api
            .post('/api/v1/auth/login/verifyCode', body)
            .then((response: any) => response.data);
    }
}
