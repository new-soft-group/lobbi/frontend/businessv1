/* eslint-disable @typescript-eslint/no-unsafe-member-access */
/* eslint-disable @typescript-eslint/no-unsafe-return */
import { api } from 'boot/axios';
import { QRGenerateRequestBody } from 'src/services/requests/qr/QRGenerateRequest';
import {
    QRGenerateResponseResult,
    QRGenerateResponse
} from '../responses/qr/QrGenerateResponse';

export default class QrApi {
    async generate(
        body: QRGenerateRequestBody
    ): Promise<QRGenerateResponseResult | undefined> {
        return api
            .post('/api/v1/qr/generate', body)
            .then((response: any) => response.data)
            .then((response: QRGenerateResponse) => response.result);
    }
}
