/* eslint-disable @typescript-eslint/no-unsafe-member-access */
/* eslint-disable @typescript-eslint/no-unsafe-return */
import { api } from 'boot/axios';
import { businessApi } from 'boot/axios';
import {
    DeleteRoleBody,
    UpdateRoleBody,
    RoleBody,
    RoleListQuery
} from 'src/services/requests/role/RoleCreateRequest';
import {
    RolesListResponse,
    RolesListResponseResult
} from '../responses/role/RoleListResponse';
import { BasicResponse } from '../responses/BasicResponse';

export default class RoleApi {
    async listRole(
        query: RoleListQuery
    ): Promise<RolesListResponseResult | undefined> {
        return businessApi
            .get('/api/v1/role/list', { params: query })
            .then((response: any) => response.data)
            .then((response: RolesListResponse) => response.result);
    }
    async viewRole(
        query: RoleListQuery
    ): Promise<RolesListResponseResult | undefined> {
        return businessApi
            .get('/api/v1/role/view', { params: query })
            .then((response: any) => response.data)
            .then((response: any) => response.result);
    }
    async createRole(body: RoleBody): Promise<BasicResponse | undefined> {
        return businessApi
            .post('/api/v1/role/create', body)
            .then((response: any) => response.data);
    }
    async updateRole(body: UpdateRoleBody): Promise<BasicResponse | undefined> {
        return businessApi
            .put('/api/v1/role/update', body)
            .then((response: any) => response.data);
    }
    async deleteRole(
        query: DeleteRoleBody
    ): Promise<BasicResponse | undefined> {
        return businessApi
            .delete('/api/v1/role/delete', {
                data: { roleId: query.roleId }
            })
            .then((response: any) => response.data)
            .then((response: BasicResponse) => response);
    }
}
