/* eslint-disable @typescript-eslint/no-unsafe-return */
/* eslint-disable @typescript-eslint/no-unsafe-member-access */
import { helpdeskApi } from 'boot/axios';
import {
    EditCategoryBody,
    CreateCategoryBody,
    DeleteCategoryBody
} from 'src/services/requests/category/CategoryRequest';
import { BasicResponse } from '../responses/BasicResponse';

export default class CategoryApi {
    async list(): Promise<any | undefined> {
        return helpdeskApi
            .get('/api/v1/knowledgebase/category/list')
            .then((response: any) => response.data)
            .then((response: BasicResponse) => response);
    }

    async create(body: CreateCategoryBody): Promise<BasicResponse | undefined> {
        return helpdeskApi
            .post('/api/v1/knowledgebase/category/create', body)
            .then((response: any) => response.data);
    }

    async update(body: EditCategoryBody): Promise<any | undefined> {
        return helpdeskApi
            .put('/api/v1/knowledgebase/category/update', body)
            .then((response: any) => response.data)
            .then((response: BasicResponse) => response);
    }

    async delete(query: DeleteCategoryBody): Promise<any | undefined> {
        return helpdeskApi
            .delete('/api/v1/knowledgebase/category/delete', {
                data: { knowledgebaseCategoryId: query.knowledgebaseCategoryId }
            })
            .then((response: any) => response.data)
            .then((response: BasicResponse) => response);
    }
}
