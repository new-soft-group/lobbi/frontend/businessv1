/* eslint-disable @typescript-eslint/no-unsafe-return */
/* eslint-disable @typescript-eslint/no-unsafe-return */
/* eslint-disable @typescript-eslint/no-unsafe-member-access */
import { attendanceApi } from 'boot/axios';
import { AttendanceCheckoutRequestBody } from '../requests/attendance/AttendanceCheckoutRequest';
import { BasicResponse } from '../responses/BasicResponse';

export default class AttendanceApi {
    async checkIn(): Promise<any | undefined> {
        return attendanceApi
            .post('/api/v1/attendance/checkin')
            .then((response: any) => response.data)
            .then((response: any) => response.result);
    }

    async checkOut(
        body: AttendanceCheckoutRequestBody
    ): Promise<BasicResponse | undefined> {
        return attendanceApi
            .put('/api/v1/attendance/checkout', body)
            .then((response: any) => response.data)
            .then((response: any) => response.result);
    }

    async view(): Promise<any | undefined> {
        return attendanceApi
            .get('/api/v1/attendance/view')
            .then((response: any) => response.data)
            .then((response: any) => response.result);
    }
}
