/* eslint-disable @typescript-eslint/no-unsafe-return */
/* eslint-disable @typescript-eslint/no-unsafe-member-access */
import { helpdeskApi } from 'boot/axios';
import { LabelRequestBody } from '../requests/setting/LabelCreateRequest';
import { BasicResponse } from '../responses/BasicResponse';

export default class LabelApi {
    async list(): Promise<any | undefined> {
        return helpdeskApi
            .get('/api/v1/label/list')
            .then((response: any) => response.data)
            .then((response: BasicResponse) => response);
    }

    async create(body: LabelRequestBody): Promise<BasicResponse | undefined> {
        return helpdeskApi
            .post('/api/v1/label/create', body)
            .then((response: any) => response.data);
    }

    async update(body: LabelRequestBody): Promise<any | undefined> {
        return helpdeskApi
            .put('/api/v1/label/update', body)
            .then((response: any) => response.data)
            .then((response: BasicResponse) => response);
    }

    async delete(query: any): Promise<any | undefined> {
        return helpdeskApi
            .delete('/api/v1/label/delete', {
                data: { labelId: query.labelId }
            })
            .then((response: any) => response.data)
            .then((response: BasicResponse) => response);
    }
}
