/* eslint-disable @typescript-eslint/no-unsafe-member-access */
/* eslint-disable @typescript-eslint/no-unsafe-return */
import { businessApi } from 'boot/axios';
import { MemberInviteUserRequestBody } from 'src/services/requests/member/MemberInviteUserRequest';
import {
    MemberListQuery,
    MemberUpdateRequestBody
} from '../requests/member/MemberListRequest';
import {
    MemberListResponse,
    MemberListResponseResult
} from '../responses/member/MemberListResponse';
import { BasicResponse } from '../responses/BasicResponse';

export default class MemberApi {
    async inviteUser(
        body: MemberInviteUserRequestBody
    ): Promise<BasicResponse | undefined> {
        return businessApi
            .post('/api/v1/member/invite/user', body)
            .then((response: any) => response.data);
    }

    async getMembers(
        query: MemberListQuery
    ): Promise<MemberListResponseResult | undefined> {
        return businessApi
            .get('/api/v1/member/list', { params: query })
            .then((response: any) => response.data)
            .then((response: MemberListResponse) => response.result);
    }

    async update(body: MemberUpdateRequestBody): Promise<any | undefined> {
        return businessApi
            .put('/api/v1/member/update', body)
            .then((response: any) => response.data)
            .then((response: BasicResponse) => response);
    }

    async delete(query: any): Promise<any | undefined> {
        return businessApi
            .delete('/api/v1/member/remove', {
                data: { memberId: query.memberId }
            })
            .then((response: any) => response.data)
            .then((response: BasicResponse) => response);
    }
}
