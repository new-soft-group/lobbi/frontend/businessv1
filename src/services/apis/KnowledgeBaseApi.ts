/* eslint-disable @typescript-eslint/no-unsafe-return */
/* eslint-disable @typescript-eslint/no-unsafe-member-access */
import { helpdeskApi } from 'boot/axios';
import { BasicResponse } from '../responses/BasicResponse';
import {
    CreateKnowledgeBody,
    KnowledgeQuery,
    EditKnowledgeBody,
    DeleteKnowledgeBody

} from 'src/services/requests/knowledge/KnowledgeRequest';

export default class KnowledgeBaseApi {
    async list(query: KnowledgeQuery): Promise<any | undefined> {
        return helpdeskApi
            .get('/api/v1/knowledgebase/list', { params: query })
            .then((response: any) => response.data)
            .then((response: BasicResponse) => response);
    }

    async create(body: CreateKnowledgeBody): Promise<BasicResponse | undefined> {
        return helpdeskApi
            .post('/api/v1/knowledgebase/create', body)
            .then((response: any) => response.data);
    }

    async update(body: EditKnowledgeBody): Promise<any | undefined> {
        return helpdeskApi
            .put('/api/v1/knowledgebase/update', body)
            .then((response: any) => response.data)
            .then((response: BasicResponse) => response);
    }

    async delete(query: DeleteKnowledgeBody): Promise<any | undefined> {
        return helpdeskApi
            .delete('/api/v1/knowledgebase/delete', {
                data: { knowledgebaseId: query.knowledgebaseId }
            })
            .then((response: any) => response.data)
            .then((response: BasicResponse) => response);
    }
}
