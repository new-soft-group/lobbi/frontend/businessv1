/* eslint-disable @typescript-eslint/no-unsafe-return */
/* eslint-disable @typescript-eslint/no-unsafe-member-access */
import { helpdeskApi, businessApi } from 'boot/axios';
import { BasicResponse } from '../responses/BasicResponse';
import {
    BusinessResponse,
    BusinessResponseResult,
    CustomerResponse,
    CustomerResponseResult
} from '../responses/business/BusinessResponse';
import { updateSettingBody } from '../requests/setting/SettingRequest';
import {
    updateBusinessBody,
    deactivateBusinessBody,
    CustomerQuery,
    createCustomerBody
} from '../requests/business/BusinessRequest';
import { IssueTypeQuery } from 'src/services/requests/issuesType/IssueTypeRequest';

export default class BusinessApi {
    async setting(): Promise<any | undefined> {
        return helpdeskApi
            .get('/api/v1/setting/view')
            .then((response: any) => response.data)
            .then((response: BasicResponse) => response);
    }
    async putSetting(query: updateSettingBody): Promise<any | undefined> {
        return helpdeskApi
            .put('/api/v1/setting/update', query)
            .then((response: any) => response.data)
            .then((response: BasicResponse) => response);
    }

    async disableBusiness(
        query: deactivateBusinessBody
    ): Promise<any | undefined> {
        return businessApi
            .delete('/api/v1/business/service/disable', {
                data: { type: query }
            })
            .then((response: any) => response.data)
            .then((response: BasicResponse) => response);
    }

    async businesses(): Promise<any | undefined> {
        const type = 'helpdesk';
        return businessApi
            .get('/api/v1/business/membership', {
                params: { type }
            })
            .then((response: any) => response.data)
            .then((response: BusinessResponse) => response.result);
    }
    async customer(
        query?: CustomerQuery
    ): Promise<CustomerResponseResult | undefined> {
        return helpdeskApi
            .get('/api/v1/customer/list', {
                params: query
            })
            .then((response: any) => response.data)
            .then((response: CustomerResponse) => response.result);
    }
    async createCustomer(
        body?: createCustomerBody
    ): Promise<CustomerResponseResult | undefined> {
        return helpdeskApi
            .post('/api/v1/customer/create', body)
            .then((response: any) => response.data)
            .then((response: CustomerResponse) => response.result);
    }
    async createCustomerCSV(
        body: any
    ): Promise<CustomerResponseResult | undefined> {
        return helpdeskApi
            .post('/api/v1/customer/import', {
                filePath: body
            })
            .then((response: any) => response.data)
            .then((response: CustomerResponse) => response.result);
    }
    async putNote(body: any): Promise<CustomerResponseResult | undefined> {
        return helpdeskApi
            .put('/api/v1/customer/note', {
                note: body.note,
                userId: body.userId
            })
            .then((response: any) => response.data)
            .then((response: CustomerResponse) => response.result);
    }
    async businessView(
        id: IssueTypeQuery
    ): Promise<BusinessResponseResult | undefined> {
        return businessApi
            .get('/api/v1/business/view', { params: id })
            .then((response: any) => response.data)
            .then((response: BusinessResponse) => response.result);
    }

    async putBusinesses(body: updateBusinessBody): Promise<any | undefined> {
        return businessApi
            .put('/api/v1/business/update', body)
            .then((response: any) => response.data)
            .then((response: BasicResponse) => response);
    }
}
