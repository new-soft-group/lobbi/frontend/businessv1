/* eslint-disable @typescript-eslint/no-unsafe-return */
/* eslint-disable @typescript-eslint/no-unsafe-member-access */
import { businessApi } from 'boot/axios';
import {
    branchQuery,
    createBranchBody,
    deleteBranchBody,
    updateBranchBody
} from '../requests/branch/BranchRequest';
import { BasicResponse } from '../responses/BasicResponse';

export default class BranchApi {
    async list(query?: branchQuery): Promise<any | undefined> {
        return businessApi
            .get('/api/v1/branch/list', {
                params: query
            })
            .then((response: any) => response.data)
            .then((response: BasicResponse) => response);
    }

    async create(body: createBranchBody): Promise<BasicResponse | undefined> {
        return businessApi
            .post('/api/v1/branch/create', body)
            .then((response: any) => response.data);
    }

    async update(body: updateBranchBody): Promise<any | undefined> {
        /* const config = {
            headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
        }; */
        return businessApi
            .put('/api/v1/branch/update', body)
            .then((response: any) => response.data)
            .then((response: BasicResponse) => response);
    }

    async delete(query: deleteBranchBody): Promise<any | undefined> {
        return businessApi
            .delete('/api/v1/branch/delete', {
                data: { labelId: query.branchId }
            })
            .then((response: any) => response.data)
            .then((response: BasicResponse) => response);
    }
}
