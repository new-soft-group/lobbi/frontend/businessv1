/* eslint-disable @typescript-eslint/restrict-template-expressions */
/* eslint-disable @typescript-eslint/no-unsafe-return */
/* eslint-disable @typescript-eslint/no-unsafe-member-access */
/* eslint-disable @typescript-eslint/no-unsafe-assignment */
import { api, helpdeskApi } from 'boot/axios';
import { Message } from '../models/Message';
import { MessageCreateRequestBody } from '../requests/message/MessageCreateRequest';
import { MessageDeleteRequestBody } from '../requests/message/MessageDeleteRequest';
import { MessageForwardRequestBody } from '../requests/message/MessageForwardRequest';
import { MessageListRequestQuery } from '../requests/message/MessageListRequest';
import { MessageSearchRequestQuery } from '../requests/message/MessageSearchRequest';
import { MessageStatusRequestBody } from '../requests/message/MessageStatusRequest';
import { MessageCreateResponse } from '../responses/message/MessageCreateResponse';
import {
    MessageListResponse,
    MessageListResponseResult
} from '../responses/message/MessageListResponse';
import {
    MessageSearchResponse,
    MessageSearchResponseResult
} from '../responses/message/MessageSearchResponse';
import { BasicResponse } from '../responses/BasicResponse';
import { MessageFavouriteListRequestQuery } from '../requests/message/MessageFavouriteListRequest';
import { MessageSyncRequestQuery } from '../requests/message/MessageSyncRequest';

export default class MessageApi {
    async list(
        query: MessageListRequestQuery
    ): Promise<MessageListResponseResult | undefined> {
        return helpdeskApi
            .get('/api/v1/message/list', { params: query })
            .then((response: any) => {
                return response.data.result;
            });
    }

    async historyList(
        query: any
    ): Promise<MessageListResponseResult | undefined> {
        return helpdeskApi
            .get('/api/v1/message/history', { params: query })
            .then((response: any) => {
                return response.data.result;
            });
    }

    async create(body: MessageCreateRequestBody) {
        return helpdeskApi
            .post('/api/v1/message/create', body)
            .then((response: any) => response.data);
    }

    async status(
        body: MessageStatusRequestBody
    ): Promise<BasicResponse | undefined> {
        return api
            .put('/api/v2/message/status', body)
            .then((response: any) => response.data);
    }

    async delete(
        body: MessageDeleteRequestBody
    ): Promise<BasicResponse | undefined> {
        return api
            .delete('/api/v1/message/delete', { data: body })
            .then((response: any) => response.data);
    }

    async favourite(
        body: MessageStatusRequestBody
    ): Promise<BasicResponse | undefined> {
        return api
            .put('/api/v1/message/favourite', body)
            .then((response: any) => response.data);
    }

    async forward(
        body: MessageForwardRequestBody
    ): Promise<BasicResponse | undefined> {
        return api
            .post('/api/v1/message/forward', body)
            .then((response: any) => response.data);
    }

    async search(
        query: MessageSearchRequestQuery
    ): Promise<MessageSearchResponseResult | undefined> {
        return helpdeskApi
            .get('/api/v1/message/search', { params: query })
            .then((response: any) => response.data)
            .then((response: MessageSearchResponse) => response.result);
    }

    async favouriteList(
        query: MessageFavouriteListRequestQuery
    ): Promise<MessageSearchResponseResult | undefined> {
        return api
            .get('/api/v1/message/favourite', { params: query })
            .then((response: any) => response.data)
            .then((response: MessageSearchResponse) => response.result);
    }

    async sync(
        query: MessageSyncRequestQuery
    ): Promise<MessageSearchResponseResult | undefined> {
        return api
            .get('/api/v1/message/sync', { params: query })
            .then((response: any) => response.data)
            .then((response: MessageSearchResponse) => response.result);
    }

    async searchRelation(query: any): Promise<any | undefined> {
        return helpdeskApi
            .get('/api/v1/message/relation', { params: query })
            .then((response: any) => response.data)
            .then((response: any) => response.result);
    }
}
