/* eslint-disable @typescript-eslint/no-unsafe-call */
/* eslint-disable @typescript-eslint/restrict-template-expressions */
/* eslint-disable @typescript-eslint/no-unsafe-assignment */
/* eslint-disable @typescript-eslint/no-unsafe-return */
/* eslint-disable @typescript-eslint/no-unsafe-member-access */
import { api } from 'boot/axios';
import { helpdeskApi } from 'boot/axios';
import { MemberInviteUserRequestBody } from 'src/services/requests/member/MemberInviteUserRequest';
import { MemberListQuery } from '../requests/member/MemberListRequest';
import {
    MemberListResponse,
    MemberListResponseResult
} from '../responses/member/MemberListResponse';

import { BasicResponse } from '../responses/BasicResponse';

export default class NotificationApi {
    /* async list(
        query: ConversationListRequestQuery
    ): Promise<ConversationListResponseResult | undefined> {
        return helpdeskApi
            .get('/api/v1/notification/list', { params: query })
            .then((response: any) => response.data)
            .then((response: ConversationListResponse) => response.result);
    } */
}
