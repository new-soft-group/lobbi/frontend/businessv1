/* eslint-disable @typescript-eslint/no-unsafe-return */
/* eslint-disable @typescript-eslint/no-unsafe-member-access */
import { helpdeskApi } from 'boot/axios';
import { CreateServiceBody, DeleteServiceBody, EditServiceBody, ServiceQuery } from '../requests/service/ServiceRequest';
import { BasicResponse } from '../responses/BasicResponse';

export default class ServiceApi {
    async list(query: ServiceQuery): Promise<any | undefined> {
        return helpdeskApi
            .get('/api/v1/serviceType/list', { params: query })
            .then((response: any) => response.data)
            .then((response: BasicResponse) => response);
    }

    async create(body: CreateServiceBody): Promise<BasicResponse | undefined> {
        return helpdeskApi
            .post('/api/v1/serviceType/create', body)
            .then((response: any) => response.data);
    }

    async update(body: EditServiceBody): Promise<any | undefined> {
        return helpdeskApi
            .put('/api/v1/serviceType/update', body)
            .then((response: any) => response.data)
            .then((response: BasicResponse) => response);
    }

    async delete(query: DeleteServiceBody): Promise<any | undefined> {
        return helpdeskApi
            .delete('/api/v1/serviceType/delete', {
                data: { serviceTypeId: query.serviceTypeId }
            })
            .then((response: any) => response.data)
            .then((response: BasicResponse) => response);
    }
}
