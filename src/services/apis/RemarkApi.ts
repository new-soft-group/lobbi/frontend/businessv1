/* eslint-disable @typescript-eslint/no-unsafe-member-access */
/* eslint-disable @typescript-eslint/no-unsafe-return */
import { helpdeskApi } from 'boot/axios';
import {
    RemarkQuery,
    CreateRemarkBody,
    EditRemarkBody,
    DeleteRemarkBody
} from 'src/services/requests/remark/RemarkRequest';
import {
    RemarkResponse,
    RemarkResponseResult
} from '../responses/remark/RemarkResponse';
import { BasicResponse } from '../responses/BasicResponse';

export default class RemarkApi {
    async getRemarkByUserID(
        query: RemarkQuery
    ): Promise<RemarkResponseResult | undefined> {
        return helpdeskApi
            .get('/api/v1/remark/list', { params: query })
            .then((response: any) => response.data)
            .then((response: RemarkResponse) => response.result);
    }
    async createRemark(
        body: CreateRemarkBody
    ): Promise<BasicResponse | undefined> {
        return helpdeskApi
            .post('/api/v1/remark/create', body)
            .then((response: any) => response.data);
    }
    async editRemark(
        body: EditRemarkBody
    ): Promise<BasicResponse | undefined> {
        return helpdeskApi
            .put('/api/v1/remark/update', body)
            .then((response: any) => response.data)
    }
    async deleteRemark(
        body: DeleteRemarkBody
    ): Promise<BasicResponse | undefined> {
        return helpdeskApi
            .delete('/api/v1/remark/delete', { data: body })
            .then((response: any) => response.data);
    }
}
