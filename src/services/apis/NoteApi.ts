import { helpdeskApi } from "src/boot/axios";
import { BasicResponse } from "../responses/BasicResponse";
import { 
    NoteQuery, 
    EditNoteBody 
} from "../requests/note/NoteRequest";

export default class NoteApi {
    async list(query: NoteQuery): Promise<any | undefined> {
        return helpdeskApi
        .get('/api/v1/customer/note', { params: query })
        .then((response: any) => response.data)
        .then((response: BasicResponse) => response);
    }

    async update(body: EditNoteBody): Promise<any | undefined> {
        return helpdeskApi
            .put('/api/v1/customer/note', body)
            .then((response: any) => response.data)
            .then((response: BasicResponse) => response);
    }
}