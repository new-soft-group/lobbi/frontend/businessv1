import { api, helpdeskApi } from 'boot/axios';
import { FileUploadRequestQuery } from '../requests/file/FileUploadRequest';
import {
    FileUploadResponseResult,
    FileUploadResponse
} from '../responses/file/FileUploadResponse';

export default class FileApi {
    async upload(
        body: FormData,
        query?: FileUploadRequestQuery
    ): Promise<FileUploadResponseResult | undefined> {
        return helpdeskApi
            .post('/api/v1/file/upload', body, {
                params: query
            })
            .then((response: any) => response.data)
            .then((response: FileUploadResponse) => response.result);
    }

    async avatarUpload(
        body: FormData,
        query?: FileUploadRequestQuery
    ): Promise<FileUploadResponseResult | undefined> {
        return api
            .post('/api/v1/file/upload', body, {
                params: query
            })
            .then((response: any) => response.data)
            .then((response: FileUploadResponse) => response.result);
    }
}
