/* eslint-disable @typescript-eslint/no-unsafe-return */
/* eslint-disable @typescript-eslint/no-unsafe-return */
/* eslint-disable @typescript-eslint/no-unsafe-member-access */
import { helpdeskApi } from 'boot/axios';
import {
    AppointmentQuery,
    createAppointmentBody,
    updateAppointmentStatusBody
} from '../requests/appointment/AppointmentRequest';

export default class AttendanceApi {
    async appointments(query?: AppointmentQuery): Promise<any | undefined> {
        return helpdeskApi
            .get('/api/v1/appointment/list', {
                params: query
            })
            .then((response: any) => response.data)
            .then((response: any) => response.result);
    }
    async createAppointment(
        body: createAppointmentBody
    ): Promise<any | undefined> {
        return helpdeskApi
            .post('/api/v1/appointment/create', body)
            .then((response: any) => response.data)
            .then((response: any) => response.result);
    }

    async updateAppointmentStatus(
        body: updateAppointmentStatusBody
    ): Promise<any | undefined> {
        return helpdeskApi
            .put('/api/v1/appointment/status', body)
            .then((response: any) => response.data)
            .then((response: any) => response.result);
    }
}
