/* eslint-disable @typescript-eslint/no-unsafe-return */
/* eslint-disable @typescript-eslint/no-unsafe-member-access */
import { helpdeskApi } from 'boot/axios';
import { AuditLogQuery } from 'src/services/requests/user/AuditLogRequest';

export default class AuditApi {
    async getAudit(query?: AuditLogQuery): Promise<any | undefined> {
        return helpdeskApi
            .get('/api/v1/auditLog/list',  { params: query })
            .then((response: any) => response.data)
            .then((response: any) => response.result);
    }
}
