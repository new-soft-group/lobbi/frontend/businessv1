/* eslint-disable @typescript-eslint/no-unsafe-member-access */
/* eslint-disable @typescript-eslint/no-unsafe-return */
import { helpdeskApi } from 'boot/axios';
import {
    IssueTypeQuery,
    CreateIssueTypeBody,
    EditIssueTypeBody,
    DeleteIssueTypeBody
} from 'src/services/requests/issuesType/IssueTypeRequest';
import {
    IssueTypeResponseResult,
    IssueTypeResponse
} from '../responses/issuesType/IssueTypeResponse';
import { BasicResponse } from '../responses/BasicResponse';

export default class ConversationApi {
    async getIssueTypeByBusinessID(
        query: IssueTypeQuery
    ): Promise<IssueTypeResponseResult | undefined> {
        return helpdeskApi
            .get('/api/v1/issueType/list', { params: query })
            .then((response: any) => response.data)
            .then((response: IssueTypeResponse) => response.result);
    }
    async createIssuesType(
        body: CreateIssueTypeBody
    ): Promise<BasicResponse | undefined> {
        return helpdeskApi
            .post('/api/v1/issueType/create', body)
            .then((response: any) => response.data);
    }
    async editIssuesType(
        body: EditIssueTypeBody
    ): Promise<BasicResponse | undefined> {
        return helpdeskApi
            .put('/api/v1/issueType/update', body)
            .then((response: any) => response.data);
    }
    async deleteIssuesType(
        body: DeleteIssueTypeBody
    ): Promise<BasicResponse | undefined> {
        return helpdeskApi
            .delete('/api/v1/issueType/delete', { data: body })
            .then((response: any) => response.data);
    }
}
