/* eslint-disable @typescript-eslint/no-unsafe-call */
/* eslint-disable @typescript-eslint/restrict-template-expressions */
/* eslint-disable @typescript-eslint/no-unsafe-assignment */
/* eslint-disable @typescript-eslint/no-unsafe-return */
/* eslint-disable @typescript-eslint/no-unsafe-member-access */
import {
    ConversationResponse,
    ConversationResponseResult,
    ConversationsTransferResponse,
    ConversationsTransferResponseResult
} from '../responses/conversation/ConversationResponse';
import {
    IssueTypeResponse,
    IssueTypeResponseResult
} from '../responses/conversation/IssueTypeResponse';
import { api, helpdeskApi } from 'boot/axios';
import {
    ConversationFlagRequestBody,
    ConversationFlagRequestParam
} from '../requests/conversation/ConversationFlagRequest';
import {
    ConversationCancelTransferBody,
    ConversationListRequestQuery,
    CreateConversationRequestBody
} from '../requests/conversation/ConversationListRequest';
import { ConversationSyncRequestQuery } from '../requests/conversation/ConversationSyncRequest';
import { ConversationViewRequestQuery } from '../requests/conversation/ConversationViewRequest';
import {
    ConversationTransferBody,
    ConversationJoinBody,
    ConversationEndBody
} from '../requests/conversation/ConversationListRequest';
import { BasicResponse } from '../responses/BasicResponse';
import {
    ConversationCommonResponse,
    ConversationCommonResponseResult
} from '../responses/conversation/ConversationCommonResponse';
import {
    ConversationListResponseResult,
    ConversationListResponse
} from '../responses/conversation/ConversationListResponse';
import {
    ConversationViewResponseResult,
    ConversationViewResponse
} from '../responses/conversation/ConversationViewResponse';

export default class ConversationApi {
    async list(
        query: ConversationListRequestQuery
    ): Promise<ConversationListResponseResult | undefined> {
        return helpdeskApi
            .get('/api/v1/conversation/list', { params: query })
            .then((response: any) => response.data)
            .then((response: ConversationListResponse) => response.result);
    }

    async view(
        query: ConversationViewRequestQuery
    ): Promise<ConversationViewResponseResult | undefined> {
        return api
            .get('/api/v1/conversation/view', { params: query })
            .then((response: any) => response.data)
            .then((response: ConversationViewResponse) => response.result);
    }

    async flag(
        param: ConversationFlagRequestParam,
        body: ConversationFlagRequestBody
    ): Promise<BasicResponse | undefined> {
        return api
            .put(`/api/v1/conversation/flag/${param.flag}`, body)
            .then((response: any) => response.data);
    }

    async clear(
        body: ConversationViewRequestQuery
    ): Promise<BasicResponse | undefined> {
        return api
            .delete('/api/v1/conversation/clear', { data: body })
            .then((response: any) => response.data);
    }

    async delete(
        body: ConversationViewRequestQuery
    ): Promise<BasicResponse | undefined> {
        return api
            .delete('/api/v1/conversation/delete', { data: body })
            .then((response: any) => response.data);
    }

    async sync(
        query: ConversationSyncRequestQuery
    ): Promise<ConversationCommonResponseResult | undefined> {
        return api
            .get('/api/v1/conversation/sync', { params: query })
            .then((response: any) => response.data)
            .then((response: ConversationCommonResponse) => response.result);
    }

    async markAsRead(
        body: ConversationViewRequestQuery
    ): Promise<BasicResponse | undefined> {
        return api
            .put('/api/v1/conversation/read', body)
            .then((response: any) => response.data);
    }

    async getConversations(
        query: ConversationListRequestQuery
    ): Promise<ConversationResponseResult | undefined> {
        return helpdeskApi
            .get('/api/v1/conversation/list', {
                params: {
                    limit: 20,
                    page: query.page,
                    isDownward: query.isDownward,
                    statuses: query.status,
                    userId: query.userId,
                    issueTypeIds: query.issueTypeIds,
                    agentId: query.agentId
                }
            })
            .then((response: any) => response.data)
            .then((response: ConversationResponse) => response.result);
    }
    async conversations(data: {
        businessID: string;
        agentID: string;
        status: number;
    }): Promise<ConversationResponseResult | undefined> {
        return helpdeskApi
            .get('/api/v1/conversation/list', {
                params: {
                    agentId: data.agentID,
                    limit: 20,
                    isDownward: 0,
                    statuses: data.status
                }
            })
            .then((response: any) => response.data)
            .then((response: ConversationResponse) => response.result);
    }

    async transferConversation(
        body: ConversationTransferBody
    ): Promise<BasicResponse | undefined> {
        return helpdeskApi
            .post('/api/v1/conversation/transfer/send', body)
            .then((response: any) => response.data);
    }

    async requestConversation(
        body: ConversationTransferBody
    ): Promise<BasicResponse | undefined> {
        return helpdeskApi
            .post('api/v1/conversation/transfer/request', body)
            .then((response: any) => response.data);
    }

    async cancelTransferConversation(
        body: ConversationCancelTransferBody
    ): Promise<BasicResponse | undefined> {
        return helpdeskApi
            .delete('/api/v1/conversation/transfer/cancel', { data: body })
            .then((response: any) => response.data);
    }

    async joinConversation(
        body: ConversationJoinBody
    ): Promise<BasicResponse | undefined> {
        return helpdeskApi
            .put('/api/v1/conversation/join', body)
            .then((response: any) => response.data);
    }

    async endConversation(
        body: ConversationEndBody
    ): Promise<BasicResponse | undefined> {
        return helpdeskApi
            .delete('/api/v1/conversation/end', {
                data: { conversationId: body }
            })
            .then((response: any) => response.data);
    }

    async getConversationsTransferList(
        businessID: string
    ): Promise<ConversationsTransferResponseResult | undefined> {
        return helpdeskApi
            .get('/api/v1/conversation/transfer/list')
            .then((response: any) => response.data)
            .then((response: ConversationsTransferResponse) => response.result);
    }

    async responseTransfer(data: {
        transferID: string;
        status: number;
    }): Promise<BasicResponse | undefined> {
        const body = {
            status: data.status,
            transferId: data.transferID
        };
        return helpdeskApi
            .put('/api/v1/conversation/transfer/response', body)
            .then((response: any) => response.data)
            .then((response: BasicResponse) => response);
    }

    async searchRelation(query: any): Promise<any | undefined> {
        return helpdeskApi
            .get('/api/v1/conversation/relation', { params: query })
            .then((response: any) => response.data)
            .then((response: any) => response.result);
    }

    async conversationUpdate(body: {
        conversationId: string;
        labelIds: any;
    }): Promise<BasicResponse | undefined> {
        return helpdeskApi.put('api/v1/conversation/update', body);
    }

    async reactivateConversation(
        body: ConversationJoinBody
    ): Promise<any | undefined> {
        return helpdeskApi.put('api/v1/conversation/reactivate', body);
    }

    async createConversation(
        body: CreateConversationRequestBody
    ): Promise<any | undefined> {
        return helpdeskApi.post('api/v1/conversation/create2', body);
    }
    
    async conversationIssueTypeUpdate(body: {
        conversationId: string;
        issueTypeId: string;
    }): Promise<BasicResponse | undefined> {
        return helpdeskApi.put('api/v1/conversation/issueType', body);
    }
}
