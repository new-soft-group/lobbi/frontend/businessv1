/* eslint-disable @typescript-eslint/no-unsafe-return */
/* eslint-disable @typescript-eslint/no-unsafe-member-access */
import { api } from 'boot/axios';
import { UserAvatarRequestBody } from '../requests/user/UserAvatarRequest';
import {
    UserFlagRequestBody,
    UserFlagRequestParam
} from '../requests/user/UserFlagRequest';
import { UserSearchRequestQuery } from '../requests/user/UserSearchRequest';
import { UserLocaleRequestBody } from '../requests/user/UserLocaleRequest';
import { UserLogoutRequestQuery } from '../requests/user/UserLogoutRequest';
import { UserUpdateRequestBody } from '../requests/user/UserUpdateRequest';
import { BasicResponse } from '../responses/BasicResponse';
import {
    UserProfileResponse,
    UserProfileResponseResult
} from '../responses/user/UserProfileResponse';
import {
    UserSearchResponse,
    UserSearchResponseResult
} from '../responses/user/UserSearchResponse';

export default class UserApi {
    async profile(): Promise<UserProfileResponseResult | undefined> {
        return api
            .get('/api/v1/user/profile')
            .then((response: any) => response.data)
            .then((response: UserProfileResponse) => response.result);
    }

    async search(
        query: UserSearchRequestQuery
    ): Promise<UserSearchResponseResult | undefined> {
        return api
            .get('/api/v1/user/search', { params: query })
            .then((response: any) => response.data)
            .then((response: UserSearchResponse) => response.result);
    }

    async update(
        body: UserUpdateRequestBody
    ): Promise<BasicResponse | undefined> {
        return api
            .put('/api/v1/user/profile', body)
            .then((response: any) => response.data);
    }

    async flag(
        param: UserFlagRequestParam,
        body: UserFlagRequestBody
    ): Promise<BasicResponse | undefined> {
        return api
            .put(`/api/v1/user/flag/${param.name}`, body)
            .then((response: any) => response.data);
    }

    async avatar(
        body: UserAvatarRequestBody
    ): Promise<BasicResponse | undefined> {
        return api
            .put('/api/v1/user/avatar', body)
            .then((response: any) => response.data);
    }

    async logout(
        query: UserLogoutRequestQuery
    ): Promise<BasicResponse | undefined> {
        return api
            .delete('/api/v1/user/logout', { params: query })
            .then((response: any) => response.data);
    }

    async locale(
        body: UserLocaleRequestBody
    ): Promise<BasicResponse | undefined> {
        return api
            .put('/api/v1/user/locale', body)
            .then((response: any) => response.data);
    }
}
