export interface FilterList {
    page: number;
    agent?: any;
    client?: any;
    issueType?: any;
    status?: any;
}
