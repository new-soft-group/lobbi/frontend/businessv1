export interface Customer {
    id: string;
    name: string;
    imageUrl?: string;
    totalConversation?: string;
    latestConversationId: string;
    latestAgent?: string;
    latestConversationAt: string;
    labels: {
        id: string;
        name?: string;
        color?: string;
        icon?: string;
    };
    createdAt?: Date;
}
