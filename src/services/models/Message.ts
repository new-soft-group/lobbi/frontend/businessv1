import { MessageDeleteFlag } from '../enums/MessageDeleteFlag';
import { MessageReaction } from '../enums/MessageReaction';
import { MessageType } from '../enums/MessageType';
import { SimpleConversation } from './Conversation';
import { SimpleUser } from './User';

export interface Message {
    id: string;
    userId: string;
    user?: SimpleUser;
    conversationId?: string;
    conversation?: SimpleConversation;
    localId: string;
    type: MessageType;
    text?: string;
    repliedMessage?: Message;
    mentionedUsers?: SimpleUser[];
    contactUsers?: SimpleUser[];
    localization?: Localization;
    reminderDateTime?: Date;
    isForwarded?: 0 | 1;
    broadcastedMessageId?: string;
    flags?: MessageFlags;
    location?: Location;
    file?: File;
    thumbnail?: File;
    placeholder?: string;
    deliveredAt?: Date;
    readAt?: Date;
    hiddenReadAt?: Date;
    selectedReaction?: MessageReaction;
    reactions?: Reactions;
    recipients?: Recipient[];
    sharedMoment?: null;
    poll?: null;
    createdAt: Date;
}

export interface Location {
    name?: string;
    address?: string;
    coordinate?: string;
    isActive: 0 | 1;
}

export interface File {
    url?: string;
    path?: string;
    name?: string;
    size?: string;
    mime?: string;
    duration?: number;
    width?: number;
    height?: number;
}

export interface MessageFlags {
    deleteFlag?: MessageDeleteFlag;
    favouriteFlag?: 0 | 1;
}

export interface Localization {
    key?: string;
    en?: string;
    my?: string;
    zh?: string;
}

export interface Reactions {
    love?: number;
    like?: number;
    happy?: number;
    sad?: number;
    shock?: number;
    angry?: number;
}

export interface Recipient {
    userId?: string;
    reaction?: MessageReaction;
    deliveredAt?: Date;
    readAt?: Date;
    hiddenReadAt?: Date;
}
