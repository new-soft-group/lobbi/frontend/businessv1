export interface Catogory {
    id: string;
    businessId: string;
    parentId: string;
    name: string;
    slug: string;
    nestedChild?: any[];
}
export interface Knowledgebases {
    id: string;
    businessId: string;
    title: string;
    content: string;
    slug: string;
    category: Catogory;
}
