/* eslint-disable @typescript-eslint/no-explicit-any */
// import { Dictionary } from 'lodash';
import { ConversationMemberRole } from '../enums/ConversationMemberRole';
import { ConversationMemberStatus } from '../enums/ConversationMemberStatus';
import { ConversationType } from '../enums/ConversationType';
import { Message } from './Message';

export interface Conversation {
    id: string;
    type: ConversationType;
    name?: string;
    userId?: string;
    description?: string;
    limit?: number;
    flags?: ConversationFlags;
    imageUrl?: string;
    updatedAt?: Date;
    totals?: ConversationTotals;
    members?: Member;
    message?: Message;
    lastReadAt?: Date;
    agent?: any;
    lastAgent?: {
        name: string;
        id: string;
    };
}

export interface ConversationOriginal {
    id: string;
    type: ConversationType;
    name?: string;
    userId?: string;
    description?: string;
    limit?: number;
    flags?: ConversationFlags;
    imageUrl?: string;
    updatedAt?: Date;
    totals?: ConversationTotals;
    members?: Member[];
    message?: Message;
    lastReadAt?: Date;
    lastClearedAt?: Date;
}

export interface SimpleConversation {
    id: string;
    name?: string;
    imageUrl?: string;
}

export interface ConversationFlags {
    publicFlag?: 0 | 1;
    pinFlag?: 0 | 1;
    notificationFlag?: 0 | 1;
}

export interface Member {
    userId: string;
    name?: string;
    username?: string;
    imageUrl?: string;
    role?: ConversationMemberRole;
    status?: ConversationMemberStatus;
}

export interface ConversationTotals {
    totalUnread?: number;
}
export interface ConversationList {
    id: string;
    user: {
        id: string;
        name: string;
        imageUrl?: string;
    };
    agent?: any;
    issueType: {
        id: string;
        name: string;
    };
    description: string;
    branch?: any;
    labels?: any;
    status: 0;
    referenceNo: string;
    createdAt: Date;
    updatedAt: Date;
    transfer?: any;
    finishLoaded?: boolean;
    lastAgent?: {
        name: string;
        id: string;
    };
}
export interface allChatList {
    id: string;
    client: {
        id: string;
        name: string;
        type?: string;
    };
    ticketId?: string;
    issueType: string;
    lastAgent: {
        name: string;
        id: string;
    };
    status: any;
}
export interface ConversationList {
    id: string;
    user: {
        id: string;
        name: string;
        imageUrl?: string;
    };
    agent?: any;
    issueType: {
        id: string;
        name: string;
    };
    description: string;
    branch?: any;
    labels?: any;
    status: 0;
    referenceNo: string;
    createdAt: Date;
    updatedAt: Date;
    transfer?: any;
    finishLoaded?: boolean;
}
