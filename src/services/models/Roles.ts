export interface Roles {
    id: string;
    name?: string;
    color?: string;
    permissions?: any;
    totalMember?: number;
}

export interface Permissions {
    conversation?: {
        list: boolean;
        join: boolean;
        update: boolean;
        end: boolean;
        transfer: boolean;
        transferForce: boolean;
        transferCancel: boolean;
    };
    role?: {
        list: boolean;
        create: boolean;
        view: boolean;
        update: boolean;
        delete: boolean;
    };
    member?: {
        list: boolean;
        invite: boolean;
        url: boolean;
        update: boolean;
        delete: boolean;
        auditLog: boolean;
    };
    knowledge?: {
        create: boolean;
        update: boolean;
        delete: boolean;
    };
    issuesType?: {
        create: boolean;
        update: boolean;
        delete: boolean;
    };
    label?: {
        list: boolean;
        create: boolean;
        update: boolean;
        delete: boolean;
    };
    remark?: {
        create: boolean;
        update: boolean;
        delete: boolean;
    };
    branch?: {
        list: boolean;
        create: boolean;
        view: boolean;
        update: boolean;
        delete: boolean;
    };
    branchMember?: {
        create: boolean;
        update: boolean;
        delete: boolean;
    };
    department?: {
        list: boolean;
        create: boolean;
        view: boolean;
        update: boolean;
        delete: boolean;
    };
    service?: {
        enable: boolean;
        disable: boolean;
        endPoint: boolean;
    };
}
