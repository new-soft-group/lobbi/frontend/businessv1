import { AppointmentStatus } from '../enums/AppointmentStatus';

export interface Appointment {
    branchId: string;
    createdAt: string;
    id: string;
    remark?: string;
    selectedDate: string;
    serviceTypes: any;
    startTime: string;
    status: AppointmentStatus;
    user: any;
}
