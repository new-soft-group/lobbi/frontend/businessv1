export interface IssueType {
    id: string;
    name: string;
    notification?: boolean;
}
