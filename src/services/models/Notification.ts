export interface NotificationList {
    id: string;
    avatar: string;
    message: string;
    createdAt: Date;
    status: number;
    conversationId?: any;
}
