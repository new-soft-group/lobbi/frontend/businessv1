export interface Device {
    id: string;
    type: 'ios' | 'android' | 'webapp';
    platform?: string;
    location?: string;
    lastLoggedInAt?: Date;
}
