export interface MemberList {
    bussinessId: string;
    user: SimpleMemberUser;
    role?: number;
    status: number;
}

export interface SimpleMemberUser {
    id: string;
    name?: string;
    imageUrl?: string;
}
