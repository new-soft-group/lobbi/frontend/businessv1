export interface User {
    id: string;
    name: string;
    username?: string;
    phone: string;
    description?: string;
    locale?: 'en' | 'my' | 'zh';
    totals?: UserTotals;
    flags?: UserFlags;
    imageUrl?: string;
}

export interface SimpleUser {
    id: string;
    name: string;
    username?: string;
    imageUrl?: string;
}

export interface UserFlags {
    contactFlag?: 0 | 1;
    messageFlag?: 0 | 1;
    receiptFlag?: 0 | 1;
    passwordFlag?: 0 | 1;
    tutorialFlag?: 0 | 1;
    notificationFlag?: 0 | 1;
}

export interface UserTotals {
    totalPost?: number;
    totalAlbum?: number;
    totalFollower?: number;
    totalUsername?: number;
    totalFavourite?: number;
    totalFollowing?: number;
}

export interface Info {
    tags?: string[];
    alias?: string;
    emails?: string[];
    phones?: string[];
    imageUrl?: string;
    description?: string;
}
