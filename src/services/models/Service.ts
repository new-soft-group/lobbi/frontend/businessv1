export interface Service {
    id: string;
    businessId: string;
    parentId: string;
    name: string;
}