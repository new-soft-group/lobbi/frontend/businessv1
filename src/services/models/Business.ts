import { Roles } from 'src/services/models/Roles';
export interface Business {
    id: string;
    name: string;
    registrationNo?: string;
    description: string;
    type: number;
    slug: string;
    category: number;
    imageUrl?: string;
    coverUrl?: string;
    banners?: string;
    tags: [];
    member: {
        id: string;
        role?: Roles;
        status: number;
        createdAt: Date;
        isOwner: boolean;
    };
    totals: any;
    service: any;
    branches?: any;
}
