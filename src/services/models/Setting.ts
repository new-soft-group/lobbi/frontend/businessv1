export interface Setting {
    businessId: string;
    autoAssignFlag?: number;
    maxActiveConversation?: number;
    issueTypeIds?: IssueType[];
}

export interface IssueType {
    id: number;
}

export interface Label {
    id: string;
    name: string;
    color: string;
    icon: string;
}
