import { MessageType } from '../enums/MessageType';

export interface FilePreview {
    file?: File;
    url?: string;
    previewUrl?: string;
    type: MessageType;
    text?: string;
    width?: number;
    height?: number;
}
