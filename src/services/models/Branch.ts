export interface Branch {
    id?: string;
    businessId: string;
    name?: string;
    address: {
        street: string;
        postcode?: string;
        city: string;
        state: string;
        country: string;
    };
    slug?: string;
    imagePath?: string;
    coverPath?: string;
    description?: string;
    tags?: [];
    status?: number;
    contacts: {
        type: string;
        value?: string;
    };
    isPrimary: boolean;
    createdAt: Date;
    totals: {
        totalFollower: number;
    };
}
