export interface Remark {
    id: string;
    note: string;
}

export interface RemarkList {
    id: string;
    agent: AgentList;
    note: string;
    hover: boolean;
    edit: boolean;
}

export interface AgentList {
    id: string;
    name: string;
}


