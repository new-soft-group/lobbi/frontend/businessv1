export enum AppointmentStatus {
    PENDING = 0,
    ACCEPTED = 1,
    REJECTED = 2,
    CANCELLED = 3
}
