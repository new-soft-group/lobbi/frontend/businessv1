export enum ConversationMemberStatus {
    PENDING = 0,
    ACTIVE = 1,
    LEFT = 2,
    REMOVED = 3
}
