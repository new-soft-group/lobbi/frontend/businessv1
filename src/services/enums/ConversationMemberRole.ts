export enum ConversationMemberRole {
    MEMBER = 1,
    ADMIN = 2
}
