export enum ImageType {
    IMAGE = 1,
    COVER = 2,
    BANNER = 3
}
