export enum ConversationType {
    SINGLE = 1,
    GROUP = 2,
    BROADCAST = 3
}
