export enum TabType {
    CHATTING = 'chatting',
    ONGOING = 'ongoing',
    CLIENTS = 'clients'
}
