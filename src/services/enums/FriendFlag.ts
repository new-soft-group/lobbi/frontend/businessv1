export enum FriendFlag {
    UNFRIEND = 0,
    PENDING_REQUEST = 1,
    PENDING_RESPONSE = 2,
    ACCEPTED = 3
}
