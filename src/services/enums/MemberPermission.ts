export enum MemberPermission {
    ISSUETYPE_CREATE = 'issueType/create',
    ISSUETYPE_UPDATE = 'issueType/update',
    ISSUETYPE_DELETE = 'issueType/delete',

    CONVERSATION_LIST = 'conversation/list',
    CONVERSATION_JOIN = 'conversation/join',
    CONVERSATION_TRANSFER = 'conversation/transfer',
    CONVERSATION_UPDATE = 'conversation/update',
    CONVERSATION_END = 'conversation/end',
    CONVERSATION_TRANSFER_FORCE = 'conversation/transfer/force',
    CONVERSATION_TRANSFER_CANCEL = 'conversation/transfer/cancel',

    // REMARK_CREATE = 'remark/create',
    // REMARK_UPDATE = 'remark/update',
    // REMARK_DELETE = 'remark/delete',

    KNOWLEDGEBASE_CREATE = 'knowledgebase/create',
    KNOWLEDGEBASE_UPDATE = 'knowledgebase/update',
    KNOWLEDGEBASE_DELETE = 'knowledgebase/delete',

    LABEL_LIST = 'label/list',
    LABEL_CREATE = 'label/create',
    LABEL_UPDATE = 'label/update',
    LABEL_DELETE = 'label/delete',

    AUDITLOG_LIST = 'auditLog/list',

    SERVICE_ENABLE = 'service/enable',
    SERVICE_DISABLE = 'service/disable',
    SERVICE_ENDPOINT = 'service/endpoint',

    ROLE_LIST = 'role/list',
    ROLE_CREATE = 'role/create',
    ROLE_VIEW = 'role/view',
    ROLE_UPDATE = 'role/update',
    ROLE_DELETE = 'role/delete',

    MEMBER_LIST = 'member/list',
    MEMBER_INVITE = 'member/invite',
    MEMBER_URL = 'member/url',
    MEMBER_UPDATE = 'member/update',
    MEMBER_DELETE = 'member/delete',

    DEPARTMENT_LIST = 'department/list',
    DEPARTMENT_CREATE = 'department/create',
    DEPARTMENT_VIEW = 'department/view',
    DEPARTMENT_UPDATE = 'department/update',
    DEPARTMENT_DELETE = 'department/delete',

    BRANCH_LIST = 'branch/list',
    BRANCH_CREATE = 'branch/create',
    BRANCH_VIEW = 'branch/view',
    BRANCH_UPDATE = 'branch/update',
    BRANCH_DELETE = 'branch/delete',

    BRANCHMEMBER_CREATE = 'branchMember/create',
    BRANCHMEMBER_UPDATE = 'branchMember/update',
    BRANCHMEMBER_DELETE = 'branchMember/delete'
}
