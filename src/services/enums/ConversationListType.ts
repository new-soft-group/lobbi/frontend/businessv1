export enum ConversationListType {
    PERSONAL = 1,
    GROUP_BROADCAST = 2
}
