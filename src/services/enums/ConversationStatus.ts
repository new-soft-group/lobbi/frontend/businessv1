export enum ConversationStatus {
    WAITING = 0,
    ACTIVE = 1,
    CLOSED = 2,
    PENDING = 3,
    // Not inside api
    NEW = 4
}
