export enum MessageDeleteFlag {
	EXIST = 0,
	DELETE_SELF = 1,
	DELETE_ALL = 2,
}
