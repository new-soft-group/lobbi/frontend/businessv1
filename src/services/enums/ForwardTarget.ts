import { Message } from '../models/Message';

export interface ForwardTarget {
    id: string;
    name: string;
    searchText: string;
    imageUrl: string;
    type: 'single' | 'group' | 'broadcast' | 'friend' | 'message';
    message?: Message;
    updatedAt?: Date;
    isBlocked?: boolean;
}
