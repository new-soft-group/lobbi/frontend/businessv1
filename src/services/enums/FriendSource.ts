export enum FriendSource {
    SEARCH = 1,
    QR = 2,
    GROUP = 3,
    MOMENT = 4,
    PERSONAL = 5
}
