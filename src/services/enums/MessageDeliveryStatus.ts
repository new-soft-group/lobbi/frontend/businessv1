export enum MessageDeliveryStatus {
	PENDING = 0,
	SENT = 1,
	DELIVERED = 2,
	READ = 3,
	HIDDEN_READ = -3,
}