export enum MessageType {
    SYSTEM = 0,
    TEXT = 1,
    IMAGE = 2,
    VIDEO = 3,
    DOCUMENT = 4,
    AUDIO = 5,
    STICKER = 6,
    GIF = 7,
    CONTACT = 8,
    LOCATION = 9,
    LIVE_LOCATION = 10,
    EVENT_REMINDER = 11,
    MOMENT = 12,
    POLL = 13
}
