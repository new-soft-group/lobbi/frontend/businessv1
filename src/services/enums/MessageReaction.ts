export enum MessageReaction {
	LOVE = 'love',
	LIKE = 'like',
	HAPPY = 'happy',
	SAD = 'sad',
	SHOCK = 'shock',
	ANGRY = 'angry',
}
