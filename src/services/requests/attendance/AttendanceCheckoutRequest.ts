export interface AttendanceCheckoutRequestBody {
    attendanceId: string;
    remarks?: string;
}
