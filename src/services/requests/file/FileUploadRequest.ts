export interface FileUploadRequestQuery {
    thumbnail?: boolean;
    avatar?: boolean;
}
