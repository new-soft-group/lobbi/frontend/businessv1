export interface CategoryQuery {
    businessId: string;
}
export interface CreateCategoryBody {
    name: string;
    parentId?: string;
}
export interface EditCategoryBody {
    knowledgebaseCategoryId: string;
    name?: string;
    parentId?: string;
}
export interface DeleteCategoryBody {
    knowledgebaseCategoryId: string;
}
