export interface KnowledgeQuery {
    businessId: string;
}
export interface CreateKnowledgeBody {
    title: string;
    content: string;
    knowledgebaseCategoryId: string;
}
export interface EditKnowledgeBody {
    knowledgebaseId: string;
    title?: string;
    content?: string;
    knowledgebaseCategoryId?: string;
}
export interface DeleteKnowledgeBody {
    knowledgebaseId: string;
}
