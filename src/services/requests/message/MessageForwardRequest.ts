export interface MessageForwardRequestBody {
	messageIds: string[];
	conversationIds: string[];
	userIds: string[];
}