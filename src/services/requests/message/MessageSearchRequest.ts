import { MessageType } from 'src/services/enums/MessageType';

export interface MessageSearchRequestQuery {
    keyword?: string;
    conversationId?: string;
    type?: MessageType;
    page?: number;
    limit?: number;
    branchId?: string;
}
