import { MessageType } from 'src/services/enums/MessageType';

export interface MessageCreateRequestBody {
    conversationId?: string;
    userId?: string;
    localId: string;
    type: MessageType;
    text?: string;
    mentionedUserIds?: string[];
    forwardedMessageId?: string;
    repliedMessageId?: string;
    momentId?: string;
    reminderDateTime?: Date;
    locationName?: string;
    locationAddress?: string;
    locationCoordinate?: string;
    contactUserIds?: string[];
    gifUrl?: string;
    gifDimension?: string;
    filePath?: string;
    thumbnailPath?: string;
    duration?: number;
    createdAt?: Date;
}
