import { MessageDeleteFlag } from 'src/services/enums/MessageDeleteFlag';

export interface MessageDeleteRequestBody {
	messageIds: string[];
	type: MessageDeleteFlag;
}