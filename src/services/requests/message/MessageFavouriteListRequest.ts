export interface MessageFavouriteListRequestQuery {
	conversationId?: string;
	page?: number;
	limit?: number;
}