export interface MessageSyncRequestQuery {
	startDate?: Date;
	endDate?: Date;
	page?: number;
	limit?: number;
}