import { MessageDeliveryStatus } from 'src/services/enums/MessageDeliveryStatus';

export interface MessageStatusRequestBody {
	messageIds: string[];
	value: MessageDeliveryStatus;
}