import { MessageType } from 'src/services/enums/MessageType';

export interface MessageListRequestQuery {
    conversationId?: string;
    offsetDate?: Date | undefined;
    limit?: number;
    isDownward?: 0 | 1;
    types?: MessageType[];
    isUnread?: 0 | 1;
    unshift?: boolean;
}

export interface MessageHistoryListRequestQuery {
    userId: string;
    isDownward?: 0 | 1;
    page: number;
}
