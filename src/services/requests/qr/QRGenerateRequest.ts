export interface QRGenerateRequestBody {
    type: 'weblogin';
    value: string;
}
