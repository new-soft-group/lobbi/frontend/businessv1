export interface MemberListQuery {
    page?: number;
    limit?: number;
    roleId?: string;
    keyword?: string;
    status: number;
    type?: string;
}

export interface MemberUpdateRequestBody {
    memberId: string;
    roleId: string;
    branchIds: [];
}
