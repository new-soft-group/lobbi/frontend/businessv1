export interface MemberInviteUserRequestBody {
    userIds: [];
    branchIds: [];
    roleId: string;
}
