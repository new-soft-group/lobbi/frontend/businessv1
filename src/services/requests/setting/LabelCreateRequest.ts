export interface LabelRequestBody {
    labelId?: string;
    name: string;
    color: string;
    icon: string;
}
