import { UserSearchType } from 'src/services/enums/UserSearchType';

export interface UserSearchRequestQuery {
    type: UserSearchType;
    value: string;
}
