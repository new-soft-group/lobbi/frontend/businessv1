export interface AuditLogQuery {
    userId: string;
    startDateTime: string;
    endDateTime: string;
    page?: number;
    limit?: number;
}
