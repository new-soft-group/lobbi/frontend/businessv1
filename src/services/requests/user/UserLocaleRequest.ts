export interface UserLocaleRequestBody {
    locale: 'en' | 'my' | 'zh';
}
