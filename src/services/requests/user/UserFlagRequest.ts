export interface UserFlagRequestParam {
    name:
        | 'contact'
        | 'message'
        | 'password'
        | 'tutorial'
        | 'notification'
        | 'receipt';
}

export interface UserFlagRequestBody {
    value: 0 | 1;
}
