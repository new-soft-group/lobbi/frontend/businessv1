export interface UserUpdateRequestBody {
    name?: string;
    username?: string;
    description?: string;
}
