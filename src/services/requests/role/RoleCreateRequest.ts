export interface RoleBody {
    name: string;
    permissions: [];
    color: string;
}
export interface UpdateRoleBody {
    roleId?: string;
    name: string;
    permissions: [];
    color: string;
}
export interface DeleteRoleBody {
    roleId?: string;
}
export interface RoleListQuery {
    page?: number;
    limit?: number;
    roleId?: string;
    keyword?: string;
}
