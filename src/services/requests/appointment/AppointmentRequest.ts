export interface AppointmentQuery {
    page?: number;
    limit?: number;
    keyword?: string;
    userId?: string;
    serviceTypeIds?: any;
}

export interface createAppointmentBody {
    businessId: string;
    branchId: string;
    userId: string;
    selectedDate: Date;
    startTime: Date;
    endTime: Date;
    remark?: string;
    serviceTypeIds: any;
}

export interface updateAppointmentStatusBody {
    status: number;
    appointmentId: string;
}
