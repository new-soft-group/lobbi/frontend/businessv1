export interface IssueTypeQuery {
    businessId: string;
}
export interface CreateIssueTypeBody {
    name: string;
    orderNo?: number;
}
export interface EditIssueTypeBody {
    issueTypeId: string;
    name?: string;
    orderNo?: number;
}
export interface DeleteIssueTypeBody {
    issueTypeId: string;
}
