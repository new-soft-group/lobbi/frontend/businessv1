export interface createBranchBody {
    name?: string;
    address?: address;
    contacts?: contacts[];
    slug?: string;
    imagePath?: string;
    coverPath?: string;
    description?: string;
    tags?: [];
    status?: number;
}

export interface updateBranchBody {
    branchId?: string;
    name?: string;
    address?: address;
    contacts?: contacts[];
    slug?: string;
    imagePath?: string;
    coverPath?: string;
    description?: string;
    tags?: [];
    status?: number;
}

export interface deleteBranchBody {
    branchId?: string;
}

export interface address {
    street: string;
    postcode: string;
    city: string;
    state: string;
    country: string;
}
export interface contacts {
    type: string;
    value: string;
}

export interface branchQuery {
    keyword?: string;
    page?: number;
    limit?: number;
}
