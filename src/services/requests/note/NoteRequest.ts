export interface NoteQuery {
    userId: string;
}

export interface EditNoteBody {
    userId: string;
    note: string
}