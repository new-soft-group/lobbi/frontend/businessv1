export interface ServiceQuery {
    businessId: string;
}
export interface CreateServiceBody {
    name: string;
    parentId?: string;
}
export interface EditServiceBody {
    serviceTypeId: string;
    name?: string;
    parentId?: string;
}
export interface DeleteServiceBody {
    serviceTypeId: string;
}
