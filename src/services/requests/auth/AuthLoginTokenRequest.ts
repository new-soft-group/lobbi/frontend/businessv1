export interface AuthLoginTokenRequestBody {
    token: string;
}
