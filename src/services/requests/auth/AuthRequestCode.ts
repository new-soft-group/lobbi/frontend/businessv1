export interface AuthRequestCodeBody {
    phone: string;
    code: string;
}
