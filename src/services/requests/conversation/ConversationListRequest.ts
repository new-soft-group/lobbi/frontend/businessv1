export interface ConversationListRequestQuery {
    offsetDate?: Date;
    limit?: number;
    page?: number;
    isDownward?: number;
    issueTypeIds?: string;
    statuses?: any;
    status?: number;
    userId?: string;
    agentId?: string;
}
export interface ConversationListQuery {
    limit?: number;
    isDownward?: number;
    status?: number;
}

export interface ConversationTransferBody {
    conversationId?: number;
    userId?: number;
}

export interface ConversationRequestBody {
    conversationId?: number;
}

export interface ConversationCancelTransferBody {
    transferId?: string;
}

export interface ConversationJoinBody {
    conversationId?: string;
}

export interface ConversationEndBody {
    conversationId?: string;
}

export interface ConversationAssignLabel {
    conversationId: string;
    labelIds: any;
}

export interface CreateConversationRequestBody {
    issueTypeId: string;
    description: string;
    localId: string;
    userId: string;
}
export interface ConversationUpdateIssueType {
    conversationId: string;
    issueTypeId: string;
}
