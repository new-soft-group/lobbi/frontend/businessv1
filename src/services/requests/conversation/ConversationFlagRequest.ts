export interface ConversationFlagRequestParam {
	flag: 'pin' | 'notification';
}

export interface ConversationFlagRequestBody {
	conversationId: string;
	value: 0 | 1;
}
