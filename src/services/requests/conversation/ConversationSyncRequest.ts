export interface ConversationSyncRequestQuery {
	startDate?: Date;
	endDate?: Date;
	page?: number;
	limit?: number;
}