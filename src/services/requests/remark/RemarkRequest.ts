export interface RemarkQuery {
    userId: string;
}
export interface CreateRemarkBody {
    userId: string;
    note: string;
}
export interface EditRemarkBody {
    userId: string;
    note?: string;
}
export interface DeleteRemarkBody {
    remarkId: string;
}
