export interface updateBusinessBody {
    description?: string;
    imagePath?: string;
    coverPath?: string;
    bannerPaths?: string;
}
export interface deactivateBusinessBody {
    type: string;
}

export interface CustomerQuery {
    agentId?: string;
    labelIds: [];
    keyword?: string;
    page?: number;
    limit?: number;
}
export interface createCustomerBody {
    name?: string;
    phone?: string;
}
