/* eslint-disable @typescript-eslint/restrict-plus-operands */
/* eslint-disable @typescript-eslint/no-unsafe-return */
/* eslint-disable @typescript-eslint/no-floating-promises */
/* eslint-disable @typescript-eslint/no-unsafe-call */
/* eslint-disable @typescript-eslint/no-unsafe-member-access */
/* eslint-disable @typescript-eslint/no-unsafe-assignment */
import { Message, Recipient } from 'src/services/models/Message';
import { MutationTree } from 'vuex';
import { MessageStateInterface } from './state';
import { keyBy, orderBy, size, merge, stubArray } from 'lodash';
import { User } from 'src/services/models/User';
import localForage from 'localforage';
import { MessageStatusRequestBody } from 'src/services/requests/message/MessageStatusRequest';

const messageStore = localForage.createInstance({
    name: 'lobbi/messages'
});

const newMessageStore = localForage.createInstance({
    name: 'lobbi/newMessage'
});

const mutation: MutationTree<MessageStateInterface> = {
    setMessages(state, data: { messages: Message[]; conversationId: string }) {
        const { messages, conversationId } = data;

        state.messages = keyBy<Message>(messages, 'id');
        messageStore.setItem(conversationId, JSON.stringify(state.messages));
    },

    putMessages(state, data: { messages: Message[]; conversationId: string }) {
        const { messages, conversationId } = data;

        messages.forEach((message: Message) => {
            state.messages[message.id] = message;
            state.messages[message.id].createdAt = new Date(message.createdAt);
            messageStore.setItem(
                conversationId,
                JSON.stringify(state.messages)
            );
        });
    },
    setMessagesHistory(state, data: { messages: Message[] }) {
        const { messages } = data;

        state.messagesHistory = keyBy<Message>(messages, 'id');
    },
    putMessagesHistory(state, data: { messages: Message[] }) {
        const { messages } = data;

        messages.forEach((message: Message) => {
            state.messagesHistory[message.id] = message;
        });
    },
    cutMessages(state, data: { limit: number; conversationId: string }) {
        const { limit, conversationId } = data;

        if (size(state.messages) > limit) {
            const messages: Message[] = orderBy(
                state.messages,
                ['createdAt'],
                ['desc']
            );

            const newMessages = keyBy<Message>(messages.splice(0, limit), 'id');

            if (size(newMessages) > 0) {
                state.messages = newMessages;
                messageStore.setItem(
                    conversationId,
                    JSON.stringify(state.messages)
                );
            }
        }
    },
    cutMessagesHistory(state, data: { limit: number; conversationId: string }) {
        const { limit, conversationId } = data;

        if (size(state.messagesHistory) > limit) {
            const messages: Message[] = orderBy(
                state.messagesHistory,
                ['createdAt'],
                ['desc']
            );

            const newMessages = keyBy<Message>(messages.splice(0, limit), 'id');

            if (size(newMessages) > 0) {
                // state.messagesHistory = newMessages;

                messageStore.setItem(
                    conversationId,
                    JSON.stringify(state.messagesHistory)
                );
            }
        }
    },
    putLatestMessage(state, message: Message) {
        state.messages[message.id] = message;
        state.messages[message.id].createdAt = new Date(message.createdAt);
        delete state.messages[message.localId ?? ''];
        messageStore.setItem(
            message.conversation?.id ?? message.conversationId!,
            JSON.stringify(state.messages)
        );
    },
    putLatestMessageHistory(state, message: Message) {
        // state.messagesHistory[message.id] = message;
        // delete state.messagesHistory[message.localId ?? ''];
        messageStore.setItem(
            message.conversation?.id ?? message.conversationId!,
            JSON.stringify(state.messagesHistory)
        );
    },
    setMessageStatus(state, messageStatus: any) {
        state.messageStatus = messageStatus;
    },
    clearMessageStatus(state) {
        state.messageStatus = 200;
    },
    increaseMessageListFlag(state) {
        state.messageListFlag++;
    },
    reset(state) {
        state.messages = {};
        state.messageListFlag = 0;
    },
    resetHistory(state) {
        state.messagesHistory = {};
    },
    update(state, message: Message) {
        if (state.messages[message.id]) {
            state.messages[message.id] = merge(
                state.messages[message.id],
                message
            );
            messageStore.setItem(
                message.conversation?.id ?? message.conversationId!,
                JSON.stringify(state.messages)
            );
        }
    },
    status(
        state,
        data: { messageId: string; recipient: Recipient; user: User }
    ) {
        const { messageId, user, recipient } = data;

        const message = state.messages[messageId];
        if (message) {
            let newMessage: Message = message;

            // Find recipient
            const recipientIndex = newMessage.recipients!.findIndex(
                (r) => r.userId === recipient.userId
            );
            if (recipientIndex < 0) {
                return;
            }

            // Check if user is recipient
            if (recipient.userId != user.id) {
                let key = '';
                let allUpdated = 0;

                if (recipient.deliveredAt) {
                    newMessage.recipients![recipientIndex].deliveredAt =
                        recipient.deliveredAt;
                    key = 'deliveredAt';
                    allUpdated = newMessage.recipients!.findIndex(
                        (r) => !r.deliveredAt && r.userId !== user.id
                    );
                } else if (recipient.readAt) {
                    newMessage.recipients![recipientIndex].readAt =
                        recipient.readAt;
                    key = 'readAt';
                    allUpdated = newMessage.recipients!.findIndex(
                        (r) =>
                            !r.readAt && !r.hiddenReadAt && r.userId !== user.id
                    );
                } else if (recipient.hiddenReadAt) {
                    newMessage.recipients![recipientIndex].hiddenReadAt =
                        recipient.hiddenReadAt;
                    key = 'hiddenReadAt';
                    allUpdated = newMessage.recipients!.findIndex(
                        (r) =>
                            !r.readAt && !r.hiddenReadAt && r.userId !== user.id
                    );
                }

                if (allUpdated < 0) {
                    if (key === 'deliveredAt') {
                        newMessage.deliveredAt = new Date();
                    } else if (key === 'readAt') {
                        newMessage.readAt = new Date();
                        if (!newMessage.deliveredAt) {
                            newMessage.deliveredAt = new Date();
                        }
                    } else if (key === 'hiddenReadAt') {
                        newMessage.hiddenReadAt = new Date();
                        if (!newMessage.deliveredAt) {
                            newMessage.deliveredAt = new Date();
                        }
                    }
                }
            } else {
                newMessage = { ...newMessage, ...recipient };
            }

            state.messages[message.id] = merge(
                state.messages[message.id],
                newMessage
            );
            messageStore.setItem(
                message.conversation?.id ?? message.conversationId!,
                JSON.stringify(state.messages)
            );
        }
    },
    favouriteFlag(state, body: MessageStatusRequestBody) {
        const { messageIds, value } = body;

        messageIds.forEach((messageId) => {
            if (state.messages[messageId]) {
                state.messages[messageId].flags = merge(
                    state.messages[messageId].flags ?? {},
                    {
                        favouriteFlag: value
                    }
                );
            }
        });
    },
    clearAllMessagesHistory(state) {
        state.messages = {};
        state.messagesHistory = {};
        messageStore.setItem(
            'messagesHistory',
            JSON.stringify(state.messagesHistory)
        );
    },
    newMessage(state, conversationId: string) {
        state.newMessageList.push(conversationId);
        newMessageStore.setItem(
            'newMessageList',
            JSON.stringify(state.newMessageList)
        );
    },
    clearNewMessage(state, conversationId: string) {
        const findIndex = state.newMessageList.findIndex(
            (element: any) => element == conversationId
        );
        if (findIndex != -1) {
            state.newMessageList.splice(findIndex, 1);
        }
        newMessageStore.setItem(
            'newMessageList',
            JSON.stringify(state.newMessageList)
        );
    }
};

export default mutation;
