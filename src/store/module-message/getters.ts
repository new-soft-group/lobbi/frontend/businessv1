import { GetterTree } from 'vuex';
import { StateInterface } from '../index';
import { MessageStateInterface } from './state';

const getters: GetterTree<MessageStateInterface, StateInterface> = {
    getNewMessageList(state) {
        return state.newMessageList;
    }
};

export default getters;
