/* eslint-disable @typescript-eslint/no-unsafe-return */
/* eslint-disable @typescript-eslint/no-floating-promises */
/* eslint-disable @typescript-eslint/no-unsafe-call */
/* eslint-disable @typescript-eslint/no-unsafe-member-access */
/* eslint-disable @typescript-eslint/no-unsafe-assignment */
import { ActionTree } from 'vuex';
import { StateInterface } from '../index';
import { MessageStateInterface } from './state';
import MessageApi from 'src/services/apis/MessageApi';
import { MessageListResponseResult } from 'src/services/responses/message/MessageListResponse';
import {
    MessageListRequestQuery,
    MessageHistoryListRequestQuery
} from 'src/services/requests/message/MessageListRequest';
import { Message, Recipient, File } from 'src/services/models/Message';
import { Conversation } from 'src/services/models/Conversation';
import { User } from 'src/services/models/User';
import { MessageCreateRequestBody } from 'src/services/requests/message/MessageCreateRequest';
import localForage from 'localforage';
import { orderBy, toArray } from 'lodash';
import { Howl } from 'howler';
import { MessageType } from 'src/services/enums/MessageType';
import { MessageDeleteRequestBody } from 'src/services/requests/message/MessageDeleteRequest';
import { MessageDeleteFlag } from 'src/services/enums/MessageDeleteFlag';
import { MessageStatusRequestBody } from 'src/services/requests/message/MessageStatusRequest';
import { MessageForwardRequestBody } from 'src/services/requests/message/MessageForwardRequest';
import { MessageSearchRequestQuery } from 'src/services/requests/message/MessageSearchRequest';
import { MessageFavouriteListRequestQuery } from 'src/services/requests/message/MessageFavouriteListRequest';
import { AppVisibility } from 'quasar';

const messageStore = localForage.createInstance({
    name: 'lobbi/messages'
});

const messageApi = new MessageApi();
const receiveAudio = new Howl({
    src: ['/audio/received.mp3'],
    volume: 0.2
});
const sentAudio = new Howl({
    src: ['/audio/sent.mp3']
});

const actions: ActionTree<MessageStateInterface, StateInterface> = {
    async getMessages(context, query: MessageListRequestQuery) {
        let selectedConversation: Conversation = context.rootState.chat
            .selectedConversation as Conversation;
        if (!query.offsetDate) {
            const oldMessages: string | null = await messageStore.getItem(
                query.conversationId!
            );
            if (oldMessages) {
                const messages = toArray(JSON.parse(oldMessages));
                if (messages.length > 0) {
                    if (
                        selectedConversation?.id ==
                        (messages[0].conversation?.id ??
                            messages[0].conversationId)
                    ) {
                        context.commit('putMessages', {
                            messages,
                            conversationId:
                                messages[0].conversation?.id ??
                                messages[0].conversationId
                        });
                    }
                }
            }
        }

        // Get Messages
        const result: MessageListResponseResult = (await messageApi.list(
            query
        )) as MessageListResponseResult;
        if (!result) {
            console.error('Error retrieving message list');
            return;
        }

        // Retrieve new selectedConversation just in case changed after API
        selectedConversation = context.rootState.chat
            .selectedConversation as Conversation;

        if (result.messages.length > 0) {
            if (
                selectedConversation?.id ==
                (result.messages[0].conversation?.id ??
                    result.messages[0].conversationId)
            ) {
                if (!query.offsetDate) {
                    context.commit('setMessages', {
                        messages: result.messages,
                        conversationId:
                            result.messages[0].conversation?.id ??
                            result.messages[0].conversationId
                    });
                } else {
                    context.commit('putMessages', {
                        messages: result.messages,
                        conversationId:
                            result.messages[0].conversation?.id ??
                            result.messages[0].conversationId
                    });
                }
            }

            if (query.isDownward && !query.offsetDate) {
                context.dispatch(
                    'conversation/overrideLatestMessage',
                    result.messages[result.messages.length - 1],
                    { root: true }
                );
            }
        } else {
            if (!query.offsetDate) {
                context.commit('setMessages', {
                    messages: [],
                    conversationId: selectedConversation.id
                });
            } else if (query.isDownward) {
                context.commit('cutMessages', {
                    limit: 50,
                    conversationId: selectedConversation.id
                });
            }
        }

        return result;
    },
    async getMessagesHistory(context, query: MessageHistoryListRequestQuery) {
        // Get Messages
        const result: MessageListResponseResult = (await messageApi.historyList(
            query
        )) as MessageListResponseResult;

        context.commit('putMessagesHistory', { messages: result.messages });

        return result;
    },
    async list(context, query: MessageListRequestQuery) {
        try {
            return (await messageApi.list(query)) as MessageListResponseResult;
        } catch (err: any) {
            return err.response.data;
        }
    },
    async sendMessage(
        context,
        data: {
            body: MessageCreateRequestBody;
            repliedMessage?: Message;
            file?: File;
            thumbnail?: File;
            silent?: boolean;
        }
    ) {
        const { body, repliedMessage, file, thumbnail } = data;
        let message: any | undefined;

        if (body.type == MessageType.TEXT && body.text == '') {
            return;
        }

        const user: User = context.rootState.user.user as User;
        const selectedConversation: Conversation = context.rootState.chat
            .selectedConversation as Conversation;

        if ((data.silent && data.file) || !data.file) {
            // Initialize message object locally
            const newMessage: Message = {
                id: body.localId,
                userId: user.id,
                conversationId: body.conversationId,
                localId: body.localId,
                type: body.type,
                text: body.text,
                repliedMessage,
                reminderDateTime: body.reminderDateTime,
                isForwarded: body.forwardedMessageId ? 1 : 0,
                flags: {
                    deleteFlag: 0,
                    favouriteFlag: 0
                },
                location: body.locationCoordinate
                    ? {
                          name: body.locationName,
                          address: body.locationAddress,
                          coordinate: body.locationCoordinate,
                          isActive: 1
                      }
                    : undefined,
                createdAt: body.createdAt!
            };

            if (body.type == MessageType.GIF) {
                const dimension = body.gifDimension!.split('x');
                newMessage.file = {
                    url: body.gifUrl,
                    width: dimension[0] as any,
                    height: dimension[1] as any
                };
            }

            if (file) {
                newMessage.file = file;
            }

            if (thumbnail) {
                newMessage.thumbnail = thumbnail;
            }

            // Store the message locally first
            if (selectedConversation?.id == newMessage.conversationId) {
                context.commit('putMessages', {
                    messages: [newMessage],
                    conversationId: body.conversationId
                });
                context.commit('increaseMessageListFlag');
                context.dispatch('conversation/putLatestMessage', newMessage, {
                    root: true
                });
                context.dispatch('app/incrementScrollCounter', null, {
                    root: true
                });
            }
        }

        if (!data.silent) {
            try {
                message = (await messageApi.create(body)) as Message;
                if (!message) {
                    console.error('Error send message');
                    return;
                }

                sentAudio.play();
                context.commit('setMessageStatus', message?.status);
                if (message.result) {
                    context.commit('putLatestMessage', message.result.message);
                }
                context.commit('increaseMessageListFlag');
                context.dispatch('conversation/putLatestMessage', message, {
                    root: true
                });
                context.dispatch('app/incrementScrollCounter', null, {
                    root: true
                });
                context.dispatch(
                    'conversation/updateTime',
                    message?.result?.message,
                    {
                        root: true
                    }
                );
            } catch (err: any) {
                console.error('Error send message');
                console.log(err);

                context
                    .dispatch('delete', {
                        body: {
                            messageIds: [body.localId],
                            type: MessageDeleteFlag.DELETE_SELF
                        },
                        silent: true
                    })
                    .then(() => {
                        context.commit('increaseMessageListFlag');
                    });
            }
        }

        return message;
    },
    putLatestMessage(context, message: Message) {
        const selectedConversation: Conversation = context.rootState.chat
            .selectedConversation as Conversation;
        const user: User = context.rootState.user.user as User;

        if (
            selectedConversation?.id ==
            (message.conversation?.id ?? message.conversationId ?? '')
        ) {
            context.commit('putLatestMessage', message);

            if (AppVisibility.appVisible) {
                context.dispatch(
                    'conversation/clearTotalUnread',
                    selectedConversation,
                    { root: true }
                );
            }
        }
    },
    update(context, message: Message) {
        context.commit('update', message);
    },
    status(context, data: { messageId: string; recipient: Recipient }) {
        const user: User = context.rootState.user.user as User;
        context.commit('status', { ...data, user });
    },
    reset(context) {
        context.commit('reset');
    },
    resetHistory(context) {
        context.commit('resetHistory');
    },
    async delete(
        context,
        data: { body: MessageDeleteRequestBody; silent: boolean }
    ) {
        const { body, silent } = data;
        const message: Message = context.state.messages[body.messageIds[0]];

        try {
            let result: any = { status: 200 };

            if (!silent) {
                result = await messageApi.delete(body);
            }

            if (result?.status == 200) {
                if (message) {
                    const conversationId =
                        message.conversation?.id ??
                        message.conversationId ??
                        '';
                    const conversation =
                        context.rootState.conversation.conversations[
                            conversationId
                        ];
                    if (conversation.message) {
                        if (
                            body.messageIds.includes(conversation.message?.id)
                        ) {
                            const _messages: Message[] = orderBy(
                                context.state.messages,
                                ['createdAt'],
                                ['desc']
                            );

                            const _message = _messages.filter((msg) => {
                                return (
                                    (msg.conversation?.id ??
                                        msg.conversationId) == conversationId &&
                                    msg.flags?.deleteFlag !=
                                        MessageDeleteFlag.DELETE_SELF
                                );
                            })[0];

                            context.dispatch(
                                'conversation/overrideLatestMessage',
                                _message,
                                { root: true }
                            );
                        }
                    }
                }
            }

            return result;
        } catch (err: any) {
            if (err.response) {
                return err.response.data;
            }
            return err;
        }
    },
    favouriteFlag(context, body: MessageStatusRequestBody) {
        context.commit('favouriteFlag', body);
        messageApi.favourite(body).catch(console.error);
    },
    forward(context, body: MessageForwardRequestBody) {
        messageApi.forward(body).catch(console.error);
    },
    async search(context, query: MessageSearchRequestQuery) {
        try {
            return await messageApi.search(query);
        } catch (err: any) {
            return {
                messages: [],
                pagination: null
            };
        }
    },
    async favouriteList(context, query: MessageFavouriteListRequestQuery) {
        try {
            return await messageApi.favouriteList(query);
        } catch (err: any) {
            return {
                messages: [],
                pagination: null
            };
        }
    },
    clearAllMessagesHistory(context) {
        context.commit('clearAllMessagesHistory');
    },
    async searchRelation(context, query: any) {
        try {
            return await messageApi.searchRelation(query);
        } catch (err: any) {
            return err.response.data;
        }
    },
    resendMessage(context) {
        const _messages: Message[] = orderBy(
            context.state.messages,
            ['createdAt'],
            ['desc']
        );

        _messages.filter((msg) => {
            if (msg.id == msg.localId) {
                context.dispatch('sendMessage', { body: msg });
            }
        });
    },
    resendErrorMessage(context, id) {
        const _messages: Message[] = orderBy(
            context.state.messages,
            ['createdAt'],
            ['desc']
        );

        _messages.filter((msg) => {
            if (msg.id == id && msg.id == msg.localId) {
                context.dispatch('sendMessage', { body: msg });
            }
        });
    },
    newMessage(context, message: Message) {
        const user: User = context.rootState.user.user as User;

        if (user.id != message.user?.id) {
            const existingNotificationList =
                context.rootState.notification.list.filter(
                    (element: any) => element.id == message.id
                );

            // Only play notification sound when notification list does not consists this message
            if (existingNotificationList.length == 0) {
                receiveAudio.play();
            }

            // Don't push when already have this conversation id (Because is already showing the indicator)
            const existingList =
                context.rootState.message.newMessageList.filter(
                    (element: any) => element == message.conversationId
                );

            if (existingList.length == 0) {
                context.commit('newMessage', message.conversationId);
            }
        }
    },
    clearNewMessage(context, conversationId: string) {
        context.commit('clearNewMessage', conversationId);
    },
    checkMessageLength(context, conversationId: string) {
        const data = {
            limit: 50,
            conversationId: conversationId
        };
        context.commit('cutMessages', data);
    }
};

export default actions;
