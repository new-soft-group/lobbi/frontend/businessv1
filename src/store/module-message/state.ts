import { Dictionary } from 'lodash';
import { Message } from 'src/services/models/Message';

export interface MessageStateInterface {
    messages: Dictionary<Message>;
    messagesHistory: Dictionary<Message>;
    messageListFlag: number;
    messageStatus: number;
    newMessageList: string[];
}

function state(): MessageStateInterface {
    return {
        messages: {},
        messagesHistory: {},
        messageListFlag: 0,
        messageStatus: 200,
        newMessageList: []
    };
}

export default state;
