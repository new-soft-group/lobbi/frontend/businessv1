/* eslint-disable @typescript-eslint/no-unsafe-assignment */
import { store } from 'quasar/wrappers';
import { InjectionKey } from 'vue';
import {
    createStore,
    Store as VuexStore,
    useStore as vuexUseStore
} from 'vuex';

import app from './module-app';
import { AppStateInterface } from './module-app/state';
import auth from './module-auth';
import { AuthStateInterface } from './module-auth/state';
import qr from './module-qr';
import { QrStateInterface } from './module-qr/state';
import user from './module-user';
import { UserStateInterface } from './module-user/state';
import menu from './module-menu';
import { MenuStateInterface } from './module-menu/state';
import member from './module-member';
import { MemberStateInterface } from './module-member/state';
import business from './module-business';
import { BusinessStateInterface } from './module-business/state';
import conversation from './module-conversation';
import { ConversationStateInterface } from './module-conversation/state';
import message from './module-message';
import { MessageStateInterface } from './module-message/state';
import chat from './module-chat';
import { ChatStateInterface } from './module-chat/state';
import issueType from './module-issueType';
import { IssueTypeStateInterface } from './module-issueType/state';
import notification from './module-notification';
import { NotificationStateInterface } from './module-notification/state';
import remark from './module-remark';
import { RemarkStateInterface } from './module-remark/state';
import attendance from './module-attendance';
import { AttendanceStateInterface } from './module-attendance/state';
import file from './module-file';
import { FileStateInterface } from './module-file/state';
import label from './module-label';
import { LabelStateInterface } from './module-label/state';
import role from './module-role';
import { RoleStateInterface } from './module-role/state';
import category from './module-category';
import { CategoryStateInterface } from './module-category/state';
import branch from './module-branch';
import { BranchStateInterface } from './module-branch/state';
import note from './module-note';
import { NoteStateInterface } from './module-note/state';
import filter from './module-filter';
import { FilterStateInterface } from './module-filter/state';
import appointment from './module-appointment';
import { AppointmentStateInterface } from './module-appointment/state';
import service from './module-service';
import { ServiceStateInterface } from './module-service/state';

export interface StateInterface {
    app: AppStateInterface;
    auth: AuthStateInterface;
    qr: QrStateInterface;
    user: UserStateInterface;
    menu: MenuStateInterface;
    member: MemberStateInterface;
    business: BusinessStateInterface;
    conversation: ConversationStateInterface;
    message: MessageStateInterface;
    chat: ChatStateInterface;
    issueType: IssueTypeStateInterface;
    remark: RemarkStateInterface;
    notification: NotificationStateInterface;
    attendance: AttendanceStateInterface;
    file: FileStateInterface;
    label: LabelStateInterface;
    role: RoleStateInterface;
    category: CategoryStateInterface;
    note: NoteStateInterface;
    filter: FilterStateInterface;
    branch: BranchStateInterface;
    appointment: AppointmentStateInterface;
    service: ServiceStateInterface;
}

// provide typings for `this.$store`
declare module '@vue/runtime-core' {
    interface ComponentCustomProperties {
        $store: VuexStore<StateInterface>;
    }
}

// provide typings for `useStore` helper
export const storeKey: InjectionKey<VuexStore<StateInterface>> =
    Symbol('vuex-key');

export default store(function (/* { ssrContext } */) {
    const Store = createStore<StateInterface>({
        modules: {
            app,
            auth,
            qr,
            user,
            menu,
            business,
            conversation,
            member,
            message,
            chat,
            issueType,
            notification,
            remark,
            attendance,
            file,
            label,
            role,
            category,
            note,
            filter,
            branch,
            appointment,
            service
        },

        // strict: !!process.env.DEBUGGING
        strict: false
    });

    return Store;
});

export function useStore() {
    return vuexUseStore(storeKey);
}
