import NoteApi from 'src/services/apis/NoteApi';
import {
    NoteQuery,
    EditNoteBody
} from 'src/services/requests/note/NoteRequest';
import { ActionTree } from 'vuex';
import { StateInterface } from '..';
import { NoteStateInterface } from './state';

const noteApi = new NoteApi();

const actions: ActionTree<NoteStateInterface, StateInterface> = {
    async getNoteByUserID(context, query: NoteQuery) {
        try {
            const res: any = await noteApi.list(query);
            if (res.result.note) {
                context.commit('setNote', res.result.note);
                return res;
            } else {
                context.commit('setNote', null);
                return null
            }
        } catch (err: any) {
            return err.response;
        }
    },
    async editNote(context, body: EditNoteBody) {
        try {
            const res: any = await noteApi.update(body);
            if (res.status == 200) {
                context.commit('putNote', res);
            }
            return res;
        } catch (err: any) {
            return err.response.data;
        }
    },

    clearNote(context) {
        context.commit('clearNote');
    }
};

export default actions;
