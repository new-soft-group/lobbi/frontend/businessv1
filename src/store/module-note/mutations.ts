import localforage from "localforage";
import { Note } from "src/services/models/Note";
import { MutationTree } from "vuex";
import { NoteStateInterface } from "./state";

const noteStore = localforage.createInstance({
    name: 'lobbi/note'
});

const mutation: MutationTree<NoteStateInterface> = {
    setNote(state, Note: Note) {
        const newNote = {
            id: Note.id,
            text: Note.text
        }
        state.note = newNote;
        noteStore.setItem('noteList', JSON.stringify(state.note));
    },
    putNote(state, Note: Note) {
        state.note = Note;
        noteStore.setItem('noteList', JSON.stringify(state.note));
    },
    clearNote(state) {
        state.note = { id: '', text: ''};
        noteStore.setItem('noteList', JSON.stringify(state.note));
    }
}

export default mutation;