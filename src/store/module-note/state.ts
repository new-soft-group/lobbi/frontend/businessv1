import { Note } from "src/services/models/Note";

export interface NoteStateInterface {
    note: Note;
}

function state(): NoteStateInterface {
    return {
        note: {
            id: '',
            text: ''
        }
    };
}

export default state;