import { GetterTree } from "vuex";
import { StateInterface } from "..";
import { NoteStateInterface } from "./state";

const getters: GetterTree<NoteStateInterface, StateInterface> = {
    someAction(/* context */) {
        // your code
    },
    getNoteList(state) {
        return state.note;
    }
};

export default getters;