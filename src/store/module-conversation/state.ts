import {
    ConversationList,
    allChatList
} from 'src/services/models/Conversation';
import { Dictionary } from 'lodash';
import { Conversation } from 'src/services/models/Conversation';

export interface ConversationStateInterface {
    conversationsList: ConversationList[];
    allChat: allChatList[];
    allClient: allChatList[];
    allChatPagination: any;
    allClientPagination: any;
    conversations: Dictionary<Conversation>;
    selectedConversationID?: string;
    selectedTicketID?: string;
    selectedUserID?: string;
    conversationsHistory: ConversationList[];
    conversationLength: number;
    socket: any;
    conversationsTransferList: any[];
    ticketConversation?: ConversationList;
    selectedConversation?: ConversationList;
    changeStatusConvo: any;
    assignSuccess: boolean;
    clearComponent: boolean;
    selectedIssueTypeID?: string;
    changeIssuesType: boolean;
}

function state(): ConversationStateInterface {
    return {
        conversationsList: [],
        allChatPagination: {},
        allClientPagination: {},
        allChat: [],
        allClient: [],
        conversations: {},
        conversationsHistory: [],
        conversationLength: 0,
        socket: {},
        conversationsTransferList: [],
        changeStatusConvo: {},
        assignSuccess: false,
        clearComponent: false,
        changeIssuesType: false
    };
}

export default state;
