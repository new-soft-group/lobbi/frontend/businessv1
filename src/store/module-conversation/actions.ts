/* eslint-disable @typescript-eslint/no-unsafe-return */
/* eslint-disable @typescript-eslint/no-floating-promises */
/* eslint-disable @typescript-eslint/no-unsafe-call */
/* eslint-disable @typescript-eslint/no-unsafe-member-access */
/* eslint-disable @typescript-eslint/no-unsafe-assignment */
/* eslint-disable @typescript-eslint/no-unsafe-call */
/* eslint-disable @typescript-eslint/no-unsafe-member-access */
/* eslint-disable @typescript-eslint/no-unsafe-return */
/* eslint-disable @typescript-eslint/no-unsafe-assignment */
import localForage from 'localforage';
import { keyBy } from 'lodash';
import { debounce } from 'quasar';
import ConversationApi from 'src/services/apis/ConversationApi';
import { allChatList, Conversation } from 'src/services/models/Conversation';
import { IssueType } from 'src/services/models/IssueType';
import { Message } from 'src/services/models/Message';
import { User } from 'src/services/models/User';
import { ConversationFlagRequestBody } from 'src/services/requests/conversation/ConversationFlagRequest';
import {
    ConversationAssignLabel,
    ConversationCancelTransferBody,
    ConversationJoinBody,
    ConversationListRequestQuery,
    ConversationTransferBody,
    ConversationRequestBody,
    CreateConversationRequestBody,
    ConversationUpdateIssueType
} from 'src/services/requests/conversation/ConversationListRequest';
import { ConversationListResponseResult } from 'src/services/responses/conversation/ConversationListResponse';
import {
    ConversationResponseResult,
    ConversationsTransferResponseResult
} from 'src/services/responses/conversation/ConversationResponse';
import { ActionTree } from 'vuex';
import { StateInterface } from '../index';
import { ConversationStateInterface } from './state';

const messageStore = localForage.createInstance({
    name: 'lobbi/messages'
});

const issueType = localForage.createInstance({
    name: 'lobbi/issueType'
});
const conversationApi = new ConversationApi();
const markAsReads: Record<string, any> = {};
const markAsRead = (conversationId: string) => {
    if (!(conversationId in markAsReads)) {
        markAsReads[conversationId] = debounce((conversationId: string) => {
            conversationApi.markAsRead({ conversationId }).catch(console.error);
        }, 1500);
    }

    markAsReads[conversationId](conversationId);
};

const actions: ActionTree<ConversationStateInterface, StateInterface> = {
    async getJoinedConversations(context, data) {
        const businessId = context.rootState.business.businessID;
        const branch = context.rootState.branch.branch;
        if (businessId && branch) {
            const result = (await conversationApi.conversations(
                data
            )) as ConversationResponseResult;
            if (!result) {
                console.error('Error retrieving conversation');
                return;
            }
            context.commit('setConversationsList', result.conversations);
            return result.conversations;
        }
    },

    async endConversation(context, conversationID) {
        const result = await conversationApi.endConversation(conversationID);
        context.commit('clearAllConversationsHistory');
        context.commit('clearSelectedConversationID');
        context.commit('clearSelectedUserID');
        return result;
    },

    async getConversationsTransferList(context, businessID) {
        const result = (await conversationApi.getConversationsTransferList(
            businessID
        )) as ConversationsTransferResponseResult;
        context.commit('setConversationsTransferList', result.transfers);
    },

    async updateConversationTransferList(context, conversationId) {
        const findIndex = context.state.conversationsTransferList.findIndex(
            (element: any) => element.conversationId == conversationId
        );

        if (findIndex != -1) {
            const data = {
                status: 2,
                transferID:
                    context.state.conversationsTransferList[findIndex].id
            };
            const result = await conversationApi.responseTransfer(data);
            context.commit('removeConvoTransferList', data);
            return result;
        }
    },

    updateConversationListStatus(context, conversationId) {
        context.commit('updateConversationListStatus', conversationId);
    },

    async responseTransfer(context, data) {
        const result = await conversationApi.responseTransfer(data);
        context.commit('removeConvoTransferList', data);
        return result;
    },

    async getConversation(context, query: ConversationListRequestQuery) {
        try {
            const businessId = context.rootState.business.businessID;
            const branch = context.rootState.branch.branch;
            if (businessId && branch) {
                return await conversationApi.getConversations(query);
            }
        } catch (err: any) {
            return err.response.data;
        }
    },

    async view(context, conversationId) {
        try {
            const result = await conversationApi.view({ conversationId });

            const conversation = {
                ...result?.conversation,
                members: keyBy(result?.conversation.members, 'userId')
            };

            // Store into DB
            context.commit('update', conversation);

            const selectedConversation: Conversation = context.rootState.chat
                .selectedConversation as Conversation;
            if (selectedConversation?.id == conversation.id) {
                context.dispatch('chat/updateConversation', conversation, {
                    root: true
                });
            }

            return result;
        } catch (err: any) {
            return err.response.data;
        }
    },

    async refresh(context, conversationId) {
        const limit = 20;
        const offsetDate = undefined;

        try {
            const result: ConversationListResponseResult =
                (await conversationApi.list({
                    offsetDate,
                    limit
                })) as ConversationListResponseResult;

            if (result?.conversations?.length) {
                const conversation = {
                    ...result?.conversations[0],
                    members: keyBy(result?.conversations[0].members, 'userId')
                };

                // Store into DB
                context.commit('update', conversation);

                const selectedConversation: Conversation = context.rootState
                    .chat.selectedConversation as Conversation;
                if (selectedConversation?.id == conversation.id) {
                    context.dispatch('chat/updateConversation', conversation, {
                        root: true
                    });
                }
            }
        } catch (err: any) {}
    },

    async pinFlag(context, body: ConversationFlagRequestBody) {
        try {
            context.commit('pinFlag', body);
            return await conversationApi.flag({ flag: 'pin' }, body);
        } catch (err: any) {
            body.value = body.value == 1 ? 0 : 1;
            context.commit('pinFlag', body);
            return err.response.data;
        }
    },

    async transferConversation(context, body: ConversationTransferBody) {
        try {
            const result = await conversationApi.transferConversation(body);

            return result;
        } catch (err: any) {
            return err.response.data;
        }
    },

    async requestConversation(context, body: ConversationRequestBody) {
        return await conversationApi.requestConversation(body);
    },

    async cancelTransferConversation(
        context,
        body: ConversationCancelTransferBody
    ) {
        try {
            return await conversationApi.cancelTransferConversation(body);
        } catch (err: any) {
            return err.response.data;
        }
    },

    async joinConversation(context, body: ConversationJoinBody) {
        try {
            return await conversationApi.joinConversation(body);
        } catch (err: any) {
            return err.response.data;
        }
    },

    async searchRelation(context, query: any) {
        try {
            return await conversationApi.searchRelation(query);
        } catch (err: any) {
            return err.response.data;
        }
    },

    async assignLabel(context, body: ConversationAssignLabel) {
        const labelIds = [] as any;
        body.labelIds.forEach((element: any) => {
            labelIds.push(element.id);
        });
        const data = {
            conversationId: body.conversationId,
            labelIds: labelIds
        };
        const result = await conversationApi.conversationUpdate(data);
        context.commit('updateLabel', body.labelIds);
        return result;
    },

    setIssueTypes(context, issueTypes: IssueType) {
        context.commit('setIssueTypes', issueTypes);
    },

    setAllChatList(context, allChat: allChatList[]) {
        context.commit('setAllChatList', allChat);
    },
    setAllChatStatus(context, data: any) {
        context.commit('setAllChatStatus', data);
    },
    setAllChatPending(context, data: any) {
        const permissions = context.rootState.role.permissions;
        if (permissions.conversation?.transferForce == true) {
            data.force = true;
        } else {
            data.force = false;
        }
        context.commit('setAllChatPending', data);
    },

    setAllChatPagination(context, pagination: any) {
        context.commit('setAllChatPagination', pagination);
    },

    setAllClientConvoList(context, allClient: allChatList[]) {
        context.commit('setAllClientConvoList', allClient);
    },
    setAllClientConvoStatus(context, data: any) {
        context.commit('setAllClientConvoStatus', data);
    },
    setAllClientConvoPending(context, data: any) {
        const permissions = context.rootState.role.permissions;
        if (permissions.conversation?.transferForce == true) {
            data.force = true;
        } else {
            data.force = false;
        }
        context.commit('setAllClientConvoPending', data);
    },

    setAllClientConvoPagination(context, pagination: any) {
        context.commit('setAllClientConvoPagination', pagination);
    },

    closeConversation(context) {
        context.commit('clearAllConversationsHistory');
        context.commit('clearSelectedConversationID');
        context.commit('clearSelectedUserID');
    },

    setSelectedConversationID(context, conversationID) {
        context.dispatch('chat/removeReply', null, { root: true });
        context.commit('setSelectedConversationID', conversationID);
    },

    setSelectedConversation(context, conversation) {
        context.commit('setSelectedConversation', conversation);
    },

    clearSelectedConversation(context, conversation) {
        context.commit('clearSelectedConversation', conversation);
    },

    setConversationLength(context, result) {
        context.commit('setConversationLength', result);
    },
    updateTime(context, message: any) {
        context.commit('updateTime', message);
    },
    setSocketSignal(context, result) {
        context.commit('setSocketSignal', result);
    },
    pushTransferConversation(context, result) {
        context.commit('pushTransferConversation', result);
    },

    clearSelectedConversationID(context) {
        context.commit('clearSelectedConversationID');
        context.commit('clearTicketConversation');
    },
    removeConvo(context) {
        context.commit('clearSelectedConversationID');
    },

    putConversationsList(context, conversation) {
        context.commit('putConversationsList', conversation);
    },

    setSelectedUserID(context, userID) {
        context.commit('setSelectedUserID', userID);
    },
    clearSelectedUserID(context, userID) {
        context.commit('setSelectedUserID', userID);
    },
    setSelectedIssueTypeID(context, issueTypeID) {
        context.commit('setSelectedIssueTypeID', issueTypeID);
    },
    clearSelectedIssueTypeID(context) {
        context.commit('clearSelectedIssueTypeID');
    },

    putLatestMessage(context, message: Message) {
        const selectedConversation: Conversation = context.rootState.chat
            .selectedConversation as Conversation;
        const user: User = context.rootState.user.user as User;
        context.commit('putLatestMessage', {
            message,
            selectedConversation,
            user
        });
    },

    overrideLatestMessage(context, message: Message) {
        const selectedConversation: Conversation = context.rootState.chat
            .selectedConversation as Conversation;
        const user: User = context.rootState.user.user as User;
        context.commit('putLatestMessage', {
            message,
            selectedConversation,
            user,
            force: true
        });
    },

    overrideLatestMessageHistory(context, message: Message) {
        const selectedConversation: Conversation = context.rootState.chat
            .selectedConversation as Conversation;
        const user: User = context.rootState.user.user as User;
        context.commit('putLatestMessageHistory', {
            message,
            selectedConversation,
            user,
            force: true
        });
    },

    updateLatestMessage(context, message: Message) {
        const user: User = context.rootState.user.user as User;
        context.commit('updateLatestMessage', { message, user });
    },

    clearTotalUnread(context, conversation: Conversation) {
        if (conversation) {
            markAsRead(conversation.id);
            context.commit('clearTotalUnread', conversation);
        }
    },

    update(context, conversation: Conversation) {
        context.commit('update', conversation);

        const selectedConversation: Conversation = context.rootState.chat
            .selectedConversation as Conversation;
        if (selectedConversation?.id == conversation.id) {
            context.dispatch('chat/updateConversation', conversation, {
                root: true
            });
        }
    },

    delete(context, data: { conversationId: string; silent: boolean }) {
        const { conversationId, silent } = data;

        if (!silent) {
            conversationApi.delete({ conversationId });
        }

        context.commit('delete', conversationId);

        // Remove messages from DB
        messageStore.removeItem(conversationId);

        // Close selectedConversation if currently open
        const selectedConversation: Conversation = context.rootState.chat
            .selectedConversation as Conversation;
        if (selectedConversation?.id == conversationId) {
            context.dispatch('chat/unselectConversation', null, { root: true });
        }
    },

    clear(context, data: { conversationId: string; silent: boolean }) {
        const { conversationId, silent } = data;

        if (!silent) {
            conversationApi.clear({ conversationId });
        }

        context.commit('clear', conversationId);

        // Remove messages from DB
        messageStore.removeItem(conversationId);

        // Close selectedConversation if currently open
        const selectedConversation: Conversation = context.rootState.chat
            .selectedConversation as Conversation;
        if (selectedConversation?.id == conversationId) {
            context.dispatch('message/reset', null, { root: true });
        }
    },

    clearAllConversationsHistory(context) {
        context.commit('clearAllConversationsHistory');
    },

    setFinishLoaded(context, data) {
        context.commit('setFinishLoaded', data);
    },

    removeConvoFromList(context, data) {
        context.commit('removeConvoFromList', data);
    },
    setSelectedTicketID(context, data) {
        context.commit('setSelectedTicketID', data);
    },

    changeStatusConvo(context, data) {
        context.commit('changeStatusConvo', data);
    },

    setAssignSuccess(context, data) {
        context.commit('setAssignSuccess', data);
    },

    notificationFlag(context, body: ConversationFlagRequestBody) {
        context.commit('notificationFlag', body);
        conversationApi
            .flag({ flag: 'notification' }, body)
            .catch(console.error);
    },

    getOneConversation(context, data: any) {
        return conversationApi.list(data);
    },
    setTicketConversation(context, conversation: any) {
        context.commit('clearTicketConversation');
        context.commit('setTicketConversation', conversation);
    },
    unsetComponent(context, status: boolean) {
        context.commit('unsetComponent', status);
    },
    setChangeStatus(context, status: boolean) {
        context.commit('setChangeStatus', status);
    },
    async messageClient(context, conversationId: string) {
        await conversationApi.reactivateConversation({
            conversationId: conversationId
        });

        return context.dispatch('getOneConversation', {
            conversationId: conversationId,
            isDownward: 0
        });
    },
    async createConversation(context, body: CreateConversationRequestBody) {
        return await conversationApi.createConversation(body);
    },

    async updateConversationIssueType(
        context,
        body: ConversationUpdateIssueType
    ) {
        const data = {
            conversationId: body.conversationId,
            issueTypeId: body.issueTypeId
        };
        const result = await conversationApi.conversationIssueTypeUpdate(data);
        return result;
    }
};

export default actions;
