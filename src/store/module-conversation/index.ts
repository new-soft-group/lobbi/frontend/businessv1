/* eslint-disable @typescript-eslint/no-unsafe-assignment */
import { Module } from 'vuex';
import { StateInterface } from '../index';
import state, { ConversationStateInterface } from './state';
import actions from './actions';
import getters from './getters';
import mutations from './mutations';

const conversationModule: Module<ConversationStateInterface, StateInterface> = {
    namespaced: true,
    actions,
    getters,
    mutations,
    state
};

export default conversationModule;
