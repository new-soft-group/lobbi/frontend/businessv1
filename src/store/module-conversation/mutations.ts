/* eslint-disable @typescript-eslint/no-unsafe-member-access */
/* eslint-disable @typescript-eslint/restrict-plus-operands */
/* eslint-disable @typescript-eslint/no-unsafe-return */
/* eslint-disable @typescript-eslint/no-floating-promises */
/* eslint-disable @typescript-eslint/no-unsafe-call */
/* eslint-disable @typescript-eslint/no-unsafe-assignment */
import {
    ConversationList,
    Conversation,
    allChatList
} from 'src/services/models/Conversation';
import { MutationTree } from 'vuex';
import { ConversationStateInterface } from './state';
import { Dictionary, find, keyBy, merge } from 'lodash';
import { Message } from 'src/services/models/Message';
import { User } from 'src/services/models/User';
import moment from 'moment';
import { Howl } from 'howler';
import { ConversationFlagRequestBody } from 'src/services/requests/conversation/ConversationFlagRequest';
import localForage from 'localforage';
import { AppVisibility } from 'quasar';
import { MessageType } from 'src/services/enums/MessageType';

const conversationStore = localForage.createInstance({
    name: 'lobbi/conversations'
});

const alertAudio = new Howl({
    src: ['/audio/alert.mp3']
});

const issueTypeStore = localForage.createInstance({
    name: 'lobbi/issueType'
});

const mutation: MutationTree<ConversationStateInterface> = {
    setConversationsList(state, conversation: ConversationList[]) {
        state.conversationsList = conversation;
    },
    setAllChatList(state, conversation: allChatList[]) {
        if (state.allChat.length > 0) {
            state.allChat = [];
        }
        state.allChat = conversation;
    },
    setAllChatPagination(state, pagination: any) {
        state.allChatPagination = pagination;
    },
    setAllClientConvoList(state, conversation: allChatList[]) {
        if (state.allClient.length > 0) {
            state.allClient = [];
        }
        state.allClient = conversation;
    },
    setAllClientConvoPagination(state, pagination: any) {
        state.allClientPagination = pagination;
    },
    setConversationsTransferList(state, conversation: ConversationList[]) {
        state.conversationsTransferList = conversation;
    },
    updateConversationListStatus(state, conversationId: string) {
        const findIndex = state.allChat.findIndex(
            (element: any) => element.id == conversationId
        );

        if (findIndex != -1) {
            state.allChat[findIndex].status = 'Closed';
        }
    },
    removeConvoTransferList(state, convoId: any) {
        const list = state.conversationsTransferList.filter(
            (element: any) => element.id != convoId.transferID
        );
        state.conversationsTransferList = list;
    },
    setAllChatStatus(state, chat: any) {
        let findIndex = -1;
        if (chat.id && !chat.conversationId) {
            findIndex = state.allChat.findIndex(
                (element: any) => element.id == chat.id
            );
        } else {
            findIndex = state.allChat.findIndex(
                (element: any) => element.id == chat.conversationId
            );
        }

        if (findIndex != -1) {
            if (chat.status == 1) {
                if (state.allChat[findIndex].status == 'Waiting') {
                    state.allChat[findIndex].status = 'Active';
                } else if (state.allChat[findIndex].status == 'Pending') {
                    state.allChat[findIndex].status = 'Active';
                    state.allChat[findIndex].lastAgent.id = chat.agent.id;
                    state.allChat[findIndex].lastAgent.name = chat.agent.name;
                }
            } else if (chat.status == 2) {
                if (
                    state.allChat[findIndex].status == 'Pending' &&
                    state.allChat[findIndex].lastAgent.id != ''
                ) {
                    state.allChat[findIndex].status = 'Active';
                } else {
                    state.allChat[findIndex].status = 'Waiting';
                }
            } else if (chat.agent) {
                if (state.allChat[findIndex].status == 'Pending') {
                    state.allChat[findIndex].status = 'Active';
                    state.allChat[findIndex].lastAgent.id = chat.agent.id;
                    state.allChat[findIndex].lastAgent.name = chat.agent.name;
                } else if (state.allChat[findIndex].status == 'Active') {
                    state.allChat[findIndex].status = 'Active';
                    state.allChat[findIndex].lastAgent.id = chat.agent.id;
                    state.allChat[findIndex].lastAgent.name = chat.agent.name;
                }
            } else if (chat.transfer) {
                if (state.allChat[findIndex].status == 'Active') {
                    state.allChat[findIndex].status = 'Pending';
                } else {
                    state.allChat[findIndex].status = 'Active';
                }
            } else {
                if (chat.type == 0) {
                    state.allChat[findIndex].status = 'Closed';
                } else if (state.allChat[findIndex].status == 'Pending') {
                    state.allChat[findIndex].status = 'Active';
                }
            }
        }
    },
    setAllChatPending(state: any, data: any) {
        const findIndex = state.allChat.findIndex(
            (element: any) => element.ticketId == data.ticketId
        );
        if (findIndex != -1) {
            if (state.allChat[findIndex].status && data.type == 'Request') {
                state.allChat[findIndex].status = 'Pending';
            } else if (
                state.allChat[findIndex].status &&
                data.type == 'Transfer' &&
                data.force != true
            ) {
                state.allChat[findIndex].status = 'Pending';
            }
        }
    },
    setAllClientConvoStatus(state, chat: any) {
        let findIndex = -1;
        if (chat.id && !chat.conversationId) {
            findIndex = state.allClient.findIndex(
                (element: any) => element.id == chat.id
            );
        } else {
            findIndex = state.allClient.findIndex(
                (element: any) => element.id == chat.conversationId
            );
        }
        if (findIndex != -1) {
            if (chat.status == 1) {
                if (state.allClient[findIndex].status == 'Waiting') {
                    state.allClient[findIndex].status = 'Active';
                } else if (state.allClient[findIndex].status == 'Pending') {
                    state.allClient[findIndex].status = 'Active';
                    state.allClient[findIndex].lastAgent.id = chat.agent.id;
                    state.allClient[findIndex].lastAgent.name = chat.agent.name;
                }
            } else if (chat.status == 2) {
                if (state.allClient[findIndex].status == 'Pending') {
                    state.allClient[findIndex].status = 'Active';
                }
            } else if (chat.agent) {
                if (state.allClient[findIndex].status == 'Pending') {
                    state.allClient[findIndex].status = 'Active';
                    state.allClient[findIndex].lastAgent.id = chat.agent.id;
                    state.allClient[findIndex].lastAgent.name = chat.agent.name;
                } else if (state.allClient[findIndex].status == 'Active') {
                    state.allClient[findIndex].status = 'Active';
                    state.allClient[findIndex].lastAgent.id = chat.agent.id;
                    state.allClient[findIndex].lastAgent.name = chat.agent.name;
                }
            } else {
                if (chat.type == 0) {
                    state.allClient[findIndex].status = 'Closed';
                } else if (state.allClient[findIndex].status == 'Pending') {
                    state.allClient[findIndex].status = 'Active';
                }
            }
        }
    },
    setAllClientConvoPending(state: any, data: any) {
        const findIndex = state.allClient.findIndex(
            (element: any) => element.ticketId == data.ticketId
        );
        if (findIndex != -1) {
            if (state.allClient[findIndex].status && data.type == 'Request') {
                state.allClient[findIndex].status = 'Pending';
            } else if (
                state.allClient[findIndex].status &&
                data.type == 'Transfer' &&
                data.force != true
            ) {
                state.allClient[findIndex].status = 'Pending';
            }
        }
    },
    putConversationsList(state, conversation: ConversationList) {
        state.conversationsList.push(conversation);
    },
    pushTransferConversation(state, convo: any) {
        state.conversationsTransferList.push(convo);
    },
    setSelectedConversationID(state, conversationID: string) {
        state.selectedConversationID = conversationID;
    },
    setSelectedTicketID(state, ticketID: string) {
        state.selectedTicketID = ticketID;
    },
    setSelectedConversation(state, conversation: any) {
        state.selectedConversation = conversation;
    },
    setConversationLength(state, result: any) {
        if (result) {
            state.conversationLength = result;
        }
    },
    setSocketSignal(state, result: any) {
        const lengths = state.conversationLength;
        if (result && lengths < 20) {
            return (state.socket = result.conversation);
        } else {
            return;
        }
    },
    clearSelectedConversationID(state) {
        state.selectedConversationID = '';
    },
    clearSelectedUserID(state) {
        state.selectedUserID = '';
    },
    changeStatusConvo(state, result: any) {
        state.changeStatusConvo = result;
    },
    setSelectedUserID(state, userID: string) {
        state.selectedUserID = userID;
    },
    setSelectedIssueTypeID(state, issueTypeID: string) {
        state.selectedIssueTypeID = issueTypeID;
    },
    clearSelectedIssueTypeID(state) {
        state.selectedIssueTypeID = '';
    },
    clearSelectedConversation(state) {
        state.selectedConversation = undefined;
    },
    setAssignSuccess(state, status: boolean) {
        state.assignSuccess = status;
    },
    setConversations(state, conversations: Dictionary<Conversation>) {
        state.conversations = conversations;
        conversationStore.setItem(
            'conversations',
            JSON.stringify(state.conversations)
        );
    },
    setConversationsHistory(state, conversations: ConversationList[]) {
        state.conversationsHistory = [];
        conversations.forEach((conversation) => {
            const findIndex = state.conversationsHistory.findIndex(
                (element: any) => element.id == conversation.id
            );

            if (findIndex == -1) {
                conversation.finishLoaded = false;
                state.conversationsHistory.push(conversation);
            }
        });

        conversationStore.setItem(
            'conversationsHistory',
            JSON.stringify(state.conversationsHistory)
        );
    },
    putConversations(state, conversations: Conversation[]) {
        state.conversations = {
            ...state.conversations,
            ...keyBy<Conversation>(conversations, 'id')
        };
        conversationStore.setItem(
            'conversations',
            JSON.stringify(state.conversations)
        );
    },
    putConversationsHistory(state, conversations: Conversation[]) {
        state.conversationsHistory = {
            ...state.conversationsHistory,
            ...keyBy<Conversation>(conversations, 'id')
        };
        conversationStore.setItem(
            'conversationsHistory',
            JSON.stringify(state.conversationsHistory)
        );
    },
    delete(state, conversationId: string) {
        if (state.conversations[conversationId]) {
            delete state.conversations[conversationId];
            conversationStore.setItem(
                'conversations',
                JSON.stringify(state.conversations)
            );
        }
    },
    clear(state, conversationId: string) {
        state.conversations[conversationId].message = undefined;
        conversationStore.setItem(
            'conversations',
            JSON.stringify(state.conversations)
        );
    },
    clearAllConversationsHistory(state) {
        state.conversationsHistory = [];
        conversationStore.setItem(
            'conversationsHistory',
            JSON.stringify(state.conversationsHistory)
        );
    },
    putLatestMessage(
        state,
        data: {
            message: Message;
            selectedConversation?: Conversation;
            user: User;
            force: boolean;
        }
    ) {
        const { message, selectedConversation, user, force } = data;

        const conversation =
            state.conversations[
                message.conversation?.id ?? message.conversationId ?? ''
            ];
        if (conversation) {
            if (state.conversations[conversation.id].message) {
                if (
                    !force &&
                    moment(
                        state.conversations[conversation.id].message?.createdAt
                    ).isAfter(moment(message.createdAt))
                ) {
                    return;
                }

                if (
                    state.conversations[conversation.id].message?.id ==
                    message.id
                ) {
                    return;
                }
            }

            const changes: Record<string, any> = {
                updatedAt: message.createdAt,
                message
            };

            // Increase unread
            if (
                !force &&
                (selectedConversation?.id != conversation.id ||
                    !AppVisibility.appVisible) &&
                (message.user?.id ?? message.userId) != user.id
            ) {
                changes.totals = {
                    ...state.conversations[conversation.id].totals,
                    totalUnread:
                        message.type == MessageType.SYSTEM
                            ? state.conversations[conversation.id].totals
                                  ?.totalUnread ?? 0
                            : (state.conversations[conversation.id].totals
                                  ?.totalUnread ?? 0) + 1
                };

                alertAudio.play();
            }

            // Update changes only once
            state.conversations[conversation.id] = {
                ...state.conversations[conversation.id],
                ...changes
            };
            conversationStore.setItem(
                'conversations',
                JSON.stringify(state.conversations)
            );
        }
    },
    updateLatestMessage(state, data: { message: Message; user: User }) {
        const { message, user } = data;
        let newMessage: Message = message;

        const conversation = find(
            state.conversations,
            (conversation: any) => conversation.message?.id == message.id
        );
        if (conversation) {
            if (message.recipients) {
                if (
                    message.recipients[0].deliveredAt ||
                    message.recipients[0].readAt ||
                    message.recipients[0].hiddenReadAt
                ) {
                    if (message.recipients[0].userId == user.id) {
                        newMessage = {
                            ...message,
                            ...message.recipients[0]
                        };
                    }
                }
            }

            state.conversations[conversation.id].message = merge(
                state.conversations[conversation.id].message ?? {},
                newMessage
            );
            conversationStore.setItem(
                'conversations',
                JSON.stringify(state.conversations)
            );
        }
    },
    clearTotalUnread(state, conversation: Conversation) {
        // state.conversations[conversation.id].totals = {
        //     ...state.conversations[conversation.id].totals,
        //     totalUnread: 0
        // };
        // conversationStore.setItem(
        //     'conversations',
        //     JSON.stringify(state.conversations)
        // );
    },
    update(state, conversation: Conversation) {
        const _conversation = state.conversations[conversation.id];
        if (_conversation) {
            // Update conversation
            state.conversations[conversation.id] = merge(
                _conversation,
                conversation
            );
        } else {
            // Add new conversation
            state.conversations = {
                ...state.conversations,
                ...keyBy<Conversation>([conversation], 'id')
            };
        }
        conversationStore.setItem(
            'conversations',
            JSON.stringify(state.conversations)
        );
    },
    pinFlag(state, body: ConversationFlagRequestBody) {
        const { conversationId, value } = body;

        state.conversations[conversationId].flags = merge(
            state.conversations[conversationId].flags ?? {},
            {
                pinFlag: value
            }
        );
        conversationStore.setItem(
            'conversations',
            JSON.stringify(state.conversations)
        );
    },
    notificationFlag(state, body: ConversationFlagRequestBody) {
        const { conversationId, value } = body;

        state.conversations[conversationId].flags = merge(
            state.conversations[conversationId].flags ?? {},
            {
                notificationFlag: value
            }
        );
        conversationStore.setItem(
            'conversations',
            JSON.stringify(state.conversations)
        );
    },
    setFinishLoaded(state, data) {
        const findIndex = state.conversationsHistory.findIndex(
            (element: any) => element.id == data.conversationId
        );

        if (findIndex != -1) {
            state.conversationsHistory[findIndex].finishLoaded = true;

            conversationStore.setItem(
                'conversationsHistory',
                JSON.stringify(state.conversationsHistory)
            );
        }
    },
    removeConvoFromList(state, data) {
        const findResult = state.conversationsList.filter(function (item: any) {
            return item.id !== data;
        });
        state.conversationsList = findResult;
    },
    updateLabel(state, data) {
        const labels = [] as any;
        const selectedConversationList = state.conversationsList.find(
            (element: any) => element.id == state.selectedConversationID
        );

        data.forEach((element: any) => {
            labels.push(element);
        });

        if (selectedConversationList) {
            selectedConversationList.labels = labels;
        }
    },
    setTicketConversation(state, data) {
        state.ticketConversation = data;
    },
    clearTicketConversation(state) {
        state.ticketConversation = undefined;
    },
    updateTime(state, message: any) {
        const findIndex = state.conversationsList.findIndex(
            (element: any) => element.id == message.conversationId
        );

        if (findIndex > -1) {
            state.conversationsList[findIndex].updatedAt = message.createdAt;
        }
    },
    unsetComponent(state, status) {
        state.clearComponent = status;
    },
    setChangeStatus(state, status) {
        state.changeIssuesType = status;
    }
};

export default mutation;
