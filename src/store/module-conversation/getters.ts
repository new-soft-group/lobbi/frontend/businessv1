/* eslint-disable @typescript-eslint/no-unsafe-return */
import { GetterTree } from 'vuex';
import { StateInterface } from '../index';
import { ConversationStateInterface } from './state';

const getters: GetterTree<ConversationStateInterface, StateInterface> = {
    conversations(state) {
        return state.conversationsList;
    },
    getConversationsHistory(state) {
        return state.conversationsHistory;
    },
    getSelectedConversationID(state) {
        return state.selectedConversationID;
    },
    getSelectedUserID(state) {
        return state.selectedUserID;
    },
    getSelectedIssueTypeID(state) {
        return state.selectedIssueTypeID;
    },
    getConversationsTransferList(state) {
        return state.conversationsTransferList;
    },
    getComponent(state) {
        return state.clearComponent;
    },
    getChangeStatus(state) {
        return state.changeIssuesType;
    },
    getAllChat(state) {
        return state.allChat;
    }
};

export default getters;
