import { IssueType } from 'src/services/models/IssueType';

export interface IssueTypeStateInterface {
    issueTypes: IssueType[];
    created: boolean;
    deleted: boolean;
}

function state(): IssueTypeStateInterface {
    return {
        issueTypes: [],
        created: false,
        deleted: false
    };
}

export default state;
