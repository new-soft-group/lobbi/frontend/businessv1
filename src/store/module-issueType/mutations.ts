/* eslint-disable @typescript-eslint/no-unsafe-call */
/* eslint-disable @typescript-eslint/no-unsafe-assignment */
/* eslint-disable @typescript-eslint/no-floating-promises */
import { MutationTree } from 'vuex';
import { IssueTypeStateInterface } from './state';
import { IssueType } from 'src/services/models/IssueType';
import localForage from 'localforage';

const issueTypeStore = localForage.createInstance({
    name: 'lobbi/issueType'
});

const mutation: MutationTree<IssueTypeStateInterface> = {
    async setIssueTypes(
        state,
        data: { issueTypes: IssueType[]; businessId: string }
    ) {
        state.issueTypes = data.issueTypes;

        // Persisted Store
        let persistedStore: any = await issueTypeStore.getItem(data.businessId);
        persistedStore = JSON.parse(persistedStore);

        // Get which item in persisted store have notification

        const filtered: any = [];

        if (persistedStore) {
            persistedStore.forEach((element: any) => {
                if (element.notification) {
                    filtered.push(element.id);
                }
            });

            if (filtered.length > 0) {
                state.issueTypes.forEach((element: any) => {
                    if (filtered.includes(element.id)) {
                        element.notification = true;
                    }
                });
            }
        }

        issueTypeStore.setItem(
            data.businessId,
            JSON.stringify(state.issueTypes)
        );
    },
    putIssueTypes(state, issueTypes: IssueType) {
        state.issueTypes.push(issueTypes);
        state.created = true;
        issueTypeStore.setItem(
            'issueTypeList',
            JSON.stringify(state.issueTypes)
        );
    },
    deleteIssueTypes(state, issueTypesId: any) {
        const result = state.issueTypes.filter(
            (types) => types.id !== issueTypesId
        );

        if (result) {
            state.issueTypes = result;
            state.deleted = true;
            /* issueTypeStore.setItem(
                'issueTypeList',
                JSON.stringify(state.issueTypes)
            ); */
        }
    },
    setCreated(state, data: boolean) {
        state.created = data;
    },
    setDeleted(state, data: boolean) {
        state.deleted = data;
    },
    setForceTransferNotification(
        state,
        data: { conversations: any; businessId: string }
    ) {
        state.issueTypes.forEach((element: any) => {
            if (data.conversations.issueType.id == element.id) {
                element.notification = true;
            }
        });

        issueTypeStore.setItem(
            data.businessId,
            JSON.stringify(state.issueTypes)
        );
    },
    resetForceTransferNotification(
        state,
        data: { issueTypeId: string; businessId: string }
    ) {
        const result = state.issueTypes.filter(
            (types) => types.id == data.issueTypeId
        );

        if (result) {
            result[0].notification = false;
            issueTypeStore.setItem(
                data.businessId,
                JSON.stringify(state.issueTypes)
            );
        }
    }
};

export default mutation;
