/* eslint-disable @typescript-eslint/no-floating-promises */
/* eslint-disable @typescript-eslint/no-unsafe-assignment */
/* eslint-disable @typescript-eslint/no-unsafe-member-access */
/* eslint-disable @typescript-eslint/no-unsafe-return */
import IssueTypeApi from 'src/services/apis/IssueTypeApi';
import { ActionTree } from 'vuex';
import { StateInterface } from '../index';
import { IssueTypeStateInterface } from './state';
import localForage from 'localforage';
import {
    IssueTypeQuery,
    CreateIssueTypeBody,
    EditIssueTypeBody,
    DeleteIssueTypeBody
} from 'src/services/requests/issuesType/IssueTypeRequest';

const issueTypeApi = new IssueTypeApi();

const issueType = localForage.createInstance({
    name: 'lobbi/issueType'
});

const actions: ActionTree<IssueTypeStateInterface, StateInterface> = {
    async getIssueTypeByBusinessID(context, query: IssueTypeQuery) {
        if (context.rootState.business.businessID) {
            try {
                const res: any = await issueTypeApi.getIssueTypeByBusinessID(
                    query
                );

                if (res.issueTypes.length > 0) {
                    context.commit('setIssueTypes', {
                        issueTypes: res.issueTypes,
                        businessId: context.rootState.business.businessID
                    });
                }
                return res;
            } catch (err: any) {
                return err.response.data;
            }
        }
    },
    async createIssuesType(context, body: CreateIssueTypeBody) {
        try {
            const res: any = await issueTypeApi.createIssuesType(body);
            if (res.status == 200) {
                context.commit('putIssueTypes', res.result.issueType);
            }
            return res;
        } catch (err: any) {
            return err.response.data;
        }
    },
    async editIssuesType(context, body: EditIssueTypeBody) {
        try {
            return await issueTypeApi.editIssuesType(body);
        } catch (err: any) {
            return err.response.data;
        }
    },
    async deleteIssuesType(context, body: DeleteIssueTypeBody) {
        try {
            const res: any = await issueTypeApi.deleteIssuesType(body);
            if (res.status === 200) {
                context.commit('deleteIssueTypes', body.issueTypeId);
                issueType.removeItem(body.issueTypeId);
            }
            return res;
        } catch (err: any) {
            return err.response.data;
        }
    },
    setForceTransferNotification(context, conversationId: any) {
        let conversation: any = [];

        context.rootState.conversation.conversationsList.forEach(
            (element: any) => {
                if (element.id == conversationId) {
                    conversation = element;
                }
            }
        );
        context.commit('setForceTransferNotification', {
            conversations: conversation,
            businessId: context.rootState.business.businessID
        });
    },
    resetForceTransferNotification(context, issueTypeId: string) {
        context.commit('resetForceTransferNotification', {
            issueTypeId: issueTypeId,
            businessId: context.rootState.business.businessID
        });
    }
};

export default actions;
