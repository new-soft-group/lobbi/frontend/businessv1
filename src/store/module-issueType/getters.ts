import { GetterTree } from 'vuex';
import { StateInterface } from '../index';
import { IssueTypeStateInterface } from './state';

const getters: GetterTree<IssueTypeStateInterface, StateInterface> = {
    getIssueTypesList(state) {
        return state.issueTypes;
    }
};

export default getters;
