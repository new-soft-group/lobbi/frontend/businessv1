/* eslint-disable @typescript-eslint/no-floating-promises */
/* eslint-disable @typescript-eslint/no-unsafe-assignment */
/* eslint-disable @typescript-eslint/no-unsafe-member-access */
/* eslint-disable @typescript-eslint/no-unsafe-return */
import { ActionTree } from 'vuex';
import { StateInterface } from '../index';
import { FilterStateInterface } from './state';
import { FilterList } from 'src/services/models/FilterList';

const actions: ActionTree<FilterStateInterface, StateInterface> = {
    setFilterList(context, filterList: FilterList) {
        context.commit('setFilterList', filterList);
    },
    clearFilterList(context) {
        context.commit('clearFilterList');
    }
};

export default actions;
