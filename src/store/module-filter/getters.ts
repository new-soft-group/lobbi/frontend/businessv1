import { GetterTree } from 'vuex';
import { StateInterface } from '../index';
import { FilterStateInterface } from './state';

const getters: GetterTree<FilterStateInterface, StateInterface> = {
    getClientFilterList(state) {
        return state.clientFilter;
    },
    getChatFilterList(state) {
        return state.chatFilter;
    }
};

export default getters;
