/* eslint-disable @typescript-eslint/no-unsafe-call */
/* eslint-disable @typescript-eslint/no-unsafe-assignment */
/* eslint-disable @typescript-eslint/no-floating-promises */
import { MutationTree } from 'vuex';
import { FilterStateInterface } from './state';
import { FilterList } from 'src/services/models/FilterList';
import localForage from 'localforage';
import { FilterType } from 'src/services/enums/FilterType';

const filterTypeStore = localForage.createInstance({
    name: 'lobbi/filter'
});

const mutation: MutationTree<FilterStateInterface> = {
    setFilterList(state, filters: FilterList) {
        if (filters.page == FilterType.CHAT) {
            state.chatFilter = filters;
            state.chatFilter.page = 'chat';
            filterTypeStore.setItem(
                'chatFilter',
                JSON.stringify(state.chatFilter)
            );
        }

        if (filters.page == FilterType.CLIENT) {
            state.clientFilter = filters;
            state.clientFilter.page = 'client';
            filterTypeStore.setItem(
                'clientFilter',
                JSON.stringify(state.clientFilter)
            );
        }
    },
    clearFilterList(state) {
        state.chatFilter = {};
        state.clientFilter = {};
        filterTypeStore.clear();
    }
};

export default mutation;
