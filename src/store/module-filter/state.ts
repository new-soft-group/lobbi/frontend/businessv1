export interface FilterStateInterface {
    chatFilter: any;
    clientFilter: any;
}

function state(): FilterStateInterface {
    return {
        chatFilter: {},
        clientFilter: {}
    };
}

export default state;
