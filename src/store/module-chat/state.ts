import { Conversation } from 'src/services/models/Conversation';
import { Message } from 'src/services/models/Message';

export interface ChatStateInterface {
    isRightSidebarHidden: boolean;
    isRightSearchHidden: boolean;
    selectedConversation?: Conversation;
    isOnline: boolean;
    lastIsOnlineAt?: Date;
    isTypingList: Record<string, Record<string, Date>>;
    isSelectMultiple: boolean;
    selectedMessages: Record<string, Message>;
    replyToMessage?: Message | undefined;
    scrollToMessage?: Message;
}

function state(): ChatStateInterface {
    return {
        isRightSidebarHidden: true,
        isRightSearchHidden: true,
        isOnline: false,
        isTypingList: {},
        isSelectMultiple: false,
        selectedMessages: {}
    };
}

export default state;
