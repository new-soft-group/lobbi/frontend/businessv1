/* eslint-disable @typescript-eslint/no-non-null-assertion */
/* eslint-disable @typescript-eslint/no-unsafe-member-access */
/* eslint-disable @typescript-eslint/no-unsafe-assignment */
/* eslint-disable @typescript-eslint/no-floating-promises */
import { Conversation } from 'src/services/models/Conversation';
import { Message } from 'src/services/models/Message';
import { SocketMessageResponseBody } from 'src/services/responses/socket/SocketMessageResponse';
import { ActionTree } from 'vuex';
import { StateInterface } from '../index';
import { ChatStateInterface } from './state';

const actions: ActionTree<ChatStateInterface, StateInterface> = {
    toggleRightSidebar(context) {
        context.commit('toggleRightSidebar');
    },
    toggleRightSearch(context) {
        context.commit('toggleRightSearch');
    },
    selectConversation(context, conversation: Conversation) {
        context.commit('reset');
        context.commit('selectConversation', conversation);

        // Clear unread
        if ((conversation.totals?.totalUnread ?? 0) > 0) {
            context.dispatch('conversation/clearTotalUnread', conversation, {
                root: true
            });
        }

        context.dispatch('conversation/view', conversation.id, { root: true });
    },
    unselectConversation(context) {
        context.commit('reset');
        context.commit('selectConversation', '');
    },
    updateConversation(context, conversation: Conversation) {
        context.commit('updateConversation', conversation);
    },
    putIsTyping(context, body: SocketMessageResponseBody) {
        context.commit('putIsTyping', body);
    },
    removeIsTyping(context, body: SocketMessageResponseBody) {
        context.commit('removeIsTyping', body);
    },
    putIsOnline(context, body: SocketMessageResponseBody) {
        const selectedConversation: Conversation = context.state
            .selectedConversation as Conversation;

        if (selectedConversation?.id == body.conversationId) {
            context.commit('putIsOnline', body);
        }
    },
    removeIsOnline(context, body: SocketMessageResponseBody) {
        const selectedConversation: Conversation = context.state
            .selectedConversation as Conversation;

        if (selectedConversation?.id == body.conversationId) {
            context.commit('removeIsOnline', body);
        }
    },
    setIsSelectMultiple(context, value: boolean) {
        context.commit('setIsSelectMultiple', value);
    },
    setTotalSelected(context, value: number) {
        context.commit('setTotalSelected', value);
    },
    selectMessage(context, message: Message) {
        context.commit('selectMessage', message);
    },
    unselectMessage(context, message: Message) {
        context.commit('unselectMessage', message);
    },
    setReply(context, message: Message) {
        context.commit('setReply', message);
    },
    removeReply(context) {
        context.commit('removeReply');
    },
    setScroll(context, message: Message) {
        context.commit('setScroll', message);

        const selectedConversation: Conversation = context.state
            .selectedConversation as Conversation;
        if (
            selectedConversation?.id !=
            (message.conversation?.id ?? message.conversationId)
        ) {
            // Open the conversation
            const conversation =
                context.rootState.conversation.conversations[
                    message.conversation?.id ?? message.conversationId!
                ];
            if (conversation) {
                context.dispatch('selectConversation', conversation);
            }
        }
    },
    removeScroll(context) {
        context.commit('removeScroll');
    },
    clearReplyToMessage(context) {
        context.commit('clearReplyToMessage');
    }
};

export default actions;
