/* eslint-disable @typescript-eslint/no-unsafe-call */
/* eslint-disable @typescript-eslint/no-unsafe-assignment */
import { Conversation } from 'src/services/models/Conversation';
import { SocketMessageResponseBody } from 'src/services/responses/socket/SocketMessageResponse';
import { MutationTree } from 'vuex';
import { ChatStateInterface } from './state';
import moment from 'moment';
import { Message } from 'src/services/models/Message';
import { merge } from 'lodash';

const mutation: MutationTree<ChatStateInterface> = {
    toggleRightSidebar(state) {
        state.isRightSearchHidden = true;
        state.isRightSidebarHidden = !state.isRightSidebarHidden;
    },
    toggleRightSearch(state) {
        state.isRightSidebarHidden = true;
        state.isRightSearchHidden = !state.isRightSearchHidden;
    },
    selectConversation(state, conversation: Conversation) {
        state.selectedConversation = conversation;
    },
    updateConversation(state, conversation: Conversation) {
        state.selectedConversation = merge(
            state.selectedConversation ?? {},
            conversation
        );
    },
    reset(state) {
        state.isOnline = false;
        state.lastIsOnlineAt = undefined;
        state.isRightSidebarHidden = true;
        state.isSelectMultiple = false;
        state.selectedMessages = {};
        state.replyToMessage = undefined;
        delete state.isTypingList[state.selectedConversation?.id ?? ''];
        state.selectedConversation = undefined;
    },
    putIsTyping(state, body: SocketMessageResponseBody) {
        if (!((body.conversationId ?? '') in state.isTypingList)) {
            state.isTypingList[body.conversationId ?? ''] = {};
        }

        state.isTypingList[body.conversationId ?? ''][body.userId ?? ''] =
            new Date();
    },
    removeIsTyping(state, body: SocketMessageResponseBody) {
        const lastIsTypingAt =
            state.isTypingList[body.conversationId ?? '']?.[body.userId ?? ''];
        if (
            lastIsTypingAt &&
            moment(lastIsTypingAt).add(1, 'seconds').isBefore(moment())
        ) {
            delete state.isTypingList[body.conversationId ?? ''][
                body.userId ?? '' 
            ];
        }
    },
    putIsOnline(state, body: SocketMessageResponseBody) {
        if (state.selectedConversation?.id == body.conversationId) {
            state.isOnline = true;
            state.lastIsOnlineAt = new Date();
        }
    },
    removeIsOnline(state, body: SocketMessageResponseBody) {
        if (state.selectedConversation?.id == body.conversationId) {
            if (
                state.lastIsOnlineAt &&
                moment(state.lastIsOnlineAt)
                    .add(10, 'seconds')
                    .isBefore(moment())
            ) {
                state.isOnline = false;
            }
        }
    },
    setIsSelectMultiple(state, value: boolean) {
        state.isSelectMultiple = value;
        if (value == false) {
            state.selectedMessages = {};
        }
    },
    selectMessage(state, message: Message) {
        state.selectedMessages[message.id] = message;
    },
    unselectMessage(state, message: Message) {
        delete state.selectedMessages[message.id];
    },
    setReply(state, message: Message) {
        state.replyToMessage = message;
    },
    removeReply(state) {
        delete state.replyToMessage;
    },
    setScroll(state, message: Message) {
        state.scrollToMessage = message;
    },
    removeScroll(state) {
        delete state.scrollToMessage;
    },
    clearReplyToMessage(state) {
        state.replyToMessage = undefined;
    }
};

export default mutation;
