/* eslint-disable @typescript-eslint/no-unsafe-call */
/* eslint-disable @typescript-eslint/no-unsafe-assignment */
/* eslint-disable @typescript-eslint/no-floating-promises */
import { MutationTree } from 'vuex';
import { RemarkStateInterface } from './state';
import { Remark, RemarkList } from 'src/services/models/Remark';
import localForage from 'localforage';

const remarkStore = localForage.createInstance({
    name: 'lobbi/remark'
});

const mutation: MutationTree<RemarkStateInterface> = {
    setRemark(state, Remark: Remark[]) {
        state.remark = Remark;
        remarkStore.setItem('remarkList', JSON.stringify(state.remark));
    },
    addRemark(state, remark: RemarkList) {
        /*         const list = {
            id: remark.id,
            note: remark.note,
            name: remark.agent.name,
            hover: false,
            edit: false
        };
        console.log(list,2) */
        state.remark.push(remark);
        remarkStore.setItem('remarkList', JSON.stringify(state.remark));
    },
    putRemark(state, Remark: Remark) {
        state.remark.push(Remark);
        remarkStore.setItem('remarkList', JSON.stringify(state.remark));
    },
    deleteRemark(state, remarkId: any) {
        const result = state.remark.filter((types) => types.id !== remarkId);

        if (result) {
            state.remark = result;
            remarkStore.setItem('remarkList', JSON.stringify(state.remark));
        }
    },

    clearAllRemark(state) {
        state.remark = [];
        remarkStore.setItem('remarkList', JSON.stringify(state.remark));
    }
};

export default mutation;
