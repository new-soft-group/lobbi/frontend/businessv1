import { Remark } from 'src/services/models/Remark';

export interface RemarkStateInterface {
    remark: Remark[];
}

function state(): RemarkStateInterface {
    return {
        remark: []
    };
}

export default state;
