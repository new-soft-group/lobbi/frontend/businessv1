import { GetterTree } from 'vuex';
import { StateInterface } from '../index';
import { RemarkStateInterface } from './state';

const getters: GetterTree<RemarkStateInterface, StateInterface> = {
    someAction(/* context */) {
        // your code
    },
    getRemarkList(state) {
        return state.remark;
    }
};

export default getters;
