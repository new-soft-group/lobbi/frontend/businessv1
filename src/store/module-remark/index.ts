import { Module } from 'vuex';
import { StateInterface } from '../index';
import state, { RemarkStateInterface } from './state';
import actions from './actions';
import getters from './getters';
import mutations from './mutations';

const userModule: Module<RemarkStateInterface, StateInterface> = {
    namespaced: true,
    actions,
    getters,
    mutations,
    state
};

export default userModule;
