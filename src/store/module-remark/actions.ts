/* eslint-disable @typescript-eslint/no-floating-promises */
/* eslint-disable @typescript-eslint/no-unsafe-assignment */
/* eslint-disable @typescript-eslint/no-unsafe-member-access */
/* eslint-disable @typescript-eslint/no-unsafe-return */
import RemarkApi from 'src/services/apis/RemarkApi';
import { ActionTree } from 'vuex';
import { StateInterface } from '../index';
import { RemarkStateInterface } from './state';
import localForage from 'localforage';
import {
    RemarkQuery,
    CreateRemarkBody,
    EditRemarkBody,
    DeleteRemarkBody
} from 'src/services/requests/remark/RemarkRequest';

const remarkApi = new RemarkApi();

const remark = localForage.createInstance({
    name: 'lobbi/remark'
});

const actions: ActionTree<RemarkStateInterface, StateInterface> = {
    async getRemarkByUserID(context, query: RemarkQuery) {
        try {
            const res: any = await remarkApi.getRemarkByUserID(query);
            if (res.remarks.length > 0) {
                context.commit('setRemark', res.remarks);
            }
            return res;
        } catch (err: any) {
            return err.response;
        }
    },
    async createRemark(context, body: CreateRemarkBody) {
        try {
            const res: any = await remarkApi.createRemark(body);
            if (res.status == 200) {
                context.commit('addRemark', res.result.remark);
            }
            return res;
        } catch (err: any) {
            return err.response.data;
        }
    },
    async editRemark(context, body: EditRemarkBody) {
        try {
            const res: any = await remarkApi.editRemark(body);
            /*          
            if (res.status == 200) {
                context.commit('putRemark', res);
            } */
            return res;
        } catch (err: any) {
            return err.response.data;
        }
    },
    async deleteRemark(context, body: DeleteRemarkBody) {
        try {
            const res: any = await remarkApi.deleteRemark(body);
            if (res.status === 200) {
                context.commit('deleteRemark', body.remarkId);
                remark.removeItem(body.remarkId);
            }
            return res;
        } catch (err: any) {
            return err.response.data;
        }
    },

    clearAllRemark(context) {
        context.commit('clearAllRemark');
    }
};

export default actions;
