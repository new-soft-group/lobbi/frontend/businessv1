/* eslint-disable @typescript-eslint/no-unsafe-call */
/* eslint-disable @typescript-eslint/no-unsafe-assignment */
/* eslint-disable @typescript-eslint/no-floating-promises */
import { MutationTree } from 'vuex';
import { LabelStateInterface } from './state';
import { Label } from 'src/services/models/Setting';

const mutation: MutationTree<LabelStateInterface> = {
    setLabel(state, labels: Label[]) {
        state.list = labels;
    },
    putLabel(state, label: Label) {
        state.list.push(label);
        state.created = true;
    },
    updateLabel(state, label: Label) {
        const findIndex = state.list.findIndex(
            (element: any) => element.id == label.id
        );
        if (findIndex != -1) {
            state.list[findIndex].id = label.id;
            state.list[findIndex].color = label.color;
            state.list[findIndex].icon = label.icon;
            state.list[findIndex].name = label.name;
            state.updated = true;
        }
    },
    removeLabel(state, labelId: string) {
        const findIndex = state.list.findIndex(
            (element: any) => element.id == labelId
        );
        if (findIndex != -1) {
            state.list.splice(findIndex, 1);
            state.removed = true;
        }
    },
    setCreated(state, data: boolean) {
        state.created = data;
    },
    setUpdated(state, data: boolean) {
        state.updated = data;
    },
    setRemoved(state, data: boolean) {
        state.removed = data;
    }
};

export default mutation;
