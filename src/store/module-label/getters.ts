import { GetterTree } from 'vuex';
import { StateInterface } from '../index';
import { LabelStateInterface } from './state';

const getters: GetterTree<LabelStateInterface, StateInterface> = {
    getList(state) {
        return state.list;
    }
};

export default getters;
