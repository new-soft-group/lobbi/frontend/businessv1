/* eslint-disable @typescript-eslint/no-floating-promises */
/* eslint-disable @typescript-eslint/no-unsafe-assignment */
/* eslint-disable @typescript-eslint/no-unsafe-member-access */
/* eslint-disable @typescript-eslint/no-unsafe-return */
import LabelApi from 'src/services/apis/LabelApi';
import { ActionTree } from 'vuex';
import { StateInterface } from '../index';
import { LabelStateInterface } from './state';
import localForage from 'localforage';
import { LabelRequestBody } from 'src/services/requests/setting/LabelCreateRequest';

const labelApi = new LabelApi();

const issueType = localForage.createInstance({
    name: 'lobbi/issueType'
});

const actions: ActionTree<LabelStateInterface, StateInterface> = {
    async getList(context) {
        try {
            const res: any = await labelApi.list();
            if (res.status == 200) {
                context.commit('setLabel', res.result.labels);
            }
            return res;
        } catch (err: any) {
            return err.response.data;
        }
    },

    async create(context, body: LabelRequestBody) {
        try {
            const res: any = await labelApi.create(body);
            if (res.status == 200) {
                context.commit('putLabel', res.result.label);
            }
            return res;
        } catch (err: any) {
            return err.response.data;
        }
    },

    async update(context, body: LabelRequestBody) {
        try {
            const res: any = await labelApi.update(body);
            if (res.status == 200) {
                const label = {
                    id: body.labelId,
                    name: body.name,
                    color: body.color,
                    icon: body.icon
                };
                context.commit('updateLabel', label);
            }
            return res;
        } catch (err: any) {
            return err.response.data;
        }
    },

    async delete(context, query: any) {
        try {
            const res: any = await labelApi.delete(query);
            if (res.status == 200) {
                context.commit('removeLabel', query.labelId);
            }
            return res;
        } catch (err: any) {
            return err.response.data;
        }
    }
};

export default actions;
