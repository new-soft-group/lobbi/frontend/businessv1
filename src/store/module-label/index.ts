import { Module } from 'vuex';
import { StateInterface } from '../index';
import state, { LabelStateInterface } from './state';
import actions from './actions';
import getters from './getters';
import mutations from './mutations';

const userModule: Module<LabelStateInterface, StateInterface> = {
    namespaced: true,
    actions,
    getters,
    mutations,
    state
};

export default userModule;
