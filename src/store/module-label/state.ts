import { Label } from 'src/services/models/Setting';

export interface LabelStateInterface {
    list: Label[];
    created: boolean;
    updated: boolean;
    removed: boolean;
}

function state(): LabelStateInterface {
    return {
        list: [],
        created: false,
        updated: false,
        removed: false
    };
}

export default state;
