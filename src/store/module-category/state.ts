import { Catogory, Knowledgebases } from 'src/services/models/category';

export interface CategoryStateInterface {
    category: Catogory[];
    knowledgebases: Knowledgebases[];
}

function state(): CategoryStateInterface {
    return {
        category: [],
        knowledgebases: []
    };
}

export default state;
