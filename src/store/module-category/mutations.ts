/* eslint-disable @typescript-eslint/no-unsafe-call */
/* eslint-disable @typescript-eslint/no-unsafe-assignment */
/* eslint-disable @typescript-eslint/no-floating-promises */
import { MutationTree } from 'vuex';
import { CategoryStateInterface } from './state';
import { Catogory, Knowledgebases } from 'src/services/models/category';

const mutation: MutationTree<CategoryStateInterface> = {
    setCategory(state, category: Catogory[]) {
        state.category = category;
    },
    putCategory(state, category: Catogory) {
        state.category.push(category);
    },
    updateCategory(state, category: Catogory) {
        const findIndex = state.category.findIndex(
            (element: any) => element.id == category.id
        );
        if (findIndex != -1) {
            state.category[findIndex].id = category.id;
            state.category[findIndex].name = category.name;
        }
    },
    removeCategory(state, knowledgebaseCategoryId: string) {
        const findIndex = state.category.findIndex(
            (element: any) => element.id == knowledgebaseCategoryId
        );
        if (findIndex != -1) {
            state.category.splice(findIndex, 1);
        }
    },
    setKnowledgebase(state, knowledgebases: Knowledgebases[]) {
        state.knowledgebases = knowledgebases;
    },
    putKnowledgebase(state, knowledgebases: Knowledgebases) {
        state.knowledgebases.push(knowledgebases);
    },
    updateKnowledgebase(state, knowledgebases: Knowledgebases) {
        const findIndex = state.knowledgebases.findIndex(
            (element: any) => element.id == knowledgebases.id
        );
        if (findIndex != -1) {
            state.knowledgebases[findIndex].id = knowledgebases.id;
            state.knowledgebases[findIndex].title = knowledgebases.title;
            state.knowledgebases[findIndex].content = knowledgebases.content;
        }
    },
    removeKnowledgebase(state, knowledgebaseCategoryId: string) {
        const findIndex = state.knowledgebases.findIndex(
            (element: any) => element.id == knowledgebaseCategoryId
        );
        if (findIndex != -1) {
            state.category.splice(findIndex, 1);
        }
    }
};

export default mutation;
