import { GetterTree } from 'vuex';
import { StateInterface } from '../index';
import { CategoryStateInterface } from './state';

const getters: GetterTree<CategoryStateInterface, StateInterface> = {
    getCategory(state) {
        return state.category;
    },
    getKnowledgeBase(state) {
        return state.knowledgebases;
    }
};

export default getters;
