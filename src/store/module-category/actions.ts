/* eslint-disable @typescript-eslint/no-floating-promises */
/* eslint-disable @typescript-eslint/no-unsafe-assignment */
/* eslint-disable @typescript-eslint/no-unsafe-member-access */
/* eslint-disable @typescript-eslint/no-unsafe-return */
import CategoryApi from 'src/services/apis/CategoryApi';
import KnowledgeBaseApi from 'src/services/apis/KnowledgeBaseApi';
import { ActionTree } from 'vuex';
import { StateInterface } from '../index';
import { CategoryStateInterface } from './state';
import localForage from 'localforage';
import { CreateCategoryBody,EditCategoryBody,DeleteCategoryBody } from 'src/services/requests/category/CategoryRequest';
import { CreateKnowledgeBody,EditKnowledgeBody,DeleteKnowledgeBody,KnowledgeQuery } from 'src/services/requests/knowledge/KnowledgeRequest';
import { DeleteIssueTypeBody } from 'src/services/requests/issuesType/IssueTypeRequest';

const categoryApi = new CategoryApi();
const knowledgeBaseApi = new KnowledgeBaseApi();

const actions: ActionTree<CategoryStateInterface, StateInterface> = {
    async getCategory(context) {
        try {
            const res: any = await categoryApi.list();
            if (res.status == 200) {
                context.commit('setCategory', res.result.categories);
            }
            return res;
        } catch (err: any) {
            return err.response.data;
        }
    },

    async createCategory(context, body: CreateCategoryBody) {
        try {
            const res: any = await categoryApi.create(body);
            if (res.status == 200) {
                return res;
            }
        } catch (err: any) {
            return err.response.data;
        }
    },

    async updateCategory(context, body: EditCategoryBody) {
        try {
            const res: any = await categoryApi.update(body);
            if (res.status == 200) {
                return res;
            }
        } catch (err: any) {
            return err.response.data;
        }
    },

    async deleteCategory(context, query: DeleteCategoryBody) {
        try {
            const res: any = await categoryApi.delete(query);
            if (res.status == 200) {
                return res;
            }
        } catch (err: any) {
            return err.response.data;
        }
    },
    async getKnowledgeBase(context, query: KnowledgeQuery) {
        try {
            const res: any = await knowledgeBaseApi.list(query);
            if (res.status == 200) {
                context.commit('setKnowledgebase', res.result.knowledgebases);
            }
            return res;
        } catch (err: any) {
            return err.response.data;
        }
    },

    async createKnowledgeBase(context, body: CreateKnowledgeBody) {
        try {
            const res: any = await knowledgeBaseApi.create(body);
            if (res.status == 200) {
                return res;
            }
        } catch (err: any) {
            return err.response.data;
        }
    },

    async updateKnowledgeBase(context, body: EditKnowledgeBody) {
        try {
            const res: any = await knowledgeBaseApi.update(body);
            if (res.status == 200) {
                return res;
            }
        } catch (err: any) {
            return err.response.data;
        }
    },

    async deleteKnowledgeBase(context, query: DeleteKnowledgeBody) {
        try {
            const res: any = await knowledgeBaseApi.delete(query);
            if (res.status == 200) {
                return res;
            }
        } catch (err: any) {
            return err.response.data;
        }
    }
};

export default actions;
