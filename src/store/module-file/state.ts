export interface FileStateInterface {
	selectedTab: string;
}

function state(): FileStateInterface {
	return {
		selectedTab: 'chat'
	};
};

export default state;
