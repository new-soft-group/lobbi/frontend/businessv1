import FileApi from 'src/services/apis/FileApi';
import { ActionTree } from 'vuex';
import { StateInterface } from '../index';
import { FileStateInterface } from './state';
import { FileUploadResponseResult } from 'src/services/responses/file/FileUploadResponse';

const fileApi = new FileApi();

const actions: ActionTree<FileStateInterface, StateInterface> = {
    async upload(
        context,
        data: { file: File; avatar?: boolean; thumbnail?: boolean }
    ) {
        const { file, avatar, thumbnail } = data;

        // Upload the file
        const formData = new FormData();
        formData.append('file', file);

        // Generate signed URL
        const result: FileUploadResponseResult = (await fileApi.upload(
            formData,
            { avatar, thumbnail }
        )) as FileUploadResponseResult;
        if (!result) {
            console.error('Error generating file URL');
            return;
        }

        return result.file;
    },
    async avatarUpload(
        context,
        data: { file: File; avatar?: boolean; thumbnail?: boolean }
    ) {
        const { file, avatar, thumbnail } = data;

        // Upload the file
        const formData = new FormData();
        formData.append('file', file);

        // Generate signed URL
        const result: FileUploadResponseResult = (await fileApi.avatarUpload(
            formData,
            { avatar, thumbnail }
        )) as FileUploadResponseResult;
        if (!result) {
            console.error('Error generating file URL');
            return;
        }

        return result.file;
    }
};

export default actions;
