import { MutationTree } from 'vuex';
import { FileStateInterface } from './state';

const mutation: MutationTree<FileStateInterface> = {
	switchTab(state, tab: string) {
		state.selectedTab = tab;
	}
};

export default mutation;
