/* eslint-disable @typescript-eslint/no-floating-promises */
/* eslint-disable @typescript-eslint/no-unsafe-assignment */
/* eslint-disable @typescript-eslint/no-unsafe-member-access */
/* eslint-disable @typescript-eslint/no-unsafe-return */
import ServiceApi from 'src/services/apis/ServiceApi';
import { CreateServiceBody, EditServiceBody, ServiceQuery } from 'src/services/requests/service/ServiceRequest';
import { ActionTree } from 'vuex';
import { StateInterface } from '../index';
import { ServiceStateInterface } from './state';

const serviceApi = new ServiceApi();

const actions: ActionTree<ServiceStateInterface, StateInterface> = {
    async getServiceTypeList(context, query: ServiceQuery) {
        try {
            const res: any = await serviceApi.list(query);
            if (res.status == 200) {
                context.commit('setService', res.result.serviceTypes);
            }
            return res;
        } catch (err: any) {
            return err.response.data
        }
    },

    async create(context, body: CreateServiceBody) {
        try {
            const res: any = await serviceApi.create(body);
            if (res.status == 200) {
                context.commit('putService', res.result.serviceType);
            }
            return res;
        } catch (err: any) {
            return err.response.data;
        }
    },

    async update(context, body: EditServiceBody) {
        try {
            const res: any = await serviceApi.update(body);
            if (res.status == 200) {
                context.commit('updateService', res);
            }
            return res;
        } catch (err: any) {
            return err.response.data;
        }
    },

    async delete(context, query: any) {
        try {
            const res: any = await serviceApi.delete(query);
            if (res.status == 200) {
                context.commit('removeService', query.serviceTypeId);
            }
            return res;
        } catch (err: any) {
            return err.response.data;
        }
    }
};

export default actions;
