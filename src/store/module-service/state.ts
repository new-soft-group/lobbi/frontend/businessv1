import { Service } from "src/services/models/Service";


export interface ServiceStateInterface {
    serviceType: Service[];
}

function state(): ServiceStateInterface {
    return {
        serviceType: []
    };
}

export default state;
