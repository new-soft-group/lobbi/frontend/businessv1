/* eslint-disable @typescript-eslint/no-unsafe-call */
/* eslint-disable @typescript-eslint/no-unsafe-assignment */
/* eslint-disable @typescript-eslint/no-floating-promises */
import { Service } from 'src/services/models/Service';
import { MutationTree } from 'vuex';
import { ServiceStateInterface } from './state';
import localForage from 'localforage';

const serviceType = localForage.createInstance({
    name: 'lobbi/serviceType'
});

const mutation: MutationTree<ServiceStateInterface> = {
    setService(state, service: Service[]) {
        state.serviceType = service;
        serviceType.setItem('serviceTypeList', JSON.stringify(state.serviceType));
    },
    putService(state, service: Service) {
        state.serviceType.push(service);
    },
    updateService(state, service: Service) {
        const findIndex = state.serviceType.findIndex(
            (element: any) => element.id == service.id
        );
        if (findIndex != -1) {
            state.serviceType[findIndex].id = service.id;
            state.serviceType[findIndex].name = service.name;
        }
    },
    removeService(state, serviceTypeId: string) {
        const findIndex = state.serviceType.findIndex(
            (element: any) => element.id == serviceTypeId
        );
        if (findIndex != -1) {
            state.serviceType.splice(findIndex, 1);
        }
    }
};

export default mutation;
