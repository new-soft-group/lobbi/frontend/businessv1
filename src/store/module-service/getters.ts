import { GetterTree } from 'vuex';
import { StateInterface } from '../index';
import { ServiceStateInterface } from './state';

const getters: GetterTree<ServiceStateInterface, StateInterface> = {
    getService(state) {
        return state.serviceType;
    }
};

export default getters;
