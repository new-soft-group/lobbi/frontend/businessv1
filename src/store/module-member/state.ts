import { MemberList } from 'src/services/models/Member';

export interface MemberStateInterface {
    list: MemberList[];
    allList: MemberList[];
    pendingList: MemberList[];
    removed: boolean;
    pendingRemoved: boolean;
}

function state(): MemberStateInterface {
    return {
        list: [],
        allList: [],
        pendingList: [],
        removed: false,
        pendingRemoved: false
    };
}

export default state;
