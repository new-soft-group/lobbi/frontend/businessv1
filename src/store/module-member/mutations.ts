/* eslint-disable @typescript-eslint/no-unsafe-call */
/* eslint-disable @typescript-eslint/no-unsafe-assignment */
import { MemberList } from 'src/services/models/Member';
import { MutationTree } from 'vuex';
import { MemberStateInterface } from './state';

const mutation: MutationTree<MemberStateInterface> = {
    setMember(state, members: MemberList[]) {
        state.list = members;
    },
    setAllMember(state, members: MemberList[]) {
        state.allList = members;
    },
    setPendingMember(state, members: MemberList[]) {
        state.pendingList = members;
    },
    removeMember(state, memberId: string) {
        const findIndex = state.allList.findIndex(
            (element: any) => element.id == memberId
        );
        if (findIndex != -1) {
            state.allList.splice(findIndex, 1);
            state.removed = true;
        }
    },
    removePendingMember(state, memberId: string) {
        const findIndex = state.pendingList.findIndex(
            (element: any) => element.id == memberId
        );
        if (findIndex != -1) {
            state.pendingList.splice(findIndex, 1);
            state.pendingRemoved = true;
        }
    },
    setRemoved(state, data: boolean) {
        state.removed = data;
    },
    setPendingRemoved(state, data: boolean) {
        state.pendingRemoved = data;
    }
};

export default mutation;
