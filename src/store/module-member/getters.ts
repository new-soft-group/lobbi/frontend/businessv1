import { GetterTree } from 'vuex';
import { StateInterface } from '../index';
import { MemberStateInterface } from './state';

const getters: GetterTree<MemberStateInterface, StateInterface> = {
    getMemberList(state) {
        return state.list;
    }
};

export default getters;
