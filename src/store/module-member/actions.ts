/* eslint-disable @typescript-eslint/no-floating-promises */
/* eslint-disable @typescript-eslint/no-unsafe-assignment */
/* eslint-disable @typescript-eslint/no-unsafe-member-access */
/* eslint-disable @typescript-eslint/no-unsafe-return */
import { ActionTree } from 'vuex';
import { StateInterface } from '../index';
import { MemberStateInterface } from './state';
import MemberApi from 'src/services/apis/MemberApi';
// import localForage from 'localforage';
import {
    MemberListQuery,
    MemberUpdateRequestBody
} from 'src/services/requests/member/MemberListRequest';

const memberApi = new MemberApi();

const actions: ActionTree<MemberStateInterface, StateInterface> = {
    async inviteUser(
        context,
        data: {
            userIds: [];
            branchIds: [];
            roleId: string;
        }
    ) {
        const { userIds, roleId, branchIds } = data;

        try {
            return await memberApi.inviteUser({ userIds, roleId, branchIds });
        } catch (err: any) {
            return err.response.data;
        }
    },
    async searchMembers(context, query: MemberListQuery) {
        try {
            return await memberApi.getMembers(query);
        } catch (err: any) {
            return err.response.data;
        }
    },
    async getList(context, query: MemberListQuery) {
        const businessId = context.rootState.business.businessID;
        const branch = context.rootState.branch.branch;
        try {
            if (businessId && branch) {
                const res: any = await memberApi.getMembers(query);
                if (res.members) {
                    // active
                    if (query.status == 1) {
                        if (query.type == 'all') {
                            context.commit('setAllMember', res.members);
                        } else {
                            context.commit('setMember', res.members);
                        }
                    } else {
                        context.commit('setPendingMember', res.members);
                    }
                }
                return res;
            }
        } catch (err: any) {
            return err.response.data;
        }
    },
    async update(context, body: MemberUpdateRequestBody) {
        try {
            const res: any = await memberApi.update(body);
            return res;
        } catch (err: any) {
            return err.response.data;
        }
    },
    async delete(context, query: any) {
        try {
            const res: any = await memberApi.delete(query);
            if (res.status == 200) {
                if (query.type == 0) {
                    context.commit('removePendingMember', query.memberId);
                } else {
                    context.commit('removeMember', query.memberId);
                }
            }
            return res;
        } catch (err: any) {
            return err.response.data;
        }
    }
};

export default actions;
