import BusinessApi from 'src/services/apis/BusinessApi';
import { Business } from 'src/services/models/Business';
import { BusinessResponseResult } from 'src/services/responses/business/BusinessResponse';
import {
    updateBusinessBody,
    CustomerQuery,
    createCustomerBody
} from 'src/services/requests/business/BusinessRequest';
import { ActionTree } from 'vuex';
import { StateInterface } from '../index';
import { BusinessStateInterface } from './state';
import { IssueTypeQuery } from 'src/services/requests/issuesType/IssueTypeRequest';
import { isContext } from 'vm';
import localForage from 'localforage';

const branchStore = localForage.createInstance({
    name: 'lobbi/branch'
});

const businessApi = new BusinessApi();

const actions: ActionTree<BusinessStateInterface, StateInterface> = {
    setBusinesses(context, businesses: Business) {
        context.commit('setBusinesses', businesses);
    },
    updateBusiness(context, body: any) {
        context.commit('updateBusiness', body);
    },
    async getSetting(context, initial: boolean) {
        const result = await businessApi.setting();
        if (!result) {
            console.error('Error retrieving businesses');
            return;
        }

        const newSetting = {
            businessId: context.state.businessID,
            autoAssignFlag: result.result.setting.autoAssignFlag,
            maxActiveConversation: result.result.setting.maxActiveConversation,
            issueTypeIds: result.result.setting.issueTypeIds
        };

        if (initial) {
            context.commit('setInitialSettingValue', newSetting);
        } else {
            context.commit('setSettingValue', newSetting);
        }

        return result;
    },
    async getCustomer(context, query: CustomerQuery) {
        const result = await businessApi.customer(query);
        if (!result) {
            console.error('Error retrieving customer');
            return;
        }
        context.commit('setCustomer', result.customers);
        return result;
    },
    async createCustomer(context, body: createCustomerBody) {
        const result = await businessApi.createCustomer(body);
        if (!result) {
            console.error('Error creating customer');
            return;
        }
        return result;
    },
    async createCustomerCSV(context, body: any) {
        const result = await businessApi.createCustomerCSV(body);

        return result;
    },
    async putNote(context, body: any) {
        const result = await businessApi.putNote(body);
        return result;
    },
    async putSetting(context, query: any) {
        const result = await businessApi.putSetting(query);
        if (!result) {
            console.error('Error retrieving businesses');
            return;
        }
        context.commit('setSettingValue', query);
        return result;
    },
    async disableBusiness(context, query: any) {
        /*         const formData = new FormData();
        formData.append('type', query); */
        const result = await businessApi.disableBusiness(query);

        return result;
    },
    async putBusiness(context, body: updateBusinessBody) {
        const result = await businessApi.putBusinesses(body);
        if (!result) {
            console.error('Error retrieving businesses');
            return;
        }
        return result;
    },
    async getBusinesses(context) {
        const checkBusinessId = context.state.businessID;

        if (!checkBusinessId) {
            const result = await businessApi.businesses();

            if (result.businesses.length > 0) {
                // set businesses & default business id
                context.commit('setBusinesses', result.businesses);
                context.commit('setBusinessID', result.businesses[0].id);
                context.commit('setBusinessEndPoint');

                // set default branch id
                branchStore.getItem('BranchList').then(function (value: any) {
                    value = JSON.parse(value);
                    if (!value) {
                        context.rootState.branch.branches =
                            result.businesses[0].branches;
                        branchStore.setItem(
                            'BranchList',
                            result.businesses[0].branches
                        );
                    }
                });

                branchStore.getItem('branchFilter').then(function (value: any) {
                    if (value) {
                        const business = value.find((element: any) => {
                            element.businessId == result.businesses[0].id;
                        });

                        if (business) {
                            context.dispatch(
                                'branch/setBranch',
                                result.businesses[0].branches[business.index],
                                { root: true }
                            );
                        } else {
                            context.dispatch(
                                'branch/setBranch',
                                result.businesses[0].branches[0],
                                { root: true }
                            );
                        }
                    } else {
                        context.dispatch(
                            'branch/setBranch',
                            result.businesses[0].branches[0],
                            { root: true }
                        );
                    }
                });
                // set default business role list
                if (result.businesses[0].member.isOwner) {
                    context.dispatch('role/getList', null, { root: true });
                } else {
                    if (result.businesses[0].member.role) {
                        // if role list is available
                        context.commit(
                            'role/setAgentRole',
                            result.businesses[0].member.role.permissions,
                            { root: true }
                        );
                    } else {
                        // if role is null
                        context.commit(
                            'role/setAgentRole',
                            result.businesses[0].member.role,
                            { root: true }
                        );
                    }
                }

                const businessId = context.state.businessID;
                const branch = context.rootState.branch.branch;

                if (businessId && branch) {
                    // get conversations
                    if (
                        context.rootState.role.permissions.conversation?.list ||
                        result.businesses[0].member.isOwner
                    ) {
                        context.dispatch(
                            'conversation/getJoinedConversations',
                            {
                                businessID: businessId,
                                agentID: context.rootState.user.user?.id,
                                status: 1
                            },
                            { root: true }
                        );
                    }

                    // get settings
                    context.dispatch('business/getSetting', true, {
                        root: true
                    });

                    // get business roles
                    if (
                        context.rootState.role.permissions.role?.list ||
                        result.businesses[0].member.isOwner
                    ) {
                        context.dispatch('role/getList', null, { root: true });
                    }
                }
            } else {
                console.error('Error retrieving businesses');
                return;
            }
        }
    },
    async businessView(context, query: IssueTypeQuery) {
        const result = await businessApi.businessView(query);

        if (!result) {
            console.error('Error retrieving businesses');
            return;
        }
        return result;
    },
    setBusinessID(context, businessID: number) {
        context.commit('setBusinessID', businessID);
    },
    setBusinessEndPoint(context) {
        context.commit('setBusinessEndPoint');
    },
    clearBusinesses(context) {
        context.commit('clearBusinesses');
    }
};

export default actions;
