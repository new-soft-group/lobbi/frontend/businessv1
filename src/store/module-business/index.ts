import { Module } from 'vuex';
import { StateInterface } from '../index';
import state, { BusinessStateInterface } from './state';
import actions from './actions';
import getters from './getters';
import mutations from './mutations';

const appModule: Module<BusinessStateInterface, StateInterface> = {
    namespaced: true,
    actions,
    getters,
    mutations,
    state
};

export default appModule;
