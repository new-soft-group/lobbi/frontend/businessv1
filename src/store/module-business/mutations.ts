/* eslint-disable @typescript-eslint/no-unsafe-member-access */
import { Business } from 'src/services/models/Business';
import { Setting } from 'src/services/models/Setting';
import { Customer } from 'src/services/models/Customer';
import { MutationTree } from 'vuex';
import { BusinessStateInterface } from './state';
import localForage from 'localforage';

const branchStore = localForage.createInstance({
    name: 'lobbi/branch'
});

const settingStore = localForage.createInstance({
    name: 'lobbi/business'
});

const mutation: MutationTree<BusinessStateInterface> = {
    setBusinesses(state, businesses: Business[]) {
        state.businesses = businesses;
        const filter = [] as any;
        branchStore.getItem('branchFilter').then(function (value: any) {
            if (!value) {
                state.businesses.forEach((element: any) => {
                    filter.push({
                        businessId: element.id,
                        branchId: element.branches[0].id,
                        index: 0
                    });
                });
                branchStore.setItem('branchFilter', filter);
            } else {
                state.businesses.forEach((element: any) => {
                    if (element.id == value.businessId) {
                        filter.push({
                            businessId: element.id,
                            branchId: value.branchId,
                            index: value.index
                        });
                    } else {
                        filter.push({
                            businessId: element.id,
                            branchId: element.branches[0].id,
                            index: 0
                        });
                    }
                });
                branchStore.setItem('branchFilter', filter);
            }
        });
    },

    setBusinessID(state, businessID: string) {
        state.businessID = businessID;
    },

    setInitialSettingValue(state, setting: Setting) {
        const findIndex = state.settings.findIndex(
            (element: any) => element.businessId == state.businessID
        );
        if (findIndex == -1) {
            state.settings.push(setting);
            settingStore.setItem('settings', JSON.stringify(state.settings));
        }
    },

    setSettingValue(state, setting: Setting) {
        const findIndex = state.settings.findIndex(
            (element: any) => element.businessId == state.businessID
        );

        if (findIndex != -1) {
            state.settings[findIndex] = setting;
            settingStore.setItem('settings', JSON.stringify(state.settings));
        } else {
            state.settings.push(setting);
        }
    },

    setCustomer(state, customer: Customer[]) {
        state.customer = customer;
    },

    updateBusiness(state, body: any) {
        const findIndex = state.businesses.findIndex(
            (element: any) => element.id == state.businessID
        );

        if (findIndex != -1) {
            if (body.description) {
                state.businesses[findIndex].description = body.description;
            }
            if (body.imageUrl) {
                state.businesses[findIndex].imageUrl = body.imageUrl;
            }
            if (body.coverUrl) {
                state.businesses[findIndex].coverUrl = body.coverUrl;
            }
            if (body.banners) {
                state.businesses[findIndex].banners = body.banners;
            }
        }
    },

    setBusinessEndPoint(state) {
        const findBusinessIndex = state.businesses.findIndex(
            (element: any) => element.id == state.businessID
        );

        if (findBusinessIndex != -1) {
            const businessEndPoint =
                state.businesses[findBusinessIndex].service.endpoint;

            state.businessEndPoint = businessEndPoint;
        }
    },

    putBusinessBranch(state, branch: any) {
        const findBusinessIndex = state.businesses.findIndex(
            (element: any) => element.id == state.businessID
        );

        if (findBusinessIndex != -1) {
            const newBranch = {
                id: branch.id,
                name: branch.name
            };
            state.businesses[findBusinessIndex].branches.push(newBranch);
        }
    },

    updateBusinessBranch(state, branch: any) {
        const findBusinessIndex = state.businesses.findIndex(
            (element: any) => element.id == state.businessID
        );

        if (findBusinessIndex != -1) {
            const findBranchIndex = state.businesses[
                findBusinessIndex
            ].branches.findIndex(
                (belement: any) => belement.id == branch.branchId
            );

            if (findBranchIndex != -1) {
                // deactivate
                if (branch.status == 0) {
                    state.businesses[findBusinessIndex].branches.splice(
                        findBranchIndex,
                        1
                    );
                } else {
                    const newBranch = {
                        id: branch.branchId,
                        name: branch.name
                    };

                    state.businesses[findBusinessIndex].branches[
                        findBranchIndex
                    ] = newBranch;
                }
            }
        }
    },

    clearBusinesses(state) {
        state.businesses = [];
        state.businessID = undefined;
        state.businessEndPoint = undefined;
    }
};

export default mutation;
