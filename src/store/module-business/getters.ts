import { GetterTree } from 'vuex';
import { StateInterface } from '../index';
import { BusinessStateInterface } from './state';

const getters: GetterTree<BusinessStateInterface, StateInterface> = {
    getBusinessID(state) {
        return state.businessID;
    },
    getBusinesses(state) {
        return state.businesses;
    },
    getCustomer(state) {
        return state.customer;
    },
    getSetting(state) {
        const findIndex = state.settings.findIndex(
            (setting: any) => setting.businessId == state.businessID
        );
        return state.settings[findIndex];
    },
    getBusiness(state) {
        const findIndex = state.businesses.findIndex(
            (business: any) => business.id == state.businessID
        );

        return state.businesses[findIndex];
    }
};

export default getters;
