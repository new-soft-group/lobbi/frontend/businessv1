import { Business } from 'src/services/models/Business';
import { Setting } from 'src/services/models/Setting';
import { Customer } from 'src/services/models/Customer';

export interface BusinessStateInterface {
    businesses: Business[];
    customer: Customer[];
    businessID?: string;
    settings: Setting[];
    businessEndPoint?: string;
}

function state(): BusinessStateInterface {
    return {
        businesses: [],
        customer: [],
        businessEndPoint: '',
        settings: []
    };
}

export default state;
