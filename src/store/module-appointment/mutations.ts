import { MutationTree } from 'vuex';
import { AppointmentStateInterface } from './state';
import { Appointment } from 'src/services/models/Appointment';

const mutation: MutationTree<AppointmentStateInterface> = {
    setAppointments(state, appointments: Appointment[]) {
        state.appointments = [];
        state.appointments = appointments;
    },
    setAppointment(state, appointment: Appointment) {
        state.appointments.appointments.push(appointment);
    },
    setAppointmentPagination(state, pagination: any) {
        state.appointmentsPagination = pagination;
    },
    updateAppointmentStatus(state, body: any) {
        const findIndex = state.appointments.appointments.findIndex(
            (element: any) => element.id == body.appointmentId
        );

        if (findIndex != -1) {
            state.appointments.appointments[findIndex].status = Number(
                body.status
            );
        }
    }
};

export default mutation;
