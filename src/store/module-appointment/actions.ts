import { ActionTree } from 'vuex';
import { StateInterface } from '../index';
import { AppointmentStateInterface } from './state';
import AppointmentApi from 'src/services/apis/AppointmentApi';
import {
    AppointmentQuery,
    createAppointmentBody,
    updateAppointmentStatusBody
} from 'src/services/requests/appointment/AppointmentRequest';

const appointmentApi = new AppointmentApi();

const actions: ActionTree<AppointmentStateInterface, StateInterface> = {
    async getAppointments(context, query: AppointmentQuery) {
        try {
            const result = await appointmentApi.appointments(query);
            context.commit('setAppointments', result);
            return result;
        } catch (err: any) {
            return err.response.data;
        }
    },
    async createAppointment(context, body: createAppointmentBody) {
        body.businessId = context.rootState.business.businessID ?? '';
        body.branchId = context.rootState.branch.branch.id;

        try {
            const result = await appointmentApi.createAppointment(body);
            context.commit('setAppointment', result.appointment);
            return result;
        } catch (err: any) {
            return err;
        }
    },
    setAppointmentPagination(context, pagination: any) {
        if (pagination == null) {
            pagination = { total: 0, limit: 0, page: 0, totalPage: 0 };
        }
        context.commit('setAppointmentPagination', pagination);
    },
    setAppointments(context, result: any) {
        context.commit('setAppointments', result);
    },
    async updateAppointmentStatus(context, body: updateAppointmentStatusBody) {
        try {
            const result = await appointmentApi.updateAppointmentStatus(body);
            context.commit('updateAppointmentStatus', body);
            return result;
        } catch (err: any) {
            return err;
        }
    }
};

export default actions;
