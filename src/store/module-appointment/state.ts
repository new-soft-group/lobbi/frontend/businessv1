export interface AppointmentStateInterface {
    appointments: any;
    appointmentsPagination: any;
}

function state(): AppointmentStateInterface {
    return {
        appointments: [],
        appointmentsPagination: { total: 0, limit: 0, page: 0, totalPage: 0 }
    };
}

export default state;
