import { GetterTree } from 'vuex';
import { StateInterface } from '../index';
import { AppointmentStateInterface } from './state';

const getters: GetterTree<AppointmentStateInterface, StateInterface> = {
    appointments(state) {
        return state.appointments;
    }
};

export default getters;
