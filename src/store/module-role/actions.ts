/* eslint-disable @typescript-eslint/no-floating-promises */
/* eslint-disable @typescript-eslint/no-unsafe-assignment */
/* eslint-disable @typescript-eslint/no-unsafe-member-access */
/* eslint-disable @typescript-eslint/no-unsafe-return */
import RoleApi from 'src/services/apis/RoleApi';
import { ActionTree } from 'vuex';
import { StateInterface } from '../index';
import { RoleStateInterface } from './state';
import {
    RoleBody,
    DeleteRoleBody,
    UpdateRoleBody,
    RoleListQuery
} from 'src/services/requests/role/RoleCreateRequest';

const roleApi = new RoleApi();

const actions: ActionTree<RoleStateInterface, StateInterface> = {
    async getList(context, query: RoleListQuery) {
        try {
            // context.commit('clearRole');
            const res: any = await roleApi.listRole(query);
            if (res) {
                context.commit('setRole', res.roles);
            }
            return res;
        } catch (err: any) {
            return err.response.data;
        }
    },

    async create(context, body: RoleBody) {
        try {
            const res: any = await roleApi.createRole(body);
            if (res.status == 200) {
                context.commit('putRole', res.result.role);
            }
            return res;
        } catch (err: any) {
            return err.response.data;
        }
    },

    async update(context, body: UpdateRoleBody) {
        try {
            const res: any = await roleApi.updateRole(body);
            if (res.status == 200) {
                const role = {
                    id: body.roleId,
                    name: body.name,
                    color: body.color,
                    permissions: body.permissions
                };
                context.commit('updateRole', role);
            }
            return res;
        } catch (err: any) {
            return err.response.data;
        }
    },

    async delete(context, query: DeleteRoleBody) {
        try {
            const res: any = await roleApi.deleteRole(query);
            if (res.status == 200) {
                context.commit('removeRole', query.roleId);
            }
            return res;
        } catch (err: any) {
            return err.response.data;
        }
    }
};

export default actions;
