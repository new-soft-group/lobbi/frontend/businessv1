/* eslint-disable @typescript-eslint/no-unsafe-call */
/* eslint-disable @typescript-eslint/no-unsafe-assignment */
/* eslint-disable @typescript-eslint/no-floating-promises */
import { MutationTree } from 'vuex';
import { RoleStateInterface } from './state';
import { Roles } from 'src/services/models/Roles';

const mutation: MutationTree<RoleStateInterface> = {
    setRole(state, roles: Roles[]) {
        state.role = roles;

        // conversation (default)
        state.permissions.conversation = {
            list: true,
            join: true,
            update: true,
            end: true,
            transfer: true,
            transferForce: true,
            transferCancel: true
        };

        // roles (default)
        state.permissions.role = {
            list: true,
            create: true,
            view: true,
            update: true,
            delete: true
        };

        // members (default)
        state.permissions.member = {
            list: true,
            invite: true,
            url: true,
            update: true,
            delete: true,
            auditLog: true
        };

        // knowledgebase (default)
        state.permissions.knowledge = {
            create: false,
            update: false,
            delete: false
        };

        // issue type (default)
        state.permissions.issuesType = {
            create: true,
            update: true,
            delete: true
        };

        // label (default)
        state.permissions.label = {
            list: true,
            create: true,
            update: true,
            delete: true
        };

        // remark (default)
        // state.permissions.remark = {
        //     create: true,
        //     update: true,
        //     delete: true
        // };

        // branch (default)
        state.permissions.branch = {
            list: true,
            create: true,
            view: true,
            update: true,
            delete: true
        };

        // branch member (default)
        state.permissions.branchMember = {
            create: true,
            update: true,
            delete: true
        };

        // department (default)
        state.permissions.department = {
            list: false,
            create: false,
            view: false,
            update: false,
            delete: false
        };

        // service (default)
        state.permissions.service = {
            enable: true,
            disable: true,
            endPoint: true
        };
    },
    setAgentRole(state, roles: Roles) {
        // reset role & permission
        state.role = [];
        state.permissions = {};

        if (roles) {
            state.role.push(roles);

            // conversation
            state.permissions.conversation = {
                list: roles.permissions?.includes('conversation/list')
                    ? true
                    : false,
                join: roles.permissions?.includes('conversation/join')
                    ? true
                    : false,
                update: roles.permissions?.includes('conversation/update')
                    ? true
                    : false,
                end: roles.permissions?.includes('conversation/end')
                    ? true
                    : false,
                transfer: roles.permissions?.includes('conversation/transfer')
                    ? true
                    : false,
                transferForce: roles.permissions?.includes(
                    'conversation/transfer/force'
                )
                    ? true
                    : false,
                transferCancel: roles.permissions?.includes(
                    'conversation/transfer/cancel'
                )
                    ? true
                    : false
            };

            // roles
            state.permissions.role = {
                list: roles.permissions?.includes('role/list') ? true : false,
                create: roles.permissions?.includes('role/create')
                    ? true
                    : false,
                view: roles.permissions?.includes('role/view') ? true : false,
                update: roles.permissions?.includes('role/update')
                    ? true
                    : false,
                delete: roles.permissions?.includes('role/delete')
                    ? true
                    : false
            };

            // members
            state.permissions.member = {
                list: roles.permissions?.includes('member/list') ? true : false,
                invite: roles.permissions?.includes('member/invite')
                    ? true
                    : false,
                url: roles.permissions?.includes('member/url') ? true : false,
                update: roles.permissions?.includes('member/update')
                    ? true
                    : false,
                delete: roles.permissions?.includes('member/delete')
                    ? true
                    : false,
                auditLog: roles.permissions?.includes('auditLog/list')
                    ? true
                    : false
            };

            // knowledgebase
            state.permissions.knowledge = {
                create: roles.permissions?.includes('knowledgebase/create')
                    ? true
                    : false,
                update: roles.permissions?.includes('knowledgebase/update')
                    ? true
                    : false,
                delete: roles.permissions?.includes('knowledgebase/delete')
                    ? true
                    : false
            };

            // issue type
            state.permissions.issuesType = {
                create: roles.permissions?.includes('issueType/create')
                    ? true
                    : false,
                update: roles.permissions?.includes('issueType/update')
                    ? true
                    : false,
                delete: roles.permissions?.includes('issueType/delete')
                    ? true
                    : false
            };

            // label
            state.permissions.label = {
                list: roles.permissions?.includes('label/list') ? true : false,
                create: roles.permissions?.includes('label/create')
                    ? true
                    : false,
                update: roles.permissions?.includes('label/update')
                    ? true
                    : false,
                delete: roles.permissions?.includes('label/delete')
                    ? true
                    : false
            };

            // remark
            // state.permissions.remark = {
            //     create: roles.permissions?.includes('remark/list')
            //         ? true
            //         : false,
            //     update: roles.permissions?.includes('remark/update')
            //         ? true
            //         : false,
            //     delete: roles.permissions?.includes('remark/delete')
            //         ? true
            //         : false
            // };

            // branch
            state.permissions.branch = {
                list: roles.permissions?.includes('branch/list') ? true : false,
                create: roles.permissions?.includes('branch/create')
                    ? true
                    : false,
                view: roles.permissions?.includes('branch/view') ? true : false,
                update: roles.permissions?.includes('branch/update')
                    ? true
                    : false,
                delete: roles.permissions?.includes('branch/delete')
                    ? true
                    : false
            };

            // branch member
            state.permissions.branchMember = {
                create: roles.permissions?.includes('branchMember/create')
                    ? true
                    : false,
                update: roles.permissions?.includes('branchMember/update')
                    ? true
                    : false,
                delete: roles.permissions?.includes('branchMember/delete')
                    ? true
                    : false
            };

            // department
            state.permissions.department = {
                list: roles.permissions?.includes('department/list')
                    ? true
                    : false,
                create: roles.permissions?.includes('department/create')
                    ? true
                    : false,
                view: roles.permissions?.includes('department/view')
                    ? true
                    : false,
                update: roles.permissions?.includes('department/update')
                    ? true
                    : false,
                delete: roles.permissions?.includes('department/delete')
                    ? true
                    : false
            };

            // service
            state.permissions.service = {
                enable: roles.permissions?.includes('service/enable')
                    ? true
                    : false,
                disable: roles.permissions?.includes('service/disable')
                    ? true
                    : false,
                endPoint: roles.permissions?.includes('service/endpoint')
                    ? true
                    : false
            };
        } else {
            // conversation (default)
            state.permissions.conversation = {
                list: false,
                join: false,
                update: false,
                end: false,
                transfer: false,
                transferForce: false,
                transferCancel: false
            };

            // roles (default)
            state.permissions.role = {
                list: false,
                create: false,
                view: false,
                update: false,
                delete: false
            };

            // members (default)
            state.permissions.member = {
                list: false,
                invite: false,
                url: false,
                update: false,
                delete: false,
                auditLog: false
            };

            // knowledgebase (default)
            state.permissions.knowledge = {
                create: false,
                update: false,
                delete: false
            };

            // issue type (default)
            state.permissions.issuesType = {
                create: false,
                update: false,
                delete: false
            };

            // label (default)
            state.permissions.label = {
                list: false,
                create: false,
                update: false,
                delete: false
            };

            // remark (default)
            // state.permissions.remark = {
            //     create: false,
            //     update: false,
            //     delete: false
            // };

            // branch (default)
            state.permissions.branch = {
                list: false,
                create: false,
                view: false,
                update: false,
                delete: false
            };

            // branch member (default)
            state.permissions.branchMember = {
                create: false,
                update: false,
                delete: false
            };

            // department (default)
            state.permissions.department = {
                list: false,
                create: false,
                view: false,
                update: false,
                delete: false
            };

            // service (default)
            state.permissions.service = {
                enable: false,
                disable: false,
                endPoint: false
            };
        }
    },
    updateRolePermission(state, roles: Roles) {
        // conversation
        state.permissions.conversation = {
            list: roles.permissions?.includes('conversation/list')
                ? true
                : false,
            join: roles.permissions?.includes('conversation/join')
                ? true
                : false,
            update: roles.permissions?.includes('conversation/update')
                ? true
                : false,
            end: roles.permissions?.includes('conversation/end') ? true : false,
            transfer: roles.permissions?.includes('conversation/transfer')
                ? true
                : false,
            transferForce: roles.permissions?.includes(
                'conversation/transfer/force'
            )
                ? true
                : false,
            transferCancel: roles.permissions?.includes(
                'conversation/transfer/cancel'
            )
                ? true
                : false
        };

        // roles
        state.permissions.role = {
            list: roles.permissions?.includes('role/list') ? true : false,
            create: roles.permissions?.includes('role/create') ? true : false,
            view: roles.permissions?.includes('role/view') ? true : false,
            update: roles.permissions?.includes('role/update') ? true : false,
            delete: roles.permissions?.includes('role/delete') ? true : false
        };

        // members
        state.permissions.member = {
            list: roles.permissions?.includes('member/list') ? true : false,
            invite: roles.permissions?.includes('member/invite') ? true : false,
            url: roles.permissions?.includes('member/url') ? true : false,
            update: roles.permissions?.includes('member/update') ? true : false,
            delete: roles.permissions?.includes('member/delete') ? true : false,
            auditLog: roles.permissions?.includes('auditLog/list')
                ? true
                : false
        };

        // knowledgebase
        state.permissions.knowledge = {
            create: roles.permissions?.includes('knowledgebase/create')
                ? true
                : false,
            update: roles.permissions?.includes('knowledgebase/update')
                ? true
                : false,
            delete: roles.permissions?.includes('knowledgebase/delete')
                ? true
                : false
        };

        // issue type
        state.permissions.issuesType = {
            create: roles.permissions?.includes('issueType/create')
                ? true
                : false,
            update: roles.permissions?.includes('issueType/update')
                ? true
                : false,
            delete: roles.permissions?.includes('issueType/delete')
                ? true
                : false
        };

        // label
        state.permissions.label = {
            list: roles.permissions?.includes('label/list') ? true : false,
            create: roles.permissions?.includes('label/create') ? true : false,
            update: roles.permissions?.includes('label/update') ? true : false,
            delete: roles.permissions?.includes('label/delete') ? true : false
        };

        // remark
        // state.permissions.remark = {
        //     create: roles.permissions?.includes('remark/list') ? true : false,
        //     update: roles.permissions?.includes('remark/update') ? true : false,
        //     delete: roles.permissions?.includes('remark/delete') ? true : false
        // };

        // branch
        state.permissions.branch = {
            list: roles.permissions?.includes('branch/list') ? true : false,
            create: roles.permissions?.includes('branch/create') ? true : false,
            view: roles.permissions?.includes('branch/view') ? true : false,
            update: roles.permissions?.includes('branch/update') ? true : false,
            delete: roles.permissions?.includes('branch/delete') ? true : false
        };

        // branch member
        state.permissions.branchMember = {
            create: roles.permissions?.includes('branchMember/create')
                ? true
                : false,
            update: roles.permissions?.includes('branchMember/update')
                ? true
                : false,
            delete: roles.permissions?.includes('branchMember/delete')
                ? true
                : false
        };

        // department
        state.permissions.department = {
            list: roles.permissions?.includes('department/list') ? true : false,
            create: roles.permissions?.includes('department/create')
                ? true
                : false,
            view: roles.permissions?.includes('department/view') ? true : false,
            update: roles.permissions?.includes('department/update')
                ? true
                : false,
            delete: roles.permissions?.includes('department/delete')
                ? true
                : false
        };

        // service
        state.permissions.service = {
            enable: roles.permissions?.includes('service/enable')
                ? true
                : false,
            disable: roles.permissions?.includes('service/disable')
                ? true
                : false,
            endPoint: roles.permissions?.includes('service/endpoint')
                ? true
                : false
        };
    },
    putRole(state, roles: Roles) {
        const newRole = {
            id: roles.id,
            name: roles.name,
            color: roles.color,
            permissions: roles.permissions,
            totalMember: 0
        };
        state.role.push(newRole);
    },
    updateRole(state, roles: Roles) {
        const findIndex = state.role.findIndex(
            (element: any) => element.id == roles.id
        );
        if (findIndex != -1) {
            // update role permission list
            state.role[findIndex].id = roles.id;
            state.role[findIndex].color = roles.color;
            state.role[findIndex].permissions = roles.permissions;
            state.role[findIndex].name = roles.name;
        }
    },
    removeRole(state, roleId: string) {
        const findIndex = state.role.findIndex(
            (element: any) => element.id == roleId
        );
        if (findIndex != -1) {
            state.role.splice(findIndex, 1);
        }
    },
    clearRole(state) {
        state.role = [];
        state.permissions = {};
    }
};

export default mutation;
