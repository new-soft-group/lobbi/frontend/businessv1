import { GetterTree } from 'vuex';
import { StateInterface } from '../index';
import { RoleStateInterface } from './state';

const getters: GetterTree<RoleStateInterface, StateInterface> = {
    getRole(state) {
        return state.role;
    },
    getPermissions(state) {
        return state.permissions;
    }
};

export default getters;
