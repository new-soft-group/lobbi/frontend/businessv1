import { Roles, Permissions } from 'src/services/models/Roles';

export interface RoleStateInterface {
    role: Roles[];
    permissions: Permissions;
}

function state(): RoleStateInterface {
    return {
        role: [],
        permissions: {}
    };
}

export default state;
