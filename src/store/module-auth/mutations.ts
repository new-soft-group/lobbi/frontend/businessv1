/* eslint-disable @typescript-eslint/no-unsafe-member-access */
import { MutationTree } from 'vuex';
import { AuthStateInterface } from './state';

const mutation: MutationTree<AuthStateInterface> = {
    updateToken(state, token: string) {
        state.token = /* "business/" + */ token;
    },
    logout(state) {
        state.token = undefined;
    }
};

export default mutation;
