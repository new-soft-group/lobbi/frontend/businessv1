/* eslint-disable @typescript-eslint/no-floating-promises */
import { ActionTree } from 'vuex';
import { StateInterface } from '../index';
import { AuthStateInterface } from './state';
import AuthApi from 'src/services/apis/AuthApi';
import { AuthLoginTokenRequestBody } from 'src/services/requests/auth/AuthLoginTokenRequest';
import { AuthRequestCodeBody } from 'src/services/requests/auth/AuthRequestCode';
import { AuthVerifyCodeBody } from 'src/services/requests/auth/AuthVerifyCode';
import { socket as socket2 } from 'boot/socket-secondary';
import { socket } from 'boot/socket-primary';
import { AuthLoginTokenResponseResult } from 'src/services/responses/auth/AuthLoginTokenResponse';

const authApi = new AuthApi();

const actions: ActionTree<AuthStateInterface, StateInterface> = {
    updateToken(context, token: string) {
        context.commit('updateToken', token);
    },
    async loginByToken(context, body: AuthLoginTokenRequestBody) {
        const result = (await authApi.loginByToken(
            body
        )) as AuthLoginTokenResponseResult;
        if (!result) {
            console.error('Error login by token');
            return;
        }

        context.commit('updateToken', result.token);
        context.dispatch('user/setUser', result.user, { root: true });
        context.dispatch('user/setDevice', result.device, { root: true });

        // setTimeout(() => {
        //     socket2.emit('authenticate', result.token);
        // }, 1000);
    },
    async requestCode(context, body: AuthRequestCodeBody) {
        try {
            const res: any = await authApi.requestCode(body);
            return res;
        } catch (err: any) {
            return err.response.data;
        }
    },
    async verifyCode(context, body: AuthVerifyCodeBody) {
        try {
            const result: any = await authApi.verifyCode(body);
            if (result.status == 200) {
                context.commit('updateToken', result.result.token);
                context.dispatch('user/setUser', result.result.user, {
                    root: true
                });
                context.dispatch('user/setDevice', result.result.device, {
                    root: true
                });
            }
            return result;
        } catch (err: any) {
            return err.response.data;
        }
    },
    logout(context) {
        context.dispatch('business/clearBusinesses', null, { root: true });
        context.commit('logout');
    }
};

export default actions;
