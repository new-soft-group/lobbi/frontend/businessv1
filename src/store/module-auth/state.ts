export interface AuthStateInterface {
    token?: string;
}

function state(): AuthStateInterface {
    return {};
}

export default state;
