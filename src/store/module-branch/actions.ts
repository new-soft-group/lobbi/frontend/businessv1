/* eslint-disable @typescript-eslint/no-floating-promises */
/* eslint-disable @typescript-eslint/no-unsafe-assignment */
/* eslint-disable @typescript-eslint/no-unsafe-member-access */
/* eslint-disable @typescript-eslint/no-unsafe-return */
import BranchApi from 'src/services/apis/BranchApi';
import { ActionTree } from 'vuex';
import { StateInterface } from '../index';
import { BranchStateInterface } from './state';
import localForage from 'localforage';
import {
    branchQuery,
    createBranchBody,
    deleteBranchBody,
    updateBranchBody
} from 'src/services/requests/branch/BranchRequest';
const branchApi = new BranchApi();

const branchStore = localForage.createInstance({
    name: 'lobbi/branch'
});
const actions: ActionTree<BranchStateInterface, StateInterface> = {
    setBranch(context, branch: any) {
        if (branch == '') {
            const businesses = context.rootState.business.businesses;
            const businessID = context.rootState.business.businessID;

            const selectedBusiness = businesses.find(
                (business) => business.id == businessID
            );

            if (selectedBusiness?.branches) {
                branchStore.setItem(
                    'BranchList',
                    JSON.stringify(selectedBusiness?.branches)
                );
                context.commit('setBranchList', selectedBusiness?.branches);
                context.commit('setBranchLocal', businessID);
            }
        } else {
            branchStore.setItem('Branch', JSON.stringify(branch));
            context.commit('setBranch', branch);
        }
    },

    setBranchList(context, data: any) {
        context.commit('setBranchList', data);
    },
    setBranchIDLocal(context, body: any) {
        context.commit('setBranchIDLocal', body);
    },
    async getBranches(context, query: branchQuery) {
        try {
            const res: any = await branchApi.list(query);
            if (res.result.branches.length > 0) {
                context.commit('clearBranchList');
                context.commit('setBranchList', res.result.branches);
                context.commit('setBranchPagination', res.result.pagination);
            }
            return res;
        } catch (err: any) {
            return err.response.data;
        }
    },
    async createBranch(context, body: createBranchBody) {
        try {
            const res: any = await branchApi.create(body);
            if (res.status == 200) {
                context.commit('putBranch', res.result.branch);

                // update business branch list
                context.commit(
                    'business/putBusinessBranch',
                    res.result.branch,
                    { root: true }
                );
            }
            return res;
        } catch (err: any) {
            return err.response.data;
        }
    },
    async editBranch(context, body: updateBranchBody) {
        try {
            console.log(body, 99);
            const res: any = await branchApi.update(body);
            /* if (res.status == 200) {
                // update business branch list
                context.commit('business/updateBusinessBranch', body, {
                    root: true
                });
            } */
            return res;
        } catch (err: any) {
            return err.response.data;
        }
    },
    async deleteBranch(context, body: deleteBranchBody) {
        try {
            const res: any = await branchApi.delete(body);
            if (res.status === 200) {
                context.commit('deleteBranch', body.branchId);
            }
            return res;
        } catch (err: any) {
            return err.response.data;
        }
    }
};

export default actions;
