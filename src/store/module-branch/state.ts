import { Branch } from 'src/services/models/Branch';
import { Dictionary } from 'lodash';

export interface BranchStateInterface {
    branches: Branch[];
    branch?: any;
    selectedBranch?: Dictionary<Branch>;
    pagination: [];
    created: boolean;
    deleted: boolean;
}

function state(): BranchStateInterface {
    return {
        branches: [],
        branch: {},
        selectedBranch: {},
        pagination: [],
        created: false,
        deleted: false
    };
}

export default state;
