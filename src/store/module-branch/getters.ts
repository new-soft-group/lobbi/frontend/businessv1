import { GetterTree } from 'vuex';
import { StateInterface } from '../index';
import { BranchStateInterface } from './state';

const getters: GetterTree<BranchStateInterface, StateInterface> = {
    getBranches(state) {
        return state.branches;
    },
    getSelectedBranch(state) {
        return state.selectedBranch;
    },
    getBranchPagination(state) {
        return state.pagination;
    },
    getBranch(state) {
        return state.branch;
    }
};

export default getters;
