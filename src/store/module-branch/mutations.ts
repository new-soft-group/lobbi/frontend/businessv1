/* eslint-disable @typescript-eslint/no-unsafe-call */
/* eslint-disable @typescript-eslint/no-unsafe-assignment */
/* eslint-disable @typescript-eslint/no-floating-promises */
import { MutationTree } from 'vuex';
import { BranchStateInterface } from './state';
import { Branch } from 'src/services/models/Branch';
import { Dictionary } from 'lodash';
import localForage from 'localforage';

const branchStore = localForage.createInstance({
    name: 'lobbi/branch'
});
const mutation: MutationTree<BranchStateInterface> = {
    setBranchList(state, data: Branch[]) {
        state.branches = data;
    },
    setBranch(state, branch: any) {
        state.branch = branch;
    },
    setBranchLocal(state, businessID: any) {
        state.branch = '';
        branchStore.getItem('branchFilter', function (err, value: any) {
            if (value && state.branches != undefined) {
                const selected = value.find(
                    (element: any) => element.businessId == businessID
                );
                if (selected && state.branches.length) {
                    const selectedBranch = state.branches.find(
                        (element: any) => element.id == selected.branchId
                    );
                    if (selectedBranch) {
                        state.branch = selectedBranch;
                        branchStore.setItem(
                            'Branch',
                            JSON.stringify(selectedBranch)
                        );
                    } else {
                        state.branch = state.branches[0];
                        branchStore.setItem(
                            'Branch',
                            JSON.stringify(state.branches[0])
                        );
                    }
                }
            }
        });
    },
    setSelectedBranch(state, data: Dictionary<Branch>) {
        state.selectedBranch = data;
    },
    setBranchPagination(state, data: any) {
        state.pagination = data;
    },
    putBranch(state, branch: Branch) {
        state.branches.push(branch);
        state.created = true;
    },
    deleteBranch(state, branchId: any) {
        const result = state.branches.filter((types) => types.id !== branchId);

        if (result) {
            state.branches = result;
            state.deleted = true;
            /* issueTypeStore.setItem(
                'issueTypeList',
                JSON.stringify(state.issueTypes)
            ); */
        }
    },
    clearBranchList(state) {
        state.branches = [];
    }
};

export default mutation;
