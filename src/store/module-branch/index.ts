import { Module } from 'vuex';
import { StateInterface } from '../index';
import state, { BranchStateInterface } from './state';
import actions from './actions';
import getters from './getters';
import mutations from './mutations';

const userModule: Module<BranchStateInterface, StateInterface> = {
    namespaced: true,
    actions,
    getters,
    mutations,
    state
};

export default userModule;
