import { GetterTree } from 'vuex';
import { StateInterface } from '../index';
import { NotificationStateInterface } from './state';

const getters: GetterTree<NotificationStateInterface, StateInterface> = {
    getAlert(state) {
        return state.alert;
    },
    getPanel(state) {
        return state.panel;
    },
    getList(state) {
        return state.list;
    }
};

export default getters;
