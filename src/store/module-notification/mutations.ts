import { NotificationList } from 'src/services/models/Notification';
import { MutationTree } from 'vuex';
import { NotificationStateInterface } from './state';

const mutation: MutationTree<NotificationStateInterface> = {
    setList(state, list: NotificationList) {
        if (state.list.length == 4) {
            state.list.shift();
        }
        state.list.push(list);
    },
    closePanel(state) {
        if (state.alert == true) {
            state.alert = false;
        }
    },
    setPanel(state) {
        if (state.panel) {
            state.panel = false;
        } else {
            state.panel = true;
        }
    },
    setAlert(state) {
        if (state.alert == true) {
            state.alert = false;
        } else {
            state.alert = true;
        }
    }
};

export default mutation;
