import { NotificationList } from 'src/services/models/Notification';

// eslint-disable-next-line @typescript-eslint/no-empty-interface
export interface NotificationStateInterface {
    list: NotificationList[];
    alert: boolean;
    panel: boolean;
}

function state(): NotificationStateInterface {
    return {
        list: [],
        alert: false,
        panel: false
    };
}

export default state;
