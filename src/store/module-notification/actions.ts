import { ActionTree } from 'vuex';
import { StateInterface } from '../index';
import state, { NotificationStateInterface } from './state';

const actions: ActionTree<NotificationStateInterface, StateInterface> = {
    setList(context, list: any) {
        context.commit('setAlert');
        context.commit('setList', list);
    },
    setPanel(context) {
        context.commit('setPanel');
    },
    setAlert(context) {
        context.commit('setAlert');
    },
    closePanel(context) {
        context.commit('closePanel');
    }
};

export default actions;
