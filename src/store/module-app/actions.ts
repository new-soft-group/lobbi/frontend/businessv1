import { ActionTree } from 'vuex';
import { StateInterface } from '../index';
import { AppStateInterface } from './state';

const actions: ActionTree<AppStateInterface, StateInterface> = {
    init(context) {
        const token = context.rootState.auth.token;

        if (token) {
            setTimeout(() => {
                context.dispatch('triggerInit');
            }, 500);
        }
    },
    triggerInit(context) {
        // get businesses
        context.dispatch('business/getBusinesses', null, {
            root: true
        });
    },
    incrementScrollCounter(context) {
        context.commit('incrementScrollCounter');
    }
};

export default actions;
