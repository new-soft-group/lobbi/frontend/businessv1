export interface AppStateInterface {
    selectedTab: string;
    selectedConversationTab: string;
    scrollCounter: number;
}

function state(): AppStateInterface {
    return {
        selectedTab: 'chat',
        selectedConversationTab: 'chats',
        scrollCounter: 0
    };
}

export default state;
