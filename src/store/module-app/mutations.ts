import { MutationTree } from 'vuex';
import { AppStateInterface } from './state';

const mutation: MutationTree<AppStateInterface> = {
    incrementScrollCounter(state) {
        state.scrollCounter++;
    }
};

export default mutation;
