export interface AttendanceStateInterface {
    attendance: any;
    selectedAttendanceId: string;
    mode: any;
}

function state(): AttendanceStateInterface {
    return {
        attendance: [],
        selectedAttendanceId: '',
        mode: []
    };
}

export default state;
