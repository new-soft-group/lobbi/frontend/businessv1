import { GetterTree } from 'vuex';
import { StateInterface } from '../index';
import { AttendanceStateInterface } from './state';

const getters: GetterTree<AttendanceStateInterface, StateInterface> = {
    getMode(state) {
        const findIndex = state.mode.findIndex(
            (element: any) => element.attendanceId == state.selectedAttendanceId
        );

        if (findIndex != -1) {
            return state.mode[findIndex].status;
        } else {
            return false;
        }
    },
    getStartTime(state) {
        const findIndex = state.attendance.findIndex(
            (element: any) => element.id == state.selectedAttendanceId
        );
        if (findIndex != -1) {
            return state.attendance[findIndex].startDateTime;
        } else {
            return;
        }
    }
};

export default getters;
