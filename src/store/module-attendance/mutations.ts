import { MutationTree } from 'vuex';
import { AttendanceStateInterface } from './state';
import localForage from 'localforage';

const attendanceStore = localForage.createInstance({
    name: 'lobbi/attendance'
});

const mutation: MutationTree<AttendanceStateInterface> = {
    setAttendance(state, data) {
        state.mode.push({
            businessId: data.businessId,
            attendanceId: data.result.attendance.id,
            status: true
        });
        state.attendance.push(data.result.attendance);
        state.selectedAttendanceId = data.result.attendance.id;

        attendanceStore.setItem(
            'selectedAttendanceId',
            JSON.stringify(data.result.attendance.id)
        );

        attendanceStore.setItem(
            data.result.attendance.id,
            JSON.stringify(data.result.attendance)
        );
    },
    clearAttendance(state, data) {
        const findIndex = state.mode.findIndex(
            (element: any) => element.businessId == data.businessId
        );

        if (findIndex != -1) {
            state.mode.splice(findIndex, 1);

            const attendanceIndex = state.attendance.findIndex(
                (element: any) => element.businessId == data.businessId
            );

            state.attendance.splice(attendanceIndex, 1);
            if (data.attendanceId == state.selectedAttendanceId) {
                state.selectedAttendanceId = '';
            } else {
                state.selectedAttendanceId = data.attendanceId;
            }
            attendanceStore.removeItem(data.attendanceId);
            attendanceStore.setItem('selectedAttendanceId', undefined);
        }
    }
};

export default mutation;
