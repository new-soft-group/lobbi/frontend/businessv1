import { ActionTree } from 'vuex';
import { StateInterface } from '../index';
import { AttendanceStateInterface } from './state';
import AttendanceApi from 'src/services/apis/AttendanceApi';

const attendanceApi = new AttendanceApi();

const actions: ActionTree<AttendanceStateInterface, StateInterface> = {
    async checkIn() {
        try {
            return await attendanceApi.checkIn();
        } catch (err: any) {
            return err.response.data;
        }
    },

    async checkOut(context, businessId) {
        try {
            const findIndex = context.state.mode.findIndex(
                (element: any) => element.businessId == businessId
            );

            const attendanceId = context.state.mode[findIndex].attendanceId;

            await attendanceApi.checkOut({
                attendanceId
            });

            const data = {
                businessId,
                attendanceId
            };

            context.commit('clearAttendance', data);
        } catch (err: any) {
            return err.response.data;
        }
    },

    async view(context) {
        try {
            const result = await attendanceApi.view();
            const businessId = context.rootState.business.businessID;

            const data = {
                businessId,
                result
            };

            context.commit('setAttendance', data);
            return result;
        } catch (err: any) {
            return err.response.data;
        }
    }
};

export default actions;
