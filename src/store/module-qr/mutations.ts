import { MutationTree } from 'vuex';
import { QrStateInterface } from './state';

const mutation: MutationTree<QrStateInterface> = {};

export default mutation;
