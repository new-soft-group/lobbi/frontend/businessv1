import { QRGenerateRequestBody } from 'src/services/requests/qr/QRGenerateRequest';
import { ActionTree } from 'vuex';
import { StateInterface } from '../index';
import { QrStateInterface } from './state';
import QrApi from 'src/services/apis/QrApi';
import { QRGenerateResponseResult } from 'src/services/responses/qr/QrGenerateResponse';

const qrApi = new QrApi();

const actions: ActionTree<QrStateInterface, StateInterface> = {
    async generate(context, body: QRGenerateRequestBody) {
        const result = (await qrApi.generate(body)) as QRGenerateResponseResult;
        if (!result) {
            console.error('Error retrieving QR generated hash');
            return;
        }

        return result.hash;
    }
};

export default actions;
