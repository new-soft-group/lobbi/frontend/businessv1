import { GetterTree } from 'vuex';
import { StateInterface } from '../index';
import { QrStateInterface } from './state';

const getters: GetterTree<QrStateInterface, StateInterface> = {
    someAction(/* context */) {
        // your code
    }
};

export default getters;
