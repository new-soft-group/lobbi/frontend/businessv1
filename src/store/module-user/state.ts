import { User } from 'src/services/models/User';
import { Device } from 'src/services/models/Device';

export interface UserStateInterface {
    user?: User;
    isDarkMode: boolean;
    device?: Device;
}

function state(): UserStateInterface {
    return {
        isDarkMode: window.matchMedia('(prefers-color-scheme: dark)').matches
    };
}

export default state;
