/* eslint-disable @typescript-eslint/no-unsafe-call */
/* eslint-disable @typescript-eslint/no-unsafe-assignment */
import { merge } from 'lodash';
import { User } from 'src/services/models/User';
import { MutationTree } from 'vuex';
import { UserStateInterface } from './state';
import { i18n } from 'boot/i18n';
import { Device } from 'src/services/models/Device';

const mutation: MutationTree<UserStateInterface> = {
    setUser(state, user: User) {
        state.user = user;
    },
    setDevice(state, device: Device) {
        state.device = device;
    },
    update(state, user: User) {
        state.user = merge(state.user ?? {}, user);
    },
    setDarkMode(state, value: boolean) {
        state.isDarkMode = value;
    },
    setLocale(state, locale: 'en' | 'my' | 'zh') {
        state.user!.locale = locale;
        i18n.global.locale = locale;
    }
};

export default mutation;
