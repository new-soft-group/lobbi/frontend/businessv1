/* eslint-disable @typescript-eslint/no-floating-promises */
/* eslint-disable @typescript-eslint/no-unsafe-assignment */
/* eslint-disable @typescript-eslint/no-unsafe-member-access */
/* eslint-disable @typescript-eslint/no-unsafe-return */
import { ActionTree } from 'vuex';
import { StateInterface } from '../index';
import { UserStateInterface } from './state';
import UserApi from 'src/services/apis/UserApi';
import AuditApi from 'src/services/apis/AuditApi';
import { User } from 'src/services/models/User';
import { UserProfileResponseResult } from 'src/services/responses/user/UserProfileResponse';
import { UserAvatarRequestBody } from 'src/services/requests/user/UserAvatarRequest';
import { UserSearchRequestQuery } from 'src/services/requests/user/UserSearchRequest';
import { AuditLogQuery } from 'src/services/requests/user/AuditLogRequest';
import { Device } from 'src/services/models/Device';

/* const messageStore = localForage.createInstance({
    name: 'lobbi/messages'
});
const conversationStore = localForage.createInstance({
    name: 'lobbi/conversations'
});
const relationStore = localForage.createInstance({
    name: 'lobbi/relations'
});
const stickerStore = localForage.createInstance({
    name: 'lobbi/stickers'
}); */

const userApi = new UserApi();
const auditApi = new AuditApi();

const actions: ActionTree<UserStateInterface, StateInterface> = {
    setUser(context, user: User) {
        context.commit('setUser', user);
    },
    setDevice(context, device: Device) {
        context.commit('setDevice', device);
    },
    async search(context, query: UserSearchRequestQuery) {
        try {
            return await userApi.search(query);
        } catch (err: any) {
            return err.response.data;
        }
    },
    /*     async avatarUpload(
        context,
        data: { file: File; avatar?: boolean; thumbnail?: boolean }
    ) {
        const { file, avatar, thumbnail } = data;

        // Upload the file
        const formData = new FormData();
        formData.append('file', file);

        // Generate signed URL
        const result: FileUploadResponseResult = (await fileApi.upload(
            formData,
            { avatar, thumbnail } 
        ));
        if (!result) {
            console.error('Error generating file URL');
            return;
        }

        return result.file;
    }, */
    async getAudit(context, query: AuditLogQuery) {
        try {
            return await auditApi.getAudit(query);
        } catch (err: any) {
            return err.response.data;
        }
    },
    async update(context, data: { user: User; silent: boolean }) {
        const { user, silent } = data;

        let result: any = { status: 200 };
        const currentUser = context.state.user;

        try {
            context.commit('update', user);
            if (!silent) {
                const { name, description, username } = user;
                result = await userApi.update({ name, description, username });
            }
            return result;
        } catch (err: any) {
            context.commit('update', currentUser);
            return err.response.data;
        }
    },
    async getProfile(context) {
        if (context.state.user) {
            context.commit('setLocale', context.state.user?.locale);
        }

        // Get profile
        const result = (await userApi.profile()) as UserProfileResponseResult;
        if (!result) {
            console.error('Error retrieving profile view');
            return;
        }

        // Update the current user details
        context.commit('setUser', result.user);

        return result.user;
    },
    async flag(
        context,
        data: {
            name:
                | 'contact'
                | 'message'
                | 'password'
                | 'tutorial'
                | 'notification'
                | 'receipt';
            value: 0 | 1;
        }
    ) {
        const { name, value } = data;

        const currentUser = context.state.user;
        const tempUser = JSON.parse(JSON.stringify(currentUser));

        try {
            const newFlag: Record<string, any> = {};
            newFlag[`${name}Flag`] = value;
            tempUser.flags = { ...tempUser.flags, ...newFlag };
            context.commit('update', tempUser);
            return await userApi.flag({ name }, { value });
        } catch (err: any) {
            context.commit('update', currentUser);
            return err.response.data;
        }
    },
    async avatar(context, body: UserAvatarRequestBody) {
        const currentUser = context.state.user;

        try {
            context.commit('update', {
                ...currentUser,
                imagePath: body.imagePath
            });
            return await userApi.avatar(body);
        } catch (err: any) {
            context.commit('update', currentUser);
            return err.response.data;
        }
    },
    async logout(context) {
        try {
            await userApi.logout({ deviceId: context.state.device?.id ?? '' });
        } catch (err: any) {}

        context.dispatch('auth/logout', null, { root: true });
        context.dispatch('menu/clearPage', null, { root: true });
    },
    setDarkMode(context, value: boolean) {
        context.commit('setDarkMode', value);
    },
    async setLocale(context, locale: 'en' | 'my' | 'zh') {
        const currentUser = context.state.user;

        if (currentUser?.locale == locale) {
            return;
        }

        try {
            context.commit('setLocale', locale);
            return await userApi.locale({ locale });
        } catch (err: any) {
            context.commit('setLocale', currentUser?.locale);
            return err.response.data;
        }
    }
};

export default actions;
