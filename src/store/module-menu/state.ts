export interface MenuStateInterface {
    home: boolean;
    helpdesk: boolean;
    attendance: boolean;
    notification: boolean;
    preference: boolean;
    menuDrawer: boolean;
    noteDrawer: boolean;
    ongoingDrawer: boolean;
    settingDrawer: boolean;
    notifyStatus: boolean;
    selectedShortcut: string;
}

function state(): MenuStateInterface {
    return {
        home: true,
        helpdesk: false,
        attendance: false,
        notification: false,
        preference: false,
        menuDrawer: false,
        noteDrawer: false,
        ongoingDrawer: false,
        settingDrawer: false,
        notifyStatus: false,
        selectedShortcut: 'chatting'
    };
}

export default state;
