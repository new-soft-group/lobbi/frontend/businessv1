import { MutationTree } from 'vuex';
import { MenuStateInterface } from './state';

const mutation: MutationTree<MenuStateInterface> = {
    setPage(
        state,
        page:
            | 'home'
            | 'helpdesk'
            | 'attendance'
            | 'preference'
            | 'notification'
            | 'overview'
    ) {
        state.home = false;
        state.helpdesk = false;
        state.attendance = false;
        state.preference = false;
        state.notification = false;

        if (page == 'home') {
            state.home = true;
        }
        if (page == 'helpdesk') {
            state.helpdesk = true;
        }
        if (page == 'attendance') {
            state.attendance = true;
        }
        if (page == 'preference') {
            state.preference = true;
        }
        if (page == 'notification') {
            state.notification = true;
        }
    },
    clearPage(state) {
        state.home = false;
        state.helpdesk = false;
        state.attendance = false;
        state.preference = false;
        state.notification = false;
    },
    setSelectedShortcut(state, page) {
        state.selectedShortcut = page;
    },
    setMenuDrawer(state, status) {
        state.menuDrawer = status;
    },
    setNoteDrawer(state, status) {
        state.noteDrawer = status;
    },
    setOngoingDrawer(state, status) {
        state.ongoingDrawer = status;
    },
    setSettingDrawer(state, status) {
        state.settingDrawer = status;
    },
    setNotify(state, status) {
        state.notifyStatus = status;
    }
};

export default mutation;
