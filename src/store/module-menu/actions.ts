import { ActionTree } from 'vuex';
import { StateInterface } from '../index';
import { MenuStateInterface } from './state';

const actions: ActionTree<MenuStateInterface, StateInterface> = {
    setPage(context, page: string) {
        context.commit('setPage', page);
    },
    clearPage(context) {
        context.commit('clearPage');
    },
    setSelectedShortcut(context, page: string) {
        context.commit('setSelectedShortcut', page);
    },
    setMenuDrawer(context, status: boolean) {
        context.commit('setMenuDrawer', status);
    },
    setNoteDrawer(context, status: boolean) {
        context.commit('setNoteDrawer', status);
    },
    setOngoingDrawer(context, status: boolean) {
        context.commit('setOngoingDrawer', status);
    },
    setSettingDrawer(context, status: boolean) {
        context.commit('setSettingDrawer', status);
    },
    setNotify(context, status: boolean) {
        context.commit('setNotify', status);
    }
};

export default actions;
