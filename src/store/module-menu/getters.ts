import { GetterTree } from 'vuex';
import { StateInterface } from '../index';
import { MenuStateInterface } from './state';

const getters: GetterTree<MenuStateInterface, StateInterface> = {
    someAction(/* context */) {
        // your code
    },
    getMenuDrawer(state) {
        return state.menuDrawer;
    },
    getNoteDrawer(state) {
        return state.noteDrawer;
    },
    getOngoingDrawer(state) {
        return state.ongoingDrawer;
    },
    getSettingDrawer(state) {
        return state.settingDrawer;
    },
    getNotifyStatus(state) {
        return state.notifyStatus;
    }
};

export default getters;
