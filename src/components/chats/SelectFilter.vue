<template>
    <div class="q-pa-md">
        <div v-if="state.header == 'Branch'">
            <q-select
                outlined
                class="filterSelect"
                v-model="state.selectedBranch"
                use-input
                dense
                input-debounce="500"
                :label="state.header"
                :options="branchData"
                option-value="id"
                option-label="name"
                ref="agentDialog"
                :behavior="menu"
                @update:model-value="updateValue($event, 'Branch')"
            >
                <template v-slot:no-option>
                    <q-item>
                        <q-item-section class="text-grey">
                            No results
                        </q-item-section>
                    </q-item>
                </template>
            </q-select>
        </div>
        <div v-if="state.header == 'Agent'">
            <q-select
                outlined
                class="filterSelect"
                v-model="state.selectedAgent"
                use-input
                dense
                input-debounce="500"
                :label="state.header"
                :options="state.memberLists"
                option-label="name"
                @filter="filterAgent"
                ref="agentDialog"
                clearable
                @update:model-value="updateValueCustom()"
            >
                <template v-slot:no-option>
                    <q-item>
                        <q-item-section class="text-grey">
                            No results
                        </q-item-section>
                    </q-item>
                </template>
            </q-select>
        </div>
        <div v-if="state.header == 'Client'">
            <q-select
                outlined
                class="filterSelect"
                v-model="state.selectedClient"
                use-input
                dense
                input-debounce="500"
                :label="state.header"
                :options="state.customerLists"
                option-label="name"
                @filter="filterClient"
                ref="clientDialog"
                clearable
                @update:model-value="updateValueCustom()"
            >
                <template v-slot:no-option>
                    <q-item>
                        <q-item-section class="text-grey">
                            No results
                        </q-item-section>
                    </q-item>
                </template>
            </q-select>
        </div>
        <div v-else-if="state.header == 'Issue Type'">
            <q-select
                outlined
                class="filterSelect"
                v-model="state.selectedIssueType"
                use-input
                dense
                input-debounce="0"
                :label="state.header"
                :options="issueList"
                option-label="name"
                @filter="filterIssue"
                clearable
                ref="issueDialog"
                @update:model-value="updateValueCustom()"
            >
                <template v-slot:no-option>
                    <q-item>
                        <q-item-section class="text-grey">
                            No results
                        </q-item-section>
                    </q-item>
                </template>
            </q-select>
        </div>
        <div v-else-if="state.header == 'Status'">
            <q-select
                outlined
                class="filterSelect"
                v-model="state.selectedStatus"
                input-debounce="0"
                dense
                :label="state.header"
                :options="state.statusList"
                option-label="name"
                @update:model-value="updateValueCustom()"
                clearable
            >
                <template v-slot:no-option>
                    <q-item>
                        <q-item-section class="text-grey">
                            No results
                        </q-item-section>
                    </q-item>
                </template>
            </q-select>
        </div>
    </div>
</template>
<script lang="ts">
/* eslint-disable @typescript-eslint/no-unsafe-call */
/* eslint-disable @typescript-eslint/no-unsafe-member-access */
/* eslint-disable @typescript-eslint/no-unsafe-assignment */
/* eslint-disable @typescript-eslint/no-explicit-any*/
/* eslint-disable @typescript-eslint/no-floating-promises */
/* eslint-disable @typescript-eslint/no-misused-promises */
/* eslint-disable @typescript-eslint/require-await */
/* eslint-disable @typescript-eslint/restrict-template-expressions */
/* eslint-disable @typescript-eslint/no-implicit-any-catch */

import { useStore } from 'src/store';
import localForage from 'localforage';
import {
    defineComponent,
    ref,
    reactive,
    watch,
    computed,
    onMounted
} from 'vue';
import { debounce } from 'lodash';
import { mapGetters } from 'vuex';
import { date } from 'quasar';

export default defineComponent({
    name: 'AllChat',
    computed: {
        ...mapGetters({
            issueTypes: 'issueType/getIssueTypesList',
            user: 'user/getUser',
            branch: 'branch/getBranch'
        })
    },
    props: {
        header: {
            type: String,
            required: true
        },
        reset: {
            type: Boolean,
            default: false
        },
        branchData: {
            type: Array,
            required: false
        },
        selectedBranch: {
            type: Object,
            required: false
        }
    },
    setup(props, { emit }) {
        const currentTime = date.formatDate(Date.now(), 'HH:mm:ss');
        let show = 1;
        const store = useStore();
        const branchStore = localForage.createInstance({
            name: 'lobbi/branch'
        });
        const filterStore = localForage.createInstance({
            name: 'lobbi/filter'
        });

        // data
        let search = ref('');
        let clientDialog = ref();
        let agentDialog = ref();
        let issueDialog = ref();
        let branchList = ref(props.branchData) as any;
        let state = reactive({
            selectedUser: ref(''),
            header: props.header,
            memberList: [] as any,
            memberLists: [] as any,
            customerList: [] as any,
            customerLists: [] as any,
            issueType: [] as any,
            selectedAgent: '',
            selectedClient: '',
            selectedIssueType: '',
            selectedStatus: '',
            selectedBranch: '' as any,
            statusList: [
                {
                    name: 'Waiting',
                    value: 0
                },
                {
                    name: 'Active',
                    value: 1
                },
                {
                    name: 'Closed',
                    value: 2
                }
            ] as any,
            branch: computed(() => {
                return store.state.branch.branch;
            })
        });

        const issueList = ref(state.issueType);

        const selectedBusinessId = computed(() => {
            return store.state.business.businessID;
        });

        const filterAgent = (val: any, doneFn: any) => {
            doneFn(() => {
                getAgent(val);
            });
        };

        const filterClient = (val: any, doneFn: any) => {
            doneFn(() => {
                getClient(val);
            });
        };

        const filterIssue = (val: any, update: any) => {
            getIssueTypes();
            if (val === '') {
                update(() => {
                    issueList.value = state.issueType;
                });
                return;
            }

            update(() => {
                const needle = val.toLowerCase();
                issueList.value = issueList.value.filter(
                    (v: any) => v.name.toLowerCase().indexOf(needle) > -1
                );
            });
        };

        const updateValue = (value: any, header: any) => {
            emit('update-confirm', value, header);
        };

        const updateValueCustom = async () => {
            let filterList = {
                agent: state.selectedAgent,
                client: state.selectedClient,
                issueType: state.selectedIssueType,
                status: state.selectedStatus
            };
            emit('update-confirm', filterList);
        };
        const getLocalForage = () => {
            const selectedShortcut = store.state.menu.selectedShortcut;
            let name = '';

            if (selectedShortcut == 'clients') {
                name = 'clientFilter';
            }
            if (selectedShortcut == 'chatting') {
                name = 'chatFilter';
            }
            filterStore.getItem(name, function (err, value: any) {
                value = JSON.parse(value);
                if (value) {
                    state.selectedAgent = value.agent?.id ? value.agent : '';
                    state.selectedClient = value.client?.id ? value.client : '';
                    state.selectedIssueType = value.issueType?.id
                        ? value.issueType
                        : '';
                    state.selectedStatus = value.status ? value.status : '';
                } else {
                    state.selectedAgent = '';
                    state.selectedClient = '';
                    state.selectedIssueType = '';
                    state.selectedStatus = '';
                }
            });

            branchStore.getItem('branchFilter', function (err, value: any) {
                if (value && branchList.value != undefined) {
                    const selected = value.find(
                        (element: any) =>
                            element.businessId == selectedBusinessId.value
                    );
                    if (selected && branchList.value.length) {
                        if (branchList.value[selected.index]) {
                            const findIndex = branchList.value.findIndex(
                                (element: any) =>
                                    element.id == selected.branchId
                            );
                            if (findIndex != -1) {
                                state.selectedBranch =
                                    branchList.value[findIndex];
                            }
                            store.dispatch(
                                'branch/setBranch',
                                state.selectedBranch
                            );
                        }
                    }
                }
            });
        };

        const getAgent = debounce(async (search: string) => {
            let memberList: any = [];
            if (search) {
                const result = await store.dispatch('member/searchMembers', {
                    page: 1,
                    limit: 10,
                    keyword: search
                });

                if (result.members.length > 0) {
                    result.members.forEach((element: any) => {
                        let results = {
                            id: element.user.id,
                            name: element.user.name
                        };
                        memberList.push(results);
                    });

                    state.memberLists = memberList;

                    if (agentDialog.value) {
                        agentDialog.value.refresh();
                    }
                }
            } else {
                const result = await store.dispatch('member/searchMembers', {
                    page: 1,
                    limit: 10,
                });

                if (result.members.length > 0) {
                    result.members.forEach((element: any) => {
                        let results = {
                            id: element.user.id,
                            name: element.user.name
                        };
                        memberList.push(results);
                    });

                    state.memberLists = memberList;

                    if (agentDialog.value) {
                        agentDialog.value.refresh();
                    }
                }
            }
        }, 500);

        const getClient = debounce(async (search: string) => {
            let customerLists: any = [];
            if (search) {
                const result = await store.dispatch('business/getCustomer', {
                    page: 1,
                    limit: 10,
                    keyword: search
                });

                if (result.customers.length > 0) {
                    result.customers.forEach((element: any) => {
                        let results = {
                            id: element.id,
                            name: element.name
                        };
                        customerLists.push(results);
                    });

                    state.customerLists = customerLists;

                    if (clientDialog.value) {
                        clientDialog.value.refresh();
                    }
                }
            } else {
                const result = await store.dispatch('business/getCustomer', {
                    page: 1,
                    limit: 10,
                });

                if (result.customers.length > 0) {
                    result.customers.forEach((element: any) => {
                        let results = {
                            id: element.id,
                            name: element.name
                        };
                        customerLists.push(results);
                    });
                    state.customerLists = result.customers;

                    if (clientDialog.value) {
                        clientDialog.value.refresh();
                    }
                }
            }
        }, 500);

        const getIssueTypes = async () => {
            state.issueType = [];

            const res = await store.dispatch(
                'issueType/getIssueTypeByBusinessID',
                { businessId: store.state.business.businessID }
            );

            if (res) {
                res.issueTypes.forEach((element: any) => {
                    state.issueType.push(element);
                });

                if (issueDialog.value) {
                    issueDialog.value.refresh();
                }
            }
        };
        const getCustomer = async (results: any) => {
            if (props.header == 'Client' && state.selectedClient != '') {
                let query = {
                    agentId: '',
                    labelIds: '',
                    keyword: results,
                    page: 1,
                    limit: 20
                };
                const result: any = await store.dispatch(
                    'business/getCustomer',
                    query
                );
                if (result.customers.length > 0) {
                    state.customerList = result.customers;

                    if (clientDialog.value) {
                        clientDialog.value.refresh();
                    }
                }
            } else if (props.header == 'Client' && state.selectedClient == '') {
                const result: any = await store.dispatch(
                    'business/getCustomer'
                );
                if (result.customers.length > 0) {
                    state.customerList = result.customers;

                    if (clientDialog.value) {
                        clientDialog.value.refresh();
                    }
                }
            }
        };

        const reset = () => {
            state.selectedAgent = '';
            state.selectedStatus = '';
            state.selectedIssueType = '';
            state.selectedClient = '';
            store.dispatch('filter/clearFilterList');
        };

        // watch
        store.watch(
            (state) => state.branch.branch,
            () => {
                state.selectedBranch = store.state.branch.branch;
            }
        );

        store.watch(
            (state) => state.menu.selectedShortcut,
            () => {
                getLocalForage();
            }
        );

        store.watch(
            (state) => state.business.businessID,
            () => {
                reset();
            }
        );

        watch(
            () => props.selectedBranch,
            () => {
                state.selectedBranch = props.selectedBranch;
            }
        );

        watch(
            () => props.reset,
            (currentValue) => {
                if (currentValue) {
                    reset();
                }
            }
        );

        onMounted(getLocalForage);

        return {
            tab: ref('chatting'),
            dense: ref(true),
            clientDialog,
            agentDialog,
            issueDialog,
            state,
            search,
            currentTime,
            show,
            issueList,
            updateValue,
            filterIssue,
            filterAgent,
            filterClient,
            getCustomer,
            getAgent,
            getLocalForage,
            branchList,
            updateValueCustom
        };
    }
});
</script>
