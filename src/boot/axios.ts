/* eslint-disable @typescript-eslint/no-floating-promises */
/* eslint-disable @typescript-eslint/no-unsafe-member-access */
/* eslint-disable @typescript-eslint/no-unsafe-assignment */
import { boot } from 'quasar/wrappers';
import axios, { AxiosInstance } from 'axios';

declare module '@vue/runtime-core' {
    interface ComponentCustomProperties {
        $axios: AxiosInstance;
    }
}

const api = axios.create({ baseURL: process.env.API_URL ?? '' });
const businessApi = axios.create({
    baseURL: process.env.API_BUSINESS_URL ?? ''
});
const helpdeskApi = axios.create({
    baseURL: process.env.API_HELPDESK_URL ?? ''
});
const attendanceApi = axios.create({
    baseURL: process.env.API_ATTENDANCE_URL ?? ''
});

export default boot(({ app, store }) => {
    // Attach bearer token if existed
    api.interceptors.request.use(
        (config) => {
            const token = store.state.auth.token;

            if (token) {
                config.headers.Authorization = token;
            }
            return config;
        },
        (error) => {
            return Promise.reject(error);
        }
    );

    // Attach bearer token & business id if existed
    businessApi.interceptors.request.use(
        (config) => {
            const token = store.state.auth.token;
            const businessId = store.state.business.businessID;
            const branch = store.state.branch.branch;

            if (token) {
                config.headers.Authorization = token;
                if (businessId) {
                    config.headers.common['x-business-id'] = businessId;
                }
                if (branch) {
                    config.headers.common['x-branch-id'] = branch?.id;
                }
            }
            return config;
        },
        (error) => {
            return Promise.reject(error);
        }
    );

    // Attach bearer token & business id if existed
    helpdeskApi.interceptors.request.use(
        (config) => {
            config.baseURL = store.state.business.businessEndPoint;
            const token = store.state.auth.token;
            const businessId = store.state.business.businessID;
            const branch = store.state.branch.branch;

            if (token) {
                config.headers.Authorization = token;
                if (businessId) {
                    config.headers.common['x-business-id'] = businessId;
                }
                if (branch) {
                    config.headers.common['x-branch-id'] = branch?.id;
                }
            }
            return config;
        },
        (error) => {
            return Promise.reject(error);
        }
    );

    // Attach bearer token & business id if existed
    attendanceApi.interceptors.request.use(
        (config) => {
            const token = store.state.auth.token;
            const businessId = store.state.business.businessID;
            const branch = store.state.branch.branch;
            // console.log(token);

            if (token) {
                config.headers.Authorization = token;
                if (businessId) {
                    config.headers.common['x-business-id'] = businessId;
                }
                if (branch) {
                    config.headers.common['x-branch-id'] = branch?.id;
                }
            }
            return config;
        },
        (error) => {
            return Promise.reject(error);
        }
    );

    // Handle error
    api.interceptors.response.use(undefined, function (err: any) {
        return new Promise(() => {
            /* if (err.response?.status === 401) {
                store.dispatch('user/logout').then(() => {
                    // Redirect
                    redirect({ name: 'index' });
                });
            } */
            throw err;
        });
    });

    businessApi.interceptors.response.use(undefined, function (err: any) {
        return new Promise(() => {
            /* if (err.response?.status === 401) {
                store.dispatch('user/logout').then(() => {
                    // Redirect
                    redirect({ name: 'index' });
                });
            } */
            throw err;
        });
    });

    helpdeskApi.interceptors.response.use(undefined, function (err: any) {
        return new Promise(() => {
            /* if (err.response?.status === 401) {
                store.dispatch('user/logout').then(() => {
                    // Redirect
                    redirect({ name: 'index' });
                });
            } */
            throw err;
        });
    });

    attendanceApi.interceptors.response.use(undefined, function (err: any) {
        return new Promise(() => {
            throw err;
        });
    });

    // for use inside Vue files (Options API) through this.$axios and this.$api

    app.config.globalProperties.$axios = axios;
    // ^ ^ ^ this will allow you to use this.$axios (for Vue Options API form)
    //       so you won't necessarily have to import axios in each vue file

    app.config.globalProperties.$api = api;
    app.config.globalProperties.$businessApi = businessApi;
    app.config.globalProperties.$helpdeskApi = helpdeskApi;
    app.config.globalProperties.$attendanceApi = attendanceApi;

    // ^ ^ ^ this will allow you to use this.$api (for Vue Options API form)
    //       so you can easily perform requests against your app's API
});

export { axios, api, businessApi, helpdeskApi, attendanceApi };
