import VueQrcode from 'vue3-qrcode';
import { boot } from 'quasar/wrappers';

export default boot(({ app }) => {
    app.component('vue-qrcode', VueQrcode);
});
