// @ts-ignore
import Vue3VideoPlayer from '@cloudgeek/vue3-video-player';
import '@cloudgeek/vue3-video-player/dist/vue3-video-player.css';
import { boot } from 'quasar/wrappers';

export default boot(({ app }) => {
    app.use(Vue3VideoPlayer);
});
