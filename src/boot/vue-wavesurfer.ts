// @ts-ignore
import WaveSurferVue from 'wavesurfer.js-vue';
import { boot } from 'quasar/wrappers';

export default boot(({ app }) => {
	app.use(WaveSurferVue);
});
