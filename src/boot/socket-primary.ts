/* eslint-disable @typescript-eslint/no-unsafe-call */
/* eslint-disable @typescript-eslint/no-floating-promises */
/* eslint-disable @typescript-eslint/no-unsafe-assignment */
/* eslint-disable @typescript-eslint/no-unsafe-member-access */
import { keyBy } from 'lodash';
import { boot } from 'quasar/wrappers';
import io from 'socket.io-client';
import { SocketMessageResponse } from 'src/services/responses/socket/SocketMessageResponse';

const socket = io(process.env.WS_PRIMARY_URL ?? '', {
    transports: ['websocket']
});

export default boot(({ store }) => {
    socket.on('connect', function () {
        console.log('Primary >>', 'Socket connection successful');

        if (store.state.auth.token) {
            socket.emit('authenticate', store.state.auth.token);
        }
    });

    socket.on('disconnect', function () {
        console.log('Primary >>', 'Socket disconnected');
    });

    socket.io.on('reconnect', () => {
        console.log('Primary >>', 'Socket reconnected');
    });

    socket.io.on('reconnection_attempt', () => {
        console.log('Primary >>', 'Socket reconnecting...');
    });

    socket.on('connect_error', () => {
        setTimeout(() => {
            socket.connect();
        }, 1000);
    });

    socket.on('message', (data: string) => {
        // console.log('Primary >>', data);

        const payload: SocketMessageResponse = JSON.parse(data);
        const payload2: any = JSON.parse(data);

        if (payload) {
            store.dispatch('conversation/setSocketSignal', true);
        }

        if (payload2?.conversation?.status == 2) {
            if (
                payload2.conversation.id ==
                store.state.conversation.selectedConversationID
            ) {
                store.dispatch('conversation/closeConversation');
                store.dispatch('chat/unselectConversation');
                store.dispatch('message/reset');
            }
        }

        if (payload2?.message?.type == 1) {
            store.dispatch('message/newMessage', payload2.message);
            store.dispatch('message/putLatestMessage', payload2.message);
        }

        // Handle typing
        if (payload.type == 'system') {
            // console.log('Primary >>', data);

            // Handle new message
            if (payload.body?.message && payload.body?.message?.createdAt) {
                store.dispatch(
                    'message/putLatestMessage',
                    payload.body?.message
                );
                store.dispatch(
                    'conversation/putLatestMessage',
                    payload.body?.message
                );
            }
            // Handle new conversation
            if (payload.body?.conversation) {
                store.dispatch('conversation/update', {
                    ...payload.body?.conversation,
                    members: keyBy(payload.body?.conversation.members, 'userId')
                });
            }
        }
    });
});

export { socket };
