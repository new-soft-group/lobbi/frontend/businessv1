import { boot } from 'quasar/wrappers';
import createPersistedState from 'vuex-persistedstate';

export default boot(({ store }) => {
    createPersistedState({
        key: 'lobbi/auth',
        paths: ['auth']
    })(store);

    createPersistedState({
        key: 'lobbi/user',
        paths: ['user']
    })(store);

    createPersistedState({
        key: 'lobbi/menu',
        paths: ['menu']
    })(store);

    createPersistedState({
        key: 'lobbi/business',
        paths: ['business']
    })(store);

    createPersistedState({
        key: 'lobbi/branch',
        paths: ['branch']
    })(store);

    createPersistedState({
        key: 'lobbi/issueType',
        paths: ['issueType']
    })(store);

    createPersistedState({
        key: 'lobbi/note',
        paths: ['note']
    })(store);

    createPersistedState({
        key: 'lobbi/attendance',
        paths: ['attendance']
    })(store);

    createPersistedState({
        key: 'lobbi/newMessage',
        paths: ['newMessage']
    })(store);

    createPersistedState({
        key: 'lobbi/filter',
        paths: ['filter']
    })(store);

    createPersistedState({
        key: 'lobbi/branch',
        paths: ['branch']
    })(store);

    createPersistedState({
        key: 'lobbi/role',
        paths: ['role']
    })(store);

    createPersistedState({
        key: 'lobbi/member',
        paths: ['member']
    })(store);

    createPersistedState({
        key: 'lobbi/label',
        paths: ['label']
    })(store);

    createPersistedState({
        key: 'lobbi/serviceType',
        paths: ['service']
    })(store);

    createPersistedState({
        key: 'lobbi/conversation',
        paths: ['conversation']
    })(store);
});
