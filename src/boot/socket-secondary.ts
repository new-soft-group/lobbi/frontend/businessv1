/* eslint-disable @typescript-eslint/no-unsafe-call */
/* eslint-disable @typescript-eslint/no-unsafe-assignment */
/* eslint-disable @typescript-eslint/no-floating-promises */
/* eslint-disable @typescript-eslint/no-unsafe-member-access */
import { keyBy } from 'lodash';
import { boot } from 'quasar/wrappers';
import io from 'socket.io-client';
import { MessageDeleteFlag } from 'src/services/enums/MessageDeleteFlag';
import { Message } from 'src/services/models/Message';
import { User } from 'src/services/models/User';
import { SocketMessageResponse } from 'src/services/responses/socket/SocketMessageResponse';

const socket = io(process.env.WS_SECONDARY_URL ?? '', {
    transports: ['websocket']
});

export default boot(({ store, router }) => {
    socket.on('connect', function () {
        console.log('Secondary >>', 'Socket connection successful');

        if (store.state.auth.token) {
            socket.emit('authenticate', store.state.auth.token);
        }
    });

    socket.on('disconnect', function () {
        console.log('Secondary >>', 'Socket disconnected');
    });

    socket.io.on('reconnect', () => {
        console.log('Secondary >>', 'Socket reconnected');
    });

    socket.io.on('reconnection_attempt', () => {
        console.log('Secondary >>', 'Socket reconnecting...');
    });

    socket.on('connect_error', () => {
        setTimeout(() => {
            socket.connect();
        }, 1000);
    });

    socket.on('message', async (data: string) => {
        console.log('Secondary >>', data);

        const payload: SocketMessageResponse = JSON.parse(data);
        // const user: User = store.state.user.user;

        // Handle typing
        /* if (payload.type == 'typing') {
            if (payload.body.userId != user.id) {
                // Store the token and device
                store.dispatch('chat/putIsTyping', payload.body);

                setTimeout(() => {
                    store.dispatch('chat/removeIsTyping', payload.body);
                }, 1500);
            }
        } */

        // Handle pong
        /* if (payload.type == 'pong') {
            if (payload.body.userId != user.id) {
                // Store the token and device
                store.dispatch('chat/putIsOnline', payload.body);

                setTimeout(() => {
                    store.dispatch('chat/removeIsOnline', payload.body);
                }, 12000);
            }
        } */

        // Handle ping
        /* if (payload.type == 'ping') {
            // Return pong
            if (payload.body.userId != user.id) {
                socket.emit(`publish.${payload.body.conversationId ?? ''}`, {
                    type: 'pong',
                    body: {
                        userId: user.id,
                        conversationId: payload.body.conversationId
                    }
                });
            }
        } */

        // Handle invalid token
        if (payload.type == 'alert') {
            store.dispatch('user/logout');
        }

        // Others
        if (payload.type == 'system') {
            console.log('Secondary >>', data);

            // Handle token
            if (payload.body?.token) {
                // Store the token and device
                await store.dispatch('auth/loginByToken', {
                    token: payload.body.token
                });

                setTimeout(() => {
                    // Redirect to home
                    router.replace({ name: 'home' });
                }, 1000);
            }

            // Handle logout
            if (payload.body?.loggedOutDeviceId) {
                if (
                    payload.body?.loggedOutDeviceId ==
                    store.state.user.device?.id
                ) {
                    // alert('oops handle logout alert');
                    // store.dispatch('user/logout');
                }
            } else if (payload.body && 'loggedOutDeviceId' in payload.body) {
                store.dispatch('user/logout');
            }

            // Handle message status
            if (payload.body?.message && !payload.body.message?.createdAt) {
                const message: Message = payload.body?.message;

                // // Update status for broadcast also
                // if (message.recipients && (message.recipients?.[0].deliveredAt || message.recipients?.[0].readAt || message.recipients?.[0].hiddenReadAt)) {
                // 	const data = { messageId: message.id, recipient: message.recipients[0] };
                // 	store.dispatch('message/status', data);
                // 	store.dispatch('conversation/updateLatestMessageStatus', data);

                // 	if (message.broadcastedMessageId) {
                // 		const broadcastedData = {
                // 			messageId: message.broadcastedMessageId,
                // 			recipient: message.recipients[0]
                // 		};

                // 		store.dispatch('message/status', broadcastedData);
                // 		store.dispatch('conversation/updateLatestMessageStatus', broadcastedData);
                // 	}
                // }

                // // Update for everything else
                // else {
                store.dispatch('message/update', message);
                store.dispatch('conversation/updateLatestMessage', message);
                // }

                if (
                    message.flags?.deleteFlag == MessageDeleteFlag.DELETE_SELF
                ) {
                    store.dispatch('message/delete', {
                        body: {
                            messageIds: [message.id],
                            type: MessageDeleteFlag.DELETE_SELF
                        },
                        silent: true
                    });
                    store.dispatch(
                        'conversation/refresh',
                        message.conversationId
                    );
                }
            }

            // Handle conversation updates
            if (payload.body?.conversation) {
                if (payload.body?.conversation.members) {
                    store.dispatch('conversation/update', {
                        ...payload.body?.conversation,
                        members: keyBy(
                            payload.body?.conversation.members,
                            'userId'
                        )
                    });
                } else {
                    store.dispatch(
                        'conversation/update',
                        payload.body?.conversation
                    );

                    if (payload.body?.conversation.lastClearedAt) {
                        store.dispatch('conversation/clear', {
                            conversationId: payload.body?.conversation.id,
                            silent: true
                        });
                    }
                }
            }

            // Handle deleted conversation
            if (payload.body?.deletedConversationId) {
                store.dispatch('conversation/delete', {
                    conversationId: payload.body?.deletedConversationId,
                    silent: true
                });
            }

            // Handle cleared conversation
            if (payload.body?.clearedConversationId) {
                store.dispatch('conversation/clear', {
                    conversationId: payload.body?.clearedConversationId,
                    silent: true
                });
            }

            // Handle profile changes or friend relation changes
            /* if (payload.body?.user) {
                if (payload.body?.user.id == user.id) {
                    store.dispatch('user/update', {
                        user: payload.body?.user,
                        silent: true
                    });
                } else {
                    store.dispatch('relation/updateFriend', payload.body?.user);
                }
            } */
        }
    });
});

export { socket };
